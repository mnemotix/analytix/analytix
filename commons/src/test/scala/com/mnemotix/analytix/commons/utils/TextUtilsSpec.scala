/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.commons.utils

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TextUtilsSpec extends AnyFlatSpec with Matchers {

  it should "normalize a text" in {
    val text = "Aucune amélioration n’est enregistrée dans la distribution des produits pétroliers dans les stations d'essence à Port-au-Prince comme dans les villes de province."
    val t = TextUtils.normalize(text)
    println(t)
    t shouldEqual "aucune amelioration n’est enregistree dans la distribution des produits petroliers dans les stations d'essence a port-au-prince comme dans les villes de province."
  }

  it should "strip accents" in {
    val text = "Aucune amélioration n’est enregistrée dans la distribution des produits pétroliers dans les stations d'essence à Port-au-Prince comme dans les villes de province."
    val t = TextUtils.stripAccents(text)
    println(t)
    t shouldEqual "Aucune amelioration n’est enregistree dans la distribution des produits petroliers dans les stations d'essence a Port-au-Prince comme dans les villes de province."
  }

  it should "strip punctuation" in {
    val text = "Aucune amélioration n’est enregistrée dans la distribution des produits pétroliers dans les stations d'essence à Port-au-Prince, comme dans les villes de province."
    val t = TextUtils.stripPunctuation(text)
    println(t)
    t shouldEqual "Aucune amélioration n’est enregistrée dans la distribution des produits pétroliers dans les stations d essence à Port au Prince  comme dans les villes de province "
  }

  it should "slugify a text" in {
    val text = "Aucune amélioration n’est enregistrée dans la distribution des produits pétroliers dans les stations d'essence à Port-au-Prince, comme dans les villes de province."
    val t = TextUtils.slugify(text)
    println(t)
    t shouldEqual "aucune-amelioration-nest-enregistree-dans-la-distribution-des-produits-petroliers-dans-les-stations-dessence-a-port-au-prince-comme-dans-les-villes-de-province"
  }

  it should "asciify a text" in {
    val germanString = "Entwickeln Sie mit Vergnügen"
    val t = TextUtils.asciify(germanString)
    println(t)
    t shouldEqual "Entwickeln Sie mit Vergnügen"
  }
}
