/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.commons.utils


object CollectionUtils {

  def invertMap[A,B](oldMap: Map[A, Iterable[B]]): Map[B, Seq[A]] = {
    oldMap.foldLeft(Map[B, Seq[A]]().withDefaultValue(Seq())) {
      case (m, (a, bs)) => bs.foldLeft(m)((map, b) => map.updated(b, m(b) :+ a))
    }
  }

  def addOrUpdate[K, V](m: collection.mutable.Map[K, V], k: K, kv: (K, V),
                        f: V => V): Unit = {
    m.get(k) match {
      case Some(e) => m.update(k, f(e))
      case None    => m += kv
    }
  }

  def updateMap[A, B](map: Map[A, Seq[B]], key: A, value: B): Map[A, Seq[B]] =
    map + ((key, map.getOrElse(key, Seq()) :+ value))
}