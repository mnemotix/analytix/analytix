/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.commons.utils

import scala.xml.{Elem, XML}


object MavenToSbtConverter {

  object scala {
    val version = "SCALA_VERSION$"
  }

  def gen(xml: Elem) = {


    val data: Seq[(String, String, String)] =
      (xml \ "dependency") map { d =>
      val groupId = (d \ "groupId").text
      val artifactId = (d \ "artifactId").text
      val versionNum = (d \ "version").text

      (groupId, artifactId, versionNum)
    }

    val CrossBuildArtifact = """([\w-]+)_\$SCALA_VERSION\$""".r

    def dep(a: String, g: String, v: String, cross: Boolean) = {
      val sep = if (cross) "%%" else "%"
      val ident = a.split("-").map(_.capitalize).mkString
      //"""val %s = "%s" %s "%s" %% "%s" """ format (ident, g, sep, a, v)
      """"%s" %s "%s" %% "%s" """ format ( g, sep, a, v)
    }

    val m = data map {
      case (g, CrossBuildArtifact(a), v) => dep(a, g, v, true)
      case (g, a, v) => dep(a, g, v, false)
    } mkString("\n")

    m
  }

}
