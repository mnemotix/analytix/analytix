/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.commons.utils

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Source}

import scala.concurrent.duration.{DurationInt, FiniteDuration}

object StreamsRetryUtils {

  implicit class ElemThroughFlowOps[T](val elem: T) extends AnyVal {

    /*
      this abstraction supports retrying on a stream where we decide by provided predicate whether the result is successful or not
      it retries processing elem through the passed flow and completes the stream after first successful result has been found
      it delays the stream by defined `delay` duration between the attempts
      it gives up after defined number of retry attempts and completes stream with the last obtained result (no matter if
      successful according to the predicate or not), but will not fail it!
    */
    def throughFlowWithRetry[U](retryAttempts: Int, delay: FiniteDuration, isResultOk: U => Boolean, logAttempt: Int => Unit = _ => ())(flow: Flow[T, U, NotUsed]): Source[U, NotUsed] = {
      require(retryAttempts >= 0, "retryAttempts parameter can't be negative")

      val beforeAttemptDelays = 0.seconds #:: Stream.fill(retryAttempts)(delay)

      val isAttemptOk = (attempt: Int, result: U) =>
        attempt > retryAttempts || isResultOk(result)

      elem.throughFlowWithRetryDelays(beforeAttemptDelays, isAttemptOk, logAttempt)(flow)
    }

    /*
      this abstraction supports retrying on a stream where we decide by provided predicate whether the result is successful or not
      it retries processing elem through the passed flow and completes the stream after first successful result has been found
      it delays the stream with expotentially growing delay durations, where first retry delay is `minDelay` and delay before
      last attempt is `maxDelay`
      it gives up after defined number of retry attempts and completes stream with the last obtained result (no matter if
      successful according to the predicate or not), but will not fail it!
     */

    def throughFlowWithBackoffRetry[U](retryAttempts: Int, minDelay: FiniteDuration, maxDelay: FiniteDuration, isResultOk: U => Boolean, logAttempt: Int => Unit = _ => ())(flow: Flow[T, U, NotUsed]): Source[U, NotUsed] = {

      require(retryAttempts > 0, "retryAttempts parameter can't be negative")
      require(minDelay > 0.millis, "minDelay has to be greater than 0")
      require(minDelay <= maxDelay, "maxDelay can't be less than minDelay")

      val beforeAttemptDelays = computeBackoffDelayDurations(retryAttempts, minDelay, maxDelay)

      val isAttemptOk = (attempt: Int, result: U) =>
        attempt > retryAttempts || isResultOk(result)

      elem.throughFlowWithRetryDelays(0.seconds #:: beforeAttemptDelays, isAttemptOk, logAttempt)(flow)
    }

    /*
      this abstraction supports retrying an element on the stream where we decide if result produced by the flow
      on given attempt is successful or not
      it retries processing elem through the passed flow and completes the stream after first successful result has been found
      before every attempt it delays the stream according to the passed collection of delay durations
      before every delay, it invokes `logAttempt` closure, that allows for convenient logging attempts
      if successful result can't be produced, it completes the stream after number of retries that is equivalent to
      the size of `beforeAttemptDelays`
    */

    def throughFlowWithRetryDelays[U](beforeAttemptDelays: collection.immutable.Iterable[FiniteDuration], isAttemptOk: (Int, U) => Boolean, logAttempt: Int => Unit = _ => ())(flow: Flow[T, U, NotUsed]): Source[U, NotUsed] = {

      Source(beforeAttemptDelays zip Stream.from(1))
        .flatMapConcat {
          case (beforeAttemptDelay, attempt) =>
            logAttempt(attempt)
            Source.single(elem)
              .delay(beforeAttemptDelay)
              .via(flow)
              .map(result => (attempt, result))
        }
        .collect {
          case (attempt, result) if isAttemptOk(attempt, result) =>
            result
        }
        .take(1)
    }

    /*
     this abstraction supports retrying failed stream at most `attempts` number of times, introducing delay between attempts;
     it gives up with a failed source if after defined number of attempts no element is successfully produced;
     it completes the stream once first successful result has been obtained
    */
    def throughFlowWithRecover[U](retryAttempts: Int, delay: FiniteDuration, logAttempt: Int => Unit = _ => ())(flow: Flow[T, U, NotUsed]): Source[U, NotUsed] = {
      @volatile var attempt = 1
      logAttempt(1)
      Source
        .single(elem)
        .via(flow)
        .recoverWithRetries(retryAttempts, {
          case _ =>
            attempt += 1
            logAttempt(attempt)
            Source.single(elem)
              .delay(delay)
              .via(flow)
        })
    }
  }

  def computeBackoffDelayDurations(retryAttempts: Int, minDelay: FiniteDuration, maxDelay: FiniteDuration): LazyList[FiniteDuration] = {
    require(retryAttempts >= 0, "retryAttempts parameter can't be negative")
    require(minDelay > 0.millis, "minDelay has to be greater than 0")
    require(minDelay <= maxDelay, "maxDelay can't be less than minDelay")

    val backoffFactor: Double = retryAttempts match {
      case 0 =>
        // we don't care, no delays at all
        1.0
      case 1 =>
        // we don't care, only single delay
        1.0
      case _ =>
        // first is min, second is min * backoffFactor, third is min * backoffFactor^2 ...
        // ... last is min * backoffFactor^(retryAttempts - 1) which should be equal to max
        // max = min * backoffFactor^(retryAttempts - 1)
        // backoffFactor^(retryAttempts - 1) = max / min
        // backoffFactor = pow(max / min, 1 / (retryAttempts - 1))
        Math.pow(maxDelay / minDelay, 1.0 / (retryAttempts - 1))
    }

    LazyList.iterate(minDelay, retryAttempts) {
      delay => (delay * backoffFactor).asInstanceOf[FiniteDuration] // should be safe due to passed params
    }
  }

}
