/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.commons.utils

import akka.NotUsed
import akka.stream.scaladsl.{Broadcast, Flow, GraphDSL, Keep, Merge, Sink, Source}
import akka.stream.{Materializer, SourceShape}

import scala.concurrent.Future
import scala.concurrent.duration._


object AkkaStreamIterationUtils {
  def iterateWithProgress[S, P](init: S,
                                iteration: Flow[S, S, NotUsed],
                                progressF: (S, Float) => P,
                                maxTime: FiniteDuration,
                                progressInterval: FiniteDuration): Source[P, Future[S]] = {

    loopedSource(init, iteration)
      .viaMat(progressFlow(progressF, maxTime, progressInterval))(Keep.right)
  }

  def loopedSource[S](init: S, iteration: Flow[S, S, NotUsed]): Source[S, NotUsed] = {

    Source.fromGraph(GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val merge = b.add(Merge[S](2))
      val broadcast = b.add(Broadcast[S](2, eagerCancel = true))

      Source.single(init) ~> merge.in(0)
      merge.out ~> iteration.async ~> broadcast.in
      broadcast.out(1) ~> merge.in(1)

      SourceShape(broadcast.out(0))
    })
  }

  def progressFlow[S, P](progressF: (S, Float) => P,
                         maxTime: FiniteDuration,
                         progressInterval: FiniteDuration): Flow[S, P, Future[S]] = {

    val progressTicks = Source.tick(0.seconds, progressInterval, ())
      .zipWithIndex
      .map { case (_, i) => Math.min(1.0f, progressInterval.toMillis.toFloat * i / maxTime.toMillis) }

    Flow[S]
      .conflate(Keep.right) // allow the to run at the full speed
      .takeWithin(maxTime)
      .alsoToMat(Sink.last)(Keep.right)
      .zipMat(progressTicks)(Keep.left)
      .map(progressF.tupled)
  }

  implicit class MatSourceOps[A, B](val source: Source[A, Future[B]]) extends AnyVal {

    def concatMaterializedValue(implicit m: Materializer): Source[Either[A, B], NotUsed] = {

      val (valueFuture, stream) = source.preMaterialize()

      stream
        .map(Left.apply)
        .concat(Source.fromFuture(valueFuture).map(Right.apply))
    }
  }
}
