package com.mnemotix.analytix.commons.utils

import java.nio.charset.StandardCharsets
import java.util.Base64

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object TextUtils {
  def normalize(text: String): String = com.mnemotix.synaptix.core.utils.StringUtils.normalize(text)

  def stripAccents(str: String): String = com.mnemotix.synaptix.core.utils.StringUtils.stripAccents(str)

  def stripPunctuation(str: String): String = com.mnemotix.synaptix.core.utils.StringUtils.stripPunctuation(str)

  def slugify(input: String) = com.mnemotix.synaptix.core.utils.StringUtils.slugify(input)

  def asciify(rawString: String) = {
    val buffer = StandardCharsets.UTF_8.encode(rawString)
    StandardCharsets.UTF_8.decode(buffer).toString
  }
}