/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.phonetic

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

class DoubleMetaphoneSpec extends AnyFlatSpec with Matchers {

  it should "encode a name" in {
    val code = DoubleMetaphone.sound("Peter")
    println(code)
    code.size should be <= 5
  }

  it should "give the same code to the same name" in {
    val code1 = DoubleMetaphone.sound("Lhérisson")
    val code2 = DoubleMetaphone.sound("Lherisson")
    println(DoubleMetaphone.sound("Nicolas"))
    println(code1)
    println(code2)
    code1 shouldEqual(code2)
  }

  it should "give the alternate version of a name with no variation" in {
    val codes = DoubleMetaphone.double("Lherisson")
    println(codes._2)
    codes._2 shouldEqual(None)
  }

  it should "give the alternate version of a name with a variation" in {
    val codes = DoubleMetaphone.double("William")

    println(codes)
    codes._1 == codes._2.get shouldBe false
  }



}
