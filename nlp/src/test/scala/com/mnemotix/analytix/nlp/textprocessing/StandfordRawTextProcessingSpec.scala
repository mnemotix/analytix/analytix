package com.mnemotix.analytix.nlp.textprocessing

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class StandfordRawTextProcessingSpec extends AnyFlatSpec with  Matchers {

  behavior of "RawTextProcessing"

  it should "get properties" in {
    val rawTextProcessing = new StandfordRawTextProcessing("French")
    rawTextProcessing.init
    println(rawTextProcessing.getProperties)
  }

  it should "tokenize a label" in {
    val rawTextProcessing = new StandfordRawTextProcessing("French")
    rawTextProcessing.init
    val startTimeMillis = System.currentTimeMillis()
    val tokens = rawTextProcessing.words("Mettre en place des actions commerciales")
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    tokens.foreach(println(_))
    tokens.size must be > 0
  }

  it should "tokenize a text" in {
    val rawTextProcessing = new StandfordRawTextProcessing("French")
    rawTextProcessing.init
    val startTimeMillis = System.currentTimeMillis()
    val tokens = rawTextProcessing.words("""La France est un pays attachant avec de magnifiques monuments et une savoureuse gastronomie. C'est pourquoi parler français lors de ses voyages ou pour nouer des relations professionnelles demeure un vrai plus !
                   |
                   |Nous avons sélectionné avec des professeurs expérimentés de français des textes faciles à lire et amusants. A la fin des textes, un exercice sous forme de QCM permet de valider ses acquis. Il est à noter que l'édition gratuite des textes sous forme de PDF est un vrai plus. Désormais la langue de Molière n'aura plus de secret pour vous.""".stripMargin)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000

    println(s"The durations is ${durationSeconds} s")
    tokens.foreach(println(_))
    tokens.size must be > 0
  }

  it should "retrieve the lemma of a label" in {
    val rawTextProcessing = new StandfordRawTextProcessing("French")
    rawTextProcessing.init
    val startTimeMillis = System.currentTimeMillis()
    val tokens = rawTextProcessing.lemmas("Chargé d'affaires dans l'informatique")
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    tokens.foreach(println(_))
    tokens.size must be > 0
  }

  it should "strip accent of a label" in {
    val rawTextProcessing = new StandfordRawTextProcessing("French")
    rawTextProcessing.init
    val startTimeMillis = System.currentTimeMillis()
    val tokens = rawTextProcessing.stripAccents("Chargé d'affaires dans l'informatique")
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    tokens mustBe("Charge d'affaires dans l'informatique")
  }

  it should "retrieve token of a text" in {
    val rawTextProcessing = new StandfordRawTextProcessing("French")
    rawTextProcessing.init
    val startTimeMillis = System.currentTimeMillis()
    val tokens = rawTextProcessing.tokens("Chargé d'affaires dans l'informatique")
    tokens.foreach(println(_))
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
  }

  it should "try to tokennize and recognize POS" in {
    val saulPeter = "Saul, Peter"
    val temChretien = "Témoignage chrétien"
    lazy val standfordRawTextProcessing = new StandfordRawTextProcessing("fr")
    standfordRawTextProcessing.init
    val saulPeterTokens = standfordRawTextProcessing.tokens(saulPeter)
    saulPeterTokens.foreach(token =>  println(s"token = ${token.word}, pos = ${token.pos.pos.getOrElse("")}"))

    val temChretienTokens = standfordRawTextProcessing.tokens(temChretien)
    temChretienTokens.foreach(token =>  println(s"token = ${token.word}, pos = ${token.pos.pos.getOrElse("")}"))
  }
}