/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.helpers

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class NGramSpec extends AnyFlatSpec with Matchers {

  it should "extract NGram from a sentence" in {
    val startTimeMillis = System.currentTimeMillis()

    val s = """In mathematics, graph theory is the study of graphs, which are mathematical structures used to model pairwise
      |relations between objects. A graph in this context is made up of vertices, also called
      |nodes or points, which are connected by edges, also called links or lines. A distinction is made
      |between undirected graphs, where edges link two vertices symmetrically, and directed
      |graphs, where edges link two vertices asymmetrically. Graphs are one
      |of the principal objects of study in discrete mathematics.""".stripMargin

    val ngrams = NGram.build(s, 2)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"The durations is ${durationSeconds} s")
    ngrams.model.foreach(println(_))

    ngrams.n mustEqual(2)
  }

}
