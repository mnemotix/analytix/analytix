/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.textprocessing

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AnalytixRawTextProcessingSpec extends AnyFlatSpec with Matchers {

  it should "retrieve stop words from config file" in {
    val analytixRawTextProcessing = new AnalytixRawTextProcessing("fr")
    analytixRawTextProcessing.stopWordsConfig.size should be > 0
  }

  it should "retrieve stop words from resource file" in {
    val analytixRawTextProcessingFR = new AnalytixRawTextProcessing("fr")
    val analytixRawTextProcessingEn = new AnalytixRawTextProcessing("en")
    analytixRawTextProcessingFR.stopWords.size shouldEqual(691)
    analytixRawTextProcessingEn.stopWords.size shouldEqual(544)
  }

  it should "extract words" in {
    val txt = "des mots qui se suivent et qui ne veulent rien dire. Ils se suivent, mais ne veulent rien dire."
    val analytixRawTextProcessingFR = new AnalytixRawTextProcessing("fr")
    analytixRawTextProcessingFR.words(txt).foreach(println(_))
    analytixRawTextProcessingFR.words(txt).size shouldEqual(5)
  }

  it should "count word" in {
    val txt = "des mots qui se suivent et qui ne veulent rien dire. Ils se suivent, mais ne veulent rien dire."
    val analytixRawTextProcessingFR = new AnalytixRawTextProcessing("fr")
    val count = analytixRawTextProcessingFR.wordCount(txt)
    count.size shouldEqual(3)
    count.map(c => println(c))
    count.get("suivent").get shouldEqual(2)
  }

  it should "extract sentence in english text" in {
    val helper = new AnalytixRawTextProcessing("en")
    val text = "Mr. John Johnson Jr. was born in the U.S.A but earned his Ph.D. in Israel before joining Nike Inc. as an engineer. He also worked at craigslist.org as a business analyst."
    val sentences = helper.sentencesTextExtactor(text)
    sentences.foreach(s => println(s"Sentence ==> $s"))
    sentences.size shouldEqual(2)
  }

  it should "extract sentence in french text" in {
    val helper = new AnalytixRawTextProcessing("fr")
    val text = """Le mot «Monseigneur», composé de l'adjectif possessif «mon» et de «seigneur» est employé dès l'année 1160. On le retrouve sous la forme «mon seignor», dans le sens de «titre accompagnant le nom d'un saint» puis «monseigneur» en 1216, pour qualifier «un titre donné à des personnages éminents». Son abréviation, attestée par Le Petit Robert, est «Mgr»."""
    val sentences = helper.sentencesTextExtactor(text)
    sentences.foreach(s => println(s"Sentence ==> $s"))
    sentences.size shouldEqual(3)
  }

  it should "lemma a text" in {
    val proc = new AnalytixRawTextProcessing("fr")
    val txt = "des mots qui se suivent et qui ne veulent rien dire. Ils se suivent, mais ne veulent rien dire."
    val lemmas = proc.lemmas(txt)
    lemmas.size shouldEqual(5)
  }

  it should "lemma a word" in {
    val proc = new AnalytixRawTextProcessing("fr")
    val poltronMot = proc.lemma("poltrons")
    val lemmaSuivent = proc.lemma("suivent")
    val lemmaSuivre = proc.lemma("suivre")
    val lemmaSuivant = proc.lemma("suivant")
    poltronMot shouldEqual("poltr")
    lemmaSuivent shouldEqual("suiv")
    lemmaSuivre shouldEqual("suivr")
    lemmaSuivant shouldEqual("suiv")
  }

  it should "filter word" in {
    val proc = new AnalytixRawTextProcessing("fr")
    val familyName = Seq("ruud","van", "nistelrooy")
    val filtered = proc.filterWords(familyName)
    filtered.foreach(println(_))
    filtered.size shouldEqual(2)
  }
}