/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.helpers

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class AnalytixSentenceExtractorHelperSpec extends AnyFlatSpec with Matchers {

  it should "extract sentence in english text" in {
    val helper = new AnalytixSentenceExtractorHelper()
    val text = "Mr. John Johnson Jr. was born in the U.S.A but earned his Ph.D. in Israel before joining Nike Inc. as an engineer. He also worked at craigslist.org as a business analyst."
    val sentences = helper.enSentenceExtractor(text)
    sentences.foreach(s => println(s"Sentence ==> $s"))
    sentences.size mustEqual(2)
  }

  it should "extract sentence in french text" in {
    val helper = new AnalytixSentenceExtractorHelper()
    val text = """Le mot «Monseigneur», composé de l'adjectif possessif «mon» et de «seigneur» est employé dès l'année 1160. On le retrouve sous la forme «mon seignor», dans le sens de «titre accompagnant le nom d'un saint» puis «monseigneur» en 1216, pour qualifier «un titre donné à des personnages éminents». Son abréviation, attestée par Le Petit Robert, est «Mgr»."""
    val sentences = helper.frSentenceExtractor(text)
    sentences.foreach(s => println(s"Sentence ==> $s"))
    sentences.size mustEqual(3)
  }

}