/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.keywordExtraction

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class SingleDocumentSpec extends AnyFlatSpec with Matchers {

  it should "retrieve all the couples" in {
    val singleDocument = new SingleDocument("french")
    val frequentTerm = Seq("primaire","formation","formatage","culturel","syndicaliste","histoire","europe")
    singleDocument.couples(frequentTerm).size should be > 0
  }

  it should "compute the nw function" in {
    val singleDocument = new SingleDocument("french")
    val w = "fromage"
    val sentences = Seq(Seq("omelette", "fromage", "oignon"), Seq("fromage","pain"), Seq("vin", "charcuterie", "pain","beurre"))
    val resNw = singleDocument.nw(w, sentences)
    resNw shouldEqual(2)
  }

  it should "retrieve the expected probability of a cluster" in {
    val singleDocument = new SingleDocument("french")
    val g = "fromage"
    val cluster = Seq(Seq("omelette", "fromage"), Seq("fromage", "oignon"), Seq("fromage", "pain"), Seq("vin", "charcuterie"), Seq("charcuterie", "pain"), Seq("pain", "beurre"))
    val clusterProb = singleDocument.clusterExpectedProbability(g, cluster)
    clusterProb shouldEqual(3)
  }

  it should "retrieve the expected probality of a sentence" in {
    val singleDocument = new SingleDocument("french")
    val w = "fromage"
    val sentences = Seq(Seq("omelette", "fromage", "oignon"), Seq("fromage","pain"), Seq("vin", "charcuterie", "pain","beurre"))
    val res = singleDocument.sentenceExpectedProbability(w, sentences)
    res shouldEqual(2.0/9.0)
  }

  it should "retrieve the main words of a text" in {
    val text = """Développeur Web & Mobile
                 |- Weever : Site web générique permettant la structuration et la valorisation des données
                 |artistiques, utilisé notamment par Fondation d'entreprise Galeries Lafayette. Rajout de diverses
                 |interfaces et fonctionnalitées variées.
                 |- BioMétéo : Développement d’un site web présentant la biodiversité en Dordogne ainsi que la
                 |météo locale.
                 |ReactJs, Graphql, Apollo-GraphQL, Docker, Git, MaterialUI""".stripMargin
    val startTimeMillis = System.currentTimeMillis()
    val singleDocument = new SingleDocument("french")

    val terms = singleDocument.extract(text)
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"the process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
    terms.foreach(println(_))
    terms.size should be > 0
  }
}