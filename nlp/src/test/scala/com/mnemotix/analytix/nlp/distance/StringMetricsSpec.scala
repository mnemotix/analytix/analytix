package com.mnemotix.analytix.nlp.distance

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class StringMetricsSpec extends AnyFlatSpec with  Matchers  {
  it should "compute the jaroWinkler distance betwenn two similar string" in {
    (StringMetrics.jaroWinkler("peter", "peter")) mustBe(0)
  }

  it should "compute the jaroWinkler distance betwenn two different string" in {
    (StringMetrics.jaroWinkler("peter", "peater")) must be > 0.0
  }

  it should "compute levenshtein distance" in {
    println(StringMetrics.levenshtein("Assistant commercial et ADV F/H", "Assistant commercial"))
    (StringMetrics.levenshtein("peter", "peter")) mustBe(0)
    (StringMetrics.levenshtein("peter", "castagner")) must be > 0.0
  }

  it should "compute ngram distance" in {
    println(StringMetrics.ngram("Assistant commercial et ADV F/H", "Assistant commercial", 3))
    println(StringMetrics.ngram("Assistant commercial", "Assistant commercial", 3))

    StringMetrics.ngram("Assistant commercial", "Assistant commercial", 3).get mustBe(0)
  }

  it should "compute diceSorensen" in {
    StringMetrics.diceSorensen("Assistant commercial", "Assistant commercial", 3).get mustBe(0)
  }

  it should "compute ratcliffObershelp" in {
    StringMetrics.ratcliffObershelp("Assistant commercial", "Assistant commercial").get mustBe(0)
  }

  it should "compute different distance value for slightly equals strings" in {
    println(s"N-Gram: ${StringMetrics.ngram("Assistant commercial et ADV F/H", "Assistant commercial", 3)}")
    println(s"Jaro Winkler: ${StringMetrics.jaroWinkler("Assistant commercial et ADV F/H", "Assistant commercial")}" )
    println(s"Levenshtein: ${StringMetrics.levenshtein("Assistant commercial et ADV F/H", "Assistant commercial")}")
    println(s"Dice Sorensen: ${StringMetrics.diceSorensen("Assistant commercial et ADV F/H", "Assistant commercial", 3)}")
    println(s"RatcliffObershelp: ${StringMetrics.ratcliffObershelp("Assistant commercial et ADV F/H", "Assistant commercial")}")
  }

  it should "compute different distance value for different equals strings" in {
    println(s"N-Gram: ${StringMetrics.ngram("Design industriel", "Directeur industriel", 3)}")
    println(s"Jaro Winkler: ${StringMetrics.jaroWinkler("Design industriel", "Directeur industriel")}" )
    println(s"Levenshtein: ${StringMetrics.levenshtein("Design industriel", "Directeur industriel")}")
    println(s"Dice Sorensen: ${StringMetrics.diceSorensen("Design industriel", "Directeur industriel", 3)}")
    println(s"RatcliffObershelp: ${StringMetrics.ratcliffObershelp("Design industriel", "Directeur industriel")}")
  }

  it should "compute an overall distance" in {
    val s1 = "Design industriel"
    val s2 = "Directeur industriel"
    //val s1 = "Assistant commercial et ADV F/H"
    //val s2 = "Assistant commercial"
    val distanceTotal = StringMetrics.ngram(s1, s2, 3).get + StringMetrics.jaroWinkler(s1, s2) + StringMetrics.levenshtein(s1, s2) + StringMetrics.diceSorensen(s1, s2, 3).get + StringMetrics.ratcliffObershelp(s1, s2).get
    println(distanceTotal / 5)

  }

  it should "compute different distance for two name" in {
    val name1 = " goujet daniel "
    val name2 =  " daniel goujet "

    println(s"jaro winkler ${StringMetrics.jaroWinkler(name1, name2)}")
    println(s"levenshtein ${StringMetrics.levenshtein(name1, name2)}")
    println(s"ngram ${StringMetrics.ngram(name1, name2, 3)}")
    println(s"diceSorensen ${StringMetrics.diceSorensen(name1, name2, 3)}")
    println(s"ratcliffObershelp ${StringMetrics.ratcliffObershelp(name1, name2)}")
    println(s"distance de Jaccard ${StringMetrics.jaccard(name1, name2)}")

    // kusber wolf henning | wolf henning kusber
  }
}