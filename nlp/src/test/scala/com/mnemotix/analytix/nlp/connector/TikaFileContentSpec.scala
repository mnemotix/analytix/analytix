/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.connector

import java.io.File

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class TikaFileContentSpec extends AnyFlatSpec with Matchers {

  behavior of "tika file content"

  val tikaFileContent = new TikaFileContent

  it should "parse a docx file" in {
    val docx = new File("modules/nlp/src/test/resources/data/file/doc.docx")
    val result = tikaFileContent.parse(docx)
    println(result)
    result.isDefined mustBe true
  }

  it should "parse an epub file" in {
    val epub = new File("modules/nlp/src/test/resources/data/file/epub.epub")
    val result = tikaFileContent.parse(epub)
    println(result)
    result.isDefined mustBe true
  }

  it should "parse a html file" in {
    val html = new File("modules/nlp/src/test/resources/data/file/html.html")
    val result = tikaFileContent.parse(html)
    println(result)
    result.isDefined mustBe true
  }

  it should "parse an odt file" in {
    val odt = new File("modules/nlp/src/test/resources/data/file/odt.odt")
    val result = tikaFileContent.parse(odt)
    println(result)
    result.isDefined mustBe true
  }

  it should "parse a pdf file" in {
    val pdf = new File("modules/nlp/src/test/resources/data/file/pdf.pdf")
    val result = tikaFileContent.parse(pdf)
    println(result)
    result.isDefined mustBe true
  }

  it should "parse an rtf file" in {
    val rtf = new File("modules/nlp/src/test/resources/data/file/rtf.rtf")
    val result = tikaFileContent.parse(rtf)
    println(result)
    result.isDefined mustBe true
  }
}