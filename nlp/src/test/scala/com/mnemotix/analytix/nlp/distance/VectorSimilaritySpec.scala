/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class VectorSimilaritySpec extends AnyFlatSpec with Matchers with LazyLogging {
  it should "test norm" in {
    val d: Array[Double] = Array(1.0, 2.0, 3.0)
    (Math.pow(1.0, 2) + Math.pow(2.0, 2) + Math.pow(3.0, 2)) shouldEqual (VectorSimilarity.norm(d))
  }

  it should "test a vector sim" in {
    val d = Array(1.0, 2.0, 3.0)
    val res: Double = VectorSimilarity.cosineSimilarity(d, d)

    val d1 = Array(-1.0, 2.0, -3.0)
    val res1: Double = VectorSimilarity.cosineSimilarity(d, d1)
    println(res1)

    println(res)
    res shouldBe(1.0)
  }
}