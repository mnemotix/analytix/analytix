/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance.cpmerge

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class FuzzyMapSpec extends AnyFlatSpec with  Matchers {

  it should "build a map and retrieve value from query" in {
    val fuzzyHash: FuzzyMap[String] = FuzzyMap(List(("Associé industriel", "uri1"),
      ("directeur industriel", "uri2"),
      ("préparer monter composants électroniques carte électronique", "uri3"),
      ("réalisation opérations automatisées", "uri4")
    ))

    println(fuzzyHash.get("un directeur industriel", 0.8, Cosine))
    println(fuzzyHash.get("un directeur industriel", 0.8, Jaccard))
    println(fuzzyHash.get("un directeur industriel", 0.8, Dice))

    println(fuzzyHash.getMatches("Nous recherchons un directeur industriel", 0.5, Cosine))
    println(fuzzyHash.getMatches("Nous recherchons un directeur industriel", 0.5, Jaccard))
    println(fuzzyHash.getMatches("Nous recherchons un directeur industriel", 0.5, Dice))
  }

}
