/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance

import java.io.FileInputStream
import com.mnemotix.analytix.nlp.textprocessing.{AnalytixRawTextProcessing, AnalytixRawTextProcessingSpec}
import com.typesafe.scalalogging.LazyLogging
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer
import org.nd4j.linalg.factory.Nd4j
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers



class Word2VecSpec extends AnyFlatSpec with Matchers with LazyLogging{

  //lazy val designIndus = "Associé industriel"
  //lazy val directeurIndus = "Directeur industriel"

  // Préparer monter composants électroniques carte électronique, Réalisation opérations automatisées

  lazy val designIndus = "Préparer monter composants électroniques carte électronique"
  lazy val directeurIndus = "Réalisation opérations automatisées"

  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing("french")

 it should "test word2vec" in {
   val startTime = System.currentTimeMillis()
   val file = new FileInputStream("modules/nlp/src/test/resources/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin")
   val model = WordVectorSerializer.readBinaryModel(file, true, true)
   logger.info(">> Time took load Google News Model => {} s", (System.currentTimeMillis() - startTime)/ 100)

   val listDesignIndus = synaptixRawTextProcessing.words(designIndus)
   val listDirecteurIndus = synaptixRawTextProcessing.words(directeurIndus)
   println(listDesignIndus.size)
   println(listDirecteurIndus.size)
   (listDesignIndus.foreach(println(_)))
   (listDirecteurIndus.foreach(println(_)))

   val arraysDesignIndus: Seq[Array[Double]] = listDesignIndus.map { word =>
      model.getWordVector(word)
   }.filterNot(_ == null)


   val arraysDirecteurIndus: Seq[Array[Double]] = listDirecteurIndus.map { word =>
     model.getWordVector(word)
   }.filterNot(_ == null)

   arraysDirecteurIndus.foreach(println(_))

   val designIndusVec: Array[Double] = arraysDesignIndus.reduceLeft { (x, y) =>
     (x).lazyZip(y).map(_+_)
   }
   val directeurIndusVec: Array[Double] = arraysDirecteurIndus.reduceLeft { (x, y) =>
     (x).lazyZip(y).map(_+_)
   }

   val cosineSim = VectorSimilarity.cosineSimilarity(designIndusVec, directeurIndusVec)
   println(cosineSim)

   val w2v = new Word2Vec("french")
   val rs = w2v.similarity("première phrase", "")
   println(rs)
 }
}