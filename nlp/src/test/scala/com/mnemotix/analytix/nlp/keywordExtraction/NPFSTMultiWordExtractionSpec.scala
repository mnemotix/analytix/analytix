/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.keywordExtraction

import com.mnemotix.analytix.nlp.model.MultiWord
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class NPFSTMultiWordExtractionSpec extends AnyFlatSpec with Matchers {

  it should "retrieve the coarseMap" in {
    val multiWordExtraction = new NPFSTMultiWordExtraction("fr")
    multiWordExtraction.init
    multiWordExtraction.coarseMap shouldEqual Map(
      "A" -> Seq("JJ", "JJR", "JJS", "CoarseADJ", "CD", "CoarseNUM", "A", "ADJ"),
      "D" -> Seq("DT", "CoarseDET", "D", "DET"),
      "P" -> Seq("IN", "TO", "CoarseADP", "P", "ADP"),
      "N" -> Seq("NN", "NNS", "NNP", "NNPS", "FW", "CoarseNOUN", "N", "NOUN", "PROPN", "^")
    )
  }

  it should "retrieve tag2coarse" in {
    val multiWordExtraction = new NPFSTMultiWordExtraction("fr")
    multiWordExtraction.init
    val t2c = multiWordExtraction.tag2coarse
    val inverted = Map(
      "CoarseNOUN"-> Seq("N"),
      "CoarseADJ"-> Seq("A"),
      "DET"-> Seq("D"),
      "JJ"-> Seq("A"),
      "DT"-> Seq("D"),
      "CoarseADP"-> Seq("P"),
      "NOUN"-> Seq("N"),
      "NN"-> Seq("N"),
      "FW"-> Seq("N"),
      "TO"-> Seq("P"),
      "NNS"-> Seq("N"),
      "NNP"-> Seq("N"),
      "A"-> Seq("A"),
      "D"-> Seq("D"),
      "ADP"-> Seq("P"),
      "PROPN"-> Seq("N"),
      "CoarseDET"-> Seq("D"),
      "CD"-> Seq("A"),
      "P"-> Seq("P"),
      "CoarseNUM"-> Seq("A"),
      "IN"-> Seq("P"),
      "N"-> Seq("N"),
      "ADJ"-> Seq("A"),
      "^"-> Seq("N"),
      "NNPS"-> Seq("N"),
      "JJS"-> Seq("A"),
      "JJR"-> Seq("A")
    )
    t2c shouldEqual inverted
  }

  it should "retrieve Token" in {
    val text = """Développeur Web & Mobile
                 |- Weever : Site web générique permettant la structuration et la valorisation des données
                 |artistiques, utilisé notamment par Fondation d'entreprise Galeries Lafayette. Rajout de diverses
                 |interfaces et fonctionnalitées variées.
                 |- BioMétéo : Développement d’un site web présentant la biodiversité en Dordogne ainsi que la
                 |météo locale.
                 |ReactJs, Graphql, Apollo-GraphQL, Docker, Git, MaterialUI""".stripMargin
    val multiWordExtraction = new NPFSTMultiWordExtraction("fr")
    multiWordExtraction.init
    val tokens = multiWordExtraction.tagText(text)
    tokens.foreach(println(_))
    tokens.size should be > 0
  }

  it should "retrieve the multiword" in {
    val multiWordExtraction = new NPFSTMultiWordExtraction("fr")
    multiWordExtraction.init
    val startTimeMillis = System.currentTimeMillis()
    val text = """Nous recherchons pour notre agence de Sophia Antipolis, un Administrateur système Linux. Rattaché(e) à l'un de nos projets, vous serez en charge de :
                 |
                 |- Installer, configurer (avec Ansible, Git / Gitlab CI) et optimiser des systèmes Linux (Debian, Ubuntu et RedHat), Apache Web Server et bases de données (MySQL, Cassandra, MongoDB..),
                 |- Debugger et résoudre des incidents de niveau 3,
                 |- Superviser des systèmes et applicatifs (Xymon),
                 |- Développer des scripts d'automatisation et déploiement (Python, Bash..),
                 |- Renforcer la sécurité, patcher les serveurs et les applications,
                 |- Travailler sur la configuration réseau des machines (Bonding, routes, DNS, SSH..),
                 |- S'occuper de la migration des socles obsolètes,
                 |- Déployer / migrer des applications en mode micro-services sur Docker / Swarm ou CloudFoundry
                 |
                 |Diplômé d'un Bac + 5 (Grandes Ecoles, Universités), vous justifiez au minimum de 5 années d'expérience.""".stripMargin

    /*
      phraseTokspans.zipWithIndex.map { case (tokenSpan, position) =>
    val words = tokens.slice(tokenSpan._1, tokenSpan._2)
    // case class MultiWord(numTokens: Int, tokensSpans: Seq[(Int, Int)], counts: Int, pos: Seq[String], tokens: Seq[String])
  }
  ???
     */
    val phrases: MultiWord = multiWordExtraction.phrases(text)
    val words = phrases.tokens
    val multiWords = phrases.tokensSpans.map { tokenSpan =>
      val multiWord = words.slice(tokenSpan._1, tokenSpan._2)
      multiWord.map(_.text).mkString(" ")
    }
    multiWords.foreach(println(_))
    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"the process took : ${durationSeconds} s - ${endTimeMillis - startTimeMillis} ms")
  }

}
