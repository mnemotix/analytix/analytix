/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.textprocessing

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers
import org.tartarus.snowball.ext.FrenchStemmer


class LuceneRawTextProcessingSpec extends AnyFlatSpec with  Matchers  {

  it should "stem a french word" in {
    val stemmer =  new FrenchStemmer()
    stemmer.setCurrent("antilopes")
    val stemming = stemmer.stem()
    println(stemming)
    val res = stemmer.getCurrent
    println(res)
  }

  it should "tokenize a sentence" in {
    val luceneRawTextProcessing = new LuceneRawTextProcessing("french")
    val list = luceneRawTextProcessing.words("voici un texte qui sert de test")
    list.foreach(println(_))
  }

  it should "stem a sentence" in {
    val luceneRawTextProcessing = new LuceneRawTextProcessing("french")
    val list = luceneRawTextProcessing.lemmas("voici des textes qui serviront de test spécialement et exceptionnellement pour voir si les choses fonctionnent.")
    println("voici des textes qui serviront de test spécialement et exceptionnellement pour voir si les choses fonctionnent.")
    println(list.mkString(" "))
  }

}