/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.textprocessing

import java.io.FileInputStream

import com.mnemotix.analytix.nlp.helpers.TextProcessingConfig
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer

class WordVectorizer(lang: String) {

  lazy val modelFile = new FileInputStream(TextProcessingConfig.wordsEmbeddingModel)
  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing(lang)
  lazy val model = WordVectorSerializer.readBinaryModel(modelFile, true, true)

  def vectorize(word: String): Array[Double] = {
    model.getWordVector(word)
  }

  def vectorize(listOfWords :Seq[String]): Seq[Array[Double]] = {
    listOfWords.map(vectorize(_))
  }

  def vectorMean(vectors :Seq[Array[Double]]): Array[Double] = {
    val nonNull: Seq[Array[Double]] = vectors.filterNot(_ == null)
    if (nonNull.size > 0) {
        nonNull reduceLeft { (x, y) =>
          x.lazyZip(y).map(_+_)
      }
    }.map(_ /nonNull.size.toDouble)
    else Array.ofDim(model.vectorSize())
  }
}