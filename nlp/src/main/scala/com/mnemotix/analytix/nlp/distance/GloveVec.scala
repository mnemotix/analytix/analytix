/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance

import java.io.File

import com.mnemotix.analytix.nlp.helpers.TextProcessingConfig
import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer
import org.nd4j.linalg.factory.Nd4j

class GloveVec(lang: String) {

  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing(lang)
  lazy val model = init

  def init = {
    Nd4j.getEnvironment().allowHelpers(false)
    lazy val gloveFile = new File(TextProcessingConfig.wordsEmbeddingModel)
    lazy val wordVectors = WordVectorSerializer.readWord2VecModel(gloveFile)
    WordVectorSerializer.writeWord2VecModel(wordVectors, new File(s"${TextProcessingConfig.wordsEmbeddingDirPath}glove.bin"))
    WordVectorSerializer.readWord2VecModel(new File(s"${TextProcessingConfig.wordsEmbeddingDirPath}glove.bin"))
  }

  def similarity(sentence1: String, sentence2: String): Double = {
    def similarityHelper(listsentence: Seq[String]): Array[Double] = {
      val listVector = listsentence.map { word =>
        model.getWordVector(word)
      }.filterNot(_ == null)
      if (listVector.size > 0) {
        listVector reduceLeft { (x, y) =>
          x.lazyZip(y).map(_ + _)
        }
      }
      else Array.ofDim(model.vectorSize())
    }

    val listsentence1 = synaptixRawTextProcessing.words(sentence1)
    val listsentence2 = synaptixRawTextProcessing.words(sentence2)

    val vec1 = similarityHelper(listsentence1)
    val vec2 = similarityHelper(listsentence2)

    val sim = VectorSimilarity.cosineSimilarity(vec1, vec2)
    if (sim.isNaN) 0.0 else sim
  }
}