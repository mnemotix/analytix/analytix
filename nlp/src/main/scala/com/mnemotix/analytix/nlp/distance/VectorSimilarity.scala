/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance

import org.nd4j.linalg.factory.Nd4j

object VectorSimilarity {

  def cosineSimilarity(input1Vector: Array[Float], input2Vector: Array[Float]): Double = {
    cosineSimilarity(input1Vector.map(_.toDouble), input2Vector.map(_.toDouble))
  }

  def cosineSimilarity(input1Vector: Array[Double], input2Vector: Array[Double]): Double = {
    val normA = norm(input1Vector)
    val normB = norm(input2Vector)
    val dProd = dotProduct(input1Vector,input2Vector)
    (dProd / (Math.sqrt(normA) * Math.sqrt(normB)))
  }

  def norm(inputVector: Array[Double]): Double = {
    inputVector.foldLeft(0.0)(_ + Math.pow(_, 2))
  }

  def dotProduct(input1Vector: Array[Double], input2Vector: Array[Double]): Double = {
    val vec1Avg = Nd4j.create(input1Vector, input1Vector.length)
    val vec2Avg = Nd4j.create(input2Vector, input2Vector.length)
    Nd4j.getBlasWrapper().dot(vec1Avg, vec2Avg)
  }
}