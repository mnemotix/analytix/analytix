/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.keywordExtraction

import com.mnemotix.analytix.commons.utils.CollectionUtils
import com.mnemotix.analytix.nlp.helpers.MultiWordExtractionNPFSTConfig
import com.mnemotix.analytix.nlp.model.{MultiWord, Pos, Token, Word}
import com.mnemotix.analytix.nlp.textprocessing.StandfordRawTextProcessing

/**
 *
 * Abram Handler, Matthew J. Denny, Hanna Wallach, Brendan O’Connor. Bag of What? Simple Noun Phrase Extraction for Text Analysis
 *
 * https://github.com/slanglab/phrasemachine/blob/41cec3b727f854aa6ef8ab0b45228c3216ff0d4a/py/phrasemachine/phrasemachine.py#L80
 *
 */

class NPFSTMultiWordExtraction(lang: String) {

  lazy val standfordRawTextProcessing = new StandfordRawTextProcessing(lang)
  lazy val simpleNPGrammar = if (!MultiWordExtractionNPFSTConfig.npfstGrammar.isEmpty) "(A|N)*N(PD*(A|N)*N)*" else MultiWordExtractionNPFSTConfig.npfstGrammar
  lazy val minlen = if (MultiWordExtractionNPFSTConfig.npfstMinlen.toString.isEmpty) 2 else MultiWordExtractionNPFSTConfig.npfstMinlen
  lazy val maxlen = if (!MultiWordExtractionNPFSTConfig.npfstMaxlen.toString.isEmpty) 8 else MultiWordExtractionNPFSTConfig.npfstMaxlen
  lazy val coarseMap = Map(
    "A" -> "JJ JJR JJS CoarseADJ CD CoarseNUM A ADJ".split(" ").toSeq,
    "D" -> "DT CoarseDET D DET".split(" ").toSeq,
    "P" -> "IN TO CoarseADP P ADP".split(" ").toSeq,
    "N" -> "NN NNS NNP NNPS FW CoarseNOUN N NOUN PROPN ^".split(" ").toSeq
  )
  lazy val tag2coarse = CollectionUtils.invertMap(coarseMap)

  def init: Unit = {
    standfordRawTextProcessing.init
  }

  def tagText(text: String): Seq[Token] = {
    standfordRawTextProcessing.tokens(text)
  }

  def coarseTagStr(posSeq: Seq[Pos]): String = {
    posSeq.filter(_.pos.isDefined).flatMap { pos =>
      tag2coarse.getOrElse(pos.pos.get, Seq("O"))
    }.mkString
  }

  def extractNgramFilter(posSeq : Seq[Pos], regex: String, minlen: Int, maxlen: Int): Seq[(Int, Int)] = {
    val ss = coarseTagStr(posSeq)
    def gen(): Seq[(Int, Int)] = {
      ss.toSeq.zipWithIndex.map {
        case(_, s) => {
          val max = 1 + List(maxlen, ss.size - s).min
          (minlen until max).map { n =>
            val e = s + n
            val substring = ss.substring(s, e)
            if (substring.matches(regex+"$".r)) Some((s,e)) else None
          }
        }.flatten.toSeq
      }.flatten
    }
    gen()
  }

  def spanIsIncluded(phraseTokspans: Seq[(Int, Int)], tokspan: (Int, Int)): Boolean = {
    phraseTokspans.exists(phraseTokspan => phraseTokspan._1 <= tokspan._1 && phraseTokspan._2 > tokspan._2)
  }

  def textIsIncluded(phraseTokspans: Seq[(Int, Int)], tokens: Seq[Word]) = {
    def helper(multiWords: Seq[String], multiWord: String) = {
      multiWords.filterNot(_ == multiWord).exists {m => m.contains(multiWord)}
    }
    val multiWords: Seq[(String, (Int, Int))] = phraseTokspans.map { tokenSpan =>
      val multiWord = tokens.slice(tokenSpan._1, tokenSpan._2)
      (multiWord.map(_.text).mkString(" "), (tokenSpan._1, tokenSpan._2))
    }
    val listOfMultiWord = multiWords.map(_._1)
    multiWords.filterNot(multiWord => helper(listOfMultiWord, multiWord._1)).map(_._2)
  }

  def phrases(text: String): MultiWord = {
    val tokens: Seq[Word] = tagText(text).map(_.word)
    val postags = tagText(text).map(_.pos)
    val phraseTokspans: Seq[(Int, Int)] = extractNgramFilter(postags, simpleNPGrammar, minlen, maxlen)
    val phraseTokspansRes = phraseTokspans.filterNot(spanIsIncluded(phraseTokspans, _))
    val phraseTokspanLast = textIsIncluded(phraseTokspansRes, tokens)
    MultiWord(tokens.size, phraseTokspanLast, phraseTokspanLast.size, postags, tokens)
  }
}