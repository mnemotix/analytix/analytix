/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.helpers

import com.typesafe.config.{Config, ConfigFactory}

object TextProcessingConfig {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("nlp")).getOrElse(ConfigFactory.empty())

  lazy val stopWords = conf.getString("textprocessing.stopwords")
  lazy val wordsEmbeddingModel = conf.getString("wordEmbeddings.model.file.path")
  lazy val wordsEmbeddingDirPath = conf.getString("wordEmbeddings.dir.path")
}