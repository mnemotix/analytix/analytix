/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.helpers

class AnalytixSentenceExtractorHelper {

  def enSentenceExtractor(text: String) = {
    val alphabets = "([A-Za-z])"
    val prefixes = "(Mr|St|Mrs|Ms|Dr)\\."
    val suffixes = "(Inc|Ltd|Jr|Sr|Co)"
    val starters = "(Mr|Mrs|Ms|Dr|He\\s|She\\s|It\\s|They\\s|Their\\s|Our\\s|We\\s|But\\s|However\\s|That\\s|This\\s|Wherever)"
    val acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
    val websites = "[.](com|net|org|io|gov)"

    val res = text.replaceAll("\\n", " ")
      .replaceAll(prefixes, """$1<prd>""")
      .replaceAll(websites, """<prd>$1""")
      .replaceAll("Ph\\.D\\.", "Ph<prd>D<prd>")
      .replaceAll("\\s" + alphabets + "\\. ", """ $1<prd> """)
      .replaceAll(acronyms+" "+starters, """$1<stop> $2""")
      .replaceAll(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]", """$1<prd>$2<prd>""")
      .replaceAll(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]", """$1<prd>$2<prd>$3<prd>""")
      .replaceAll(alphabets + "[.]" + alphabets + "[.]", """$1<prd>$2<prd>""")
      .replaceAll(" "+suffixes+"[.] "+starters, """ $1<stop> $2""")
      .replaceAll(" "+suffixes+"[.]", """ $1<prd>""")
      .replaceAll(" " + alphabets + "[.]", """ $1<prd>""")
      .replaceAll(".”", "”.")
      .replaceAll(".\"","\".")
      .replaceAll("!\"","\"!")
      .replaceAll("\\?\"","\"?")
      .replaceAll("\\.",".<stop>")
      .replaceAll("\\?","?<stop>")
      .replaceAll("!","!<stop>")
      .replaceAll("<prd>",".")

    val sentences = res.split("<stop>").map(_.trim)
    sentences
  }

  def frSentenceExtractor(text: String) = {
    val alphabets = "([A-Za-z])"
    val prefixes = "(Mr|St|Mrs|Ms|Dr|M|Mme|Mlle)\\."
    val suffixes = "(Inc|Ltd|Jr|Sr|Co)"
    val starters = "(Mr|Mrs|Ms|Dr|Il|Elle|Ils|Elles|Leur|Nos|Nous|Mais|Pourtant|Ceci|Ça)"
    val acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
    val websites = "[.](com|net|org|io|gov)"

    val res = text.replaceAll("\\n", " ")
      .replaceAll(prefixes, """$1<prd>""")
      .replaceAll(websites, """<prd>$1""")
      .replaceAll("Ph\\.D\\.", "Ph<prd>D<prd>")
      .replaceAll("\\s" + alphabets + "\\. ", """ $1<prd> """)
      .replaceAll(acronyms+" "+starters, """$1<stop> $2""")
      .replaceAll(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]", """$1<prd>$2<prd>""")
      .replaceAll(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]", """$1<prd>$2<prd>$3<prd>""")
      .replaceAll(alphabets + "[.]" + alphabets + "[.]", """$1<prd>$2<prd>""")
      .replaceAll(" "+suffixes+"[.] "+starters, """ $1<stop> $2""")
      .replaceAll(" "+suffixes+"[.]", """ $1<prd>""")
      .replaceAll(" " + alphabets + "[.]", """ $1<prd>""")
      .replaceAll(".”", "”.")
      .replaceAll(".\"","\".")
      .replaceAll("!\"","\"!")
      .replaceAll("\\?\"","\"?")
      .replaceAll("\\.",".<stop>")
      .replaceAll("\\?","?<stop>")
      .replaceAll("!","!<stop>")
      .replaceAll("<prd>",".")

    val sentences = res.split("<stop>").map(_.trim)
    sentences
  }
}
