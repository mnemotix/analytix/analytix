/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.keywordExtraction

import com.mnemotix.analytix.nlp.model.Token
import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing

import scala.collection.immutable.ListMap

class TermFrequency(language: String) {
  lazy val analytixRawTextProcessing = new AnalytixRawTextProcessing(language)

  def extract(text: String): Map[String, Double] = {
    val tokensSentences: Seq[Seq[Token]] = preprocessing(text)
    val sentenceString = tokensSentences.map(_.map(_.lemma.text.get))
    val frTerm: Map[String, Int] = frequentTerm(sentenceString.flatten)

    val tfResult = frTerm.map { frt =>
      (retrieveOriginalText(frt._1, tokensSentences.flatten).word.text, frt._2.toDouble)
    }
    ListMap(tfResult.toSeq.sortWith(_._2 > _._2):_*).toMap
  }

  def retrieveOriginalText(lemma: String, tokens: Seq[Token]): Token = {
    tokens.filter { token =>
      token.lemma.text.get == lemma
    }.last
  }

  def preprocessing(text: String): Seq[Seq[Token]] = {
    val sentence = analytixRawTextProcessing.sentences(text).filter(sentence => sentence.tokens.size > 3)
    sentence.map(_.tokens.filter(_.lemma.text.isDefined))
  }

  def frequentTerm(words: Seq[String]): Map[String, Int] = {
    val count = analytixRawTextProcessing.wordCount(words)
    val countOrder = ListMap(count.toSeq.sortWith(_._2 > _._2):_*)
    val toSlice = ((countOrder.size * 30) / 100).ceil.toInt
    countOrder.take(toSlice)
  }
}