/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.phonetic


/**
 *  Lawrence Philips Double Metaphone
 */

class DoubleMetaphone {
  val doubleMetaphone = new org.apache.commons.codec.language.DoubleMetaphone
  doubleMetaphone setMaxCodeLen 5

  def sound(s: String) = {
    doubleMetaphone.doubleMetaphone(s)
  }

  def encode(s: String) = {
    doubleMetaphone.encode(s)
  }

  def double(s:String) = {
    doubleMetaphone.doubleMetaphone(s, true)
  }
}

object DoubleMetaphone {
  val doubleMetaphone = new org.apache.commons.codec.language.DoubleMetaphone
  doubleMetaphone setMaxCodeLen 5

  def sound(s: String) = {
    doubleMetaphone.doubleMetaphone(s)
  }

  def encode(s: String) = {
    doubleMetaphone.encode(s)
  }

  def double(s: String) = {
    val normal = doubleMetaphone.doubleMetaphone(s)
    val alternate = doubleMetaphone.doubleMetaphone(s, true)
    if (normal == alternate) {(normal, None) }
    else (normal, Some(alternate))
  }
}
