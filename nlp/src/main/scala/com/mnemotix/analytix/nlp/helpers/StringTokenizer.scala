/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.helpers

sealed trait StringTokenizer extends Tokenizer[Array[Char]] {
  def tokenize(a: String): Option[Array[String]]
}

final case class NGramTokenizer(n: Int) extends StringTokenizer {
  override def tokenize(a: Array[Char]): Option[Array[Array[Char]]] =
    if (n <= 0 || a.length < n) None
    else Some(sequence(a, Array.empty[Array[Char]], n))

  override def tokenize(a: String): Option[Array[String]] = tokenize(a.toCharArray).map(_.map(_.mkString))

  @annotation.tailrec
  private val sequence: ((Array[Char], Array[Array[Char]], Int) => Array[Array[Char]]) = (i, o, n) =>
    if (i.length <= n) o :+ i
    else sequence(i.tail, o :+ i.take(n), n)
}