/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.stemmers.french

import org.graalvm.compiler.word.Word

/**
 * Talisman stemmers/porter
 * =========================
 *
 * The classical Porter stemmer.
 *
 * [Reference]:
 * http://tartarus.org/martin/PorterStemmer/
 *
 * [Article]:
 * C.J. van Rijsbergen, S.E. Robertson and M.F. Porter, 1980. New models in
 * probabilistic information retrieval. London: British Library.
 * (British Library Research and Development Report, no. 5587).
 */

class Porter {

  val VOWELS = Set("aeiouyâàëéêèïîôûù")

  val STEP1_SUFFIXES = Seq("issements", "issement", "atrices", "atrice",
  "ateurs", "ations", "logies", "usions", "utions", "ements", "amment", "emment",
  "ances", "iqUes", "ismes", "ables", "istes", "ateur", "ation", "logie", "usion", "ution",
  "ences", "ement", "euses", "ments", "ance", "iqUe", "isme", "able", "iste", "ence",
  "ités", "ives", "eaux", "euse", "ment", "eux", "ité", "ive", "ifs", "aux", "if")

  val STEP1_SET1 = Set(Seq("ance", "iqUe", "isme", "able", "iste", "eux", "ances", "iqUes", "ismes", "ables", "istes"))

  val STEP1_SET2 = Set(Seq("atrice", "ateur", "ation", "atrices", "ateurs", "ations"))

  val STEP1_SET3 = Set(Seq("usion", "ution", "usions", "utions"))

  val STEP1_SET4 = Set(Seq("if", "ive", "ifs", "ives"))

  val STEP2A_SUFFIXES = Seq("issaIent", "issantes", "iraIent", "issante",
  "issants", "issions", "irions", "issais", "issait", "issant", "issent", "issiez", "issons",
  "irais", "irait", "irent", "iriez", "irons", "iront", "isses", "issez", "îmes",
  "îtes", "irai", "iras", "irez", "isse", "ies", "ira", "ît", "ie", "ir", "is", "it", "i")

  val STEP2B_SUFFIXES = Seq("eraIent", "assions", "erions", "assent", "assiez", "èrent", "erais", "erait",
  "eriez", "erons", "eront", "aIent", "antes", "asses", "ions", "erai", "eras", "erez",
  "âmes", "âtes", "ante", "ants", "asse", "ées", "era", "iez", "ais",
  "ait", "ant", "ée", "és", "er", "ez", "ât", "ai", "as", "é", "a")

  val STEP2B_SET1 = Set(Seq("eraIent", "erions", "èrent",
  "erais", "erait", "eriez", "erons", "eront", "erai", "eras",
  "erez", "ées", "era", "iez", "ée", "és", "er", "ez", "é"))

  val STEP2B_SET2 = Set(Seq("assions", "assent", "assiez",
  "aIent", "antes", "asses", "âmes", "âtes", "ante",
  "ants", "asse", "ais", "ait", "ant", "ât", "ai", "as", "a"))

  val STEP4_SUFFIXES = Seq("ière", "Ière", "ion", "ier", "Ier", "e", "ë")
  
  val STEP4_SET1 = Set("aiouès")
  
  val STEP4_SET2 = Set(Seq("ier", "ière", "Ier", "Ière"))

  def findR1R2(word: String)  = {
    val wordArray: Array[Char] = word.toCharArray
    val res1 = wordArray.zipWithIndex.find(c => (!VOWELS.contains(c._1.toString) && VOWELS.contains(wordArray(c._2- 1).toString)))
    val r1 = if (res1.isDefined) wordArray.drop(res1.get._2 + 1).mkString else ""

    val r1WordArray = r1.toCharArray
    val res2 = r1WordArray.zipWithIndex.find(c => (!VOWELS.contains(c._1.toString) && VOWELS.contains(r1WordArray(c._2- 1).toString)))
    val r2 = if (res2.isDefined) wordArray.drop(res2.get._2 + 1).mkString else ""

    (r1, r2)
  }

  def findRV(word: String) = {
    val wordArray: Array[Char] = word.toCharArray
    if (word.length >= 2 && ("^(?:par|col|tap)".r.findFirstIn(word).isDefined || (VOWELS.contains(wordArray(0).toString) && VOWELS.contains(wordArray(1).toString)))) {
      wordArray.drop(3).mkString
    }
    else {
      val res = wordArray.zipWithIndex.find(c => VOWELS.contains(c._1.toString))
      if (res.isDefined) wordArray.drop(res.get._2 + 1).mkString else ""
    }
  }

  def replaceLetter(stem: String, index: Int, replacement: String): String = {
    stem.substring(0, index) + replacement + stem.drop(index + 1)
  }

  def suffixStem(stem: String, oldSuffix: String, newSuffix: String): String = {
    val length = oldSuffix.size
    stem.slice(0, -length) + newSuffix
  }

  def porter(word: String): String = {
    val stem = word.toLowerCase.replaceAll("/qu/g", "qU")

    val step1Success = false
    val rvEndingFound = false
    val step2aSuccess = false
    val step2bSuccess = false

    stem.zipWithIndex.map { c =>
      val letter = c._1.toString
      val previousLetter =  c._2
    }
???
  }
}

/*
  for (let i = 1, l = word.length - 1; i < l; i++) {
    const letter = word[i],
          previousLetter = word[i - 1],
          nextLetter = word[i + 1];

    if ((letter === 'u' ||
         letter === 'i') &&
        VOWELS.has(previousLetter) &&
        VOWELS.has(nextLetter)) {
      word = replaceLetter(word, i, letter.toUpperCase());
    }

    else if (letter === 'y' &&
             (VOWELS.has(previousLetter) ||
              VOWELS.has(nextLetter))) {
      word = replaceLetter(word, i, 'Y');
    }
  }
 */