/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance.cpmerge

import com.mnemotix.analytix.nlp.helpers.{CPMergeConfig, NGramTokenizer}

/**
 * The algorithm CPMerge can be found in the paper : Naoaki Okazaki, Jun’ichi Tsujii. Simple and Efficient Algorithm
 * for Approximate Dictionary Matching [https://aclanthology.org/C10-1096.pdf]
 *
 * https://github.com/dav009/FuzzyDict
 *
 */

class SimString(val words: Vector[String]) {

  private val wordsToIndex: Map[String, Int] = words.zipWithIndex.toMap

  private val ngramSizeWordTriples: Seq[(String, Int, Int)] = words.map(getFeature(_)).zipWithIndex.flatMap {
    case (ngrams: List[String], wordIndex: Int) =>
      ngrams.map {
        ngram => (ngram, ngrams.size, wordIndex)
      }
  }

  private val lookupTable: Map[String, Set[Int]] = ngramSizeWordTriples.map{
    triple =>
      // ngram_size => wordIndex
      (triple._1 + "_" + triple._2, triple._3)
  }.groupBy(_._1)
    .view.mapValues(_.map(_._2).toSet).toMap

  private def getFeature(word:String): List[String] = {
    NGramTokenizer(CPMergeConfig.ngram.toInt).tokenize(s"${CPMergeConfig.ngramDelim}$word${CPMergeConfig.ngramDelim}").toList.flatten
  }

  private def getMatches(size:Int, ngram:String): Option[Set[Int]] ={
    lookupTable.get(ngram+"_"+size)
  }

  private def overlapJoin(features:List[String], minOverlap:Int, sizeOfQuery:Int): Set[Int] = {
    def countCocurrances(list:List[Int]): Map[Int, Int]={
      list.groupBy(i => i).map(t => (t._1, t._2.length) ).toMap
    }
    val candidates = features.map(getMatches(sizeOfQuery, _)).flatten.sortBy(_.size)
    val candidatesCounts =  candidates.slice(0, features.size - minOverlap + 1).map(_.toList).flatten
    val narrowedCandidatesSet = candidatesCounts.toSet
    val extraCounts = candidates.slice(features.size - minOverlap + 1, features.size).flatMap{
      currentMatches =>
        narrowedCandidatesSet.toList.flatMap{
          word =>
            if (currentMatches.contains(word))
              Some(word)
            else
              None
        }
    }
    val matches = countCocurrances(candidatesCounts ++ extraCounts).filter{ t => minOverlap <= t._2 }.keySet
    matches
  }

  def search(query:String, threshold:Double,  measure:Measure): Set[String] = {
    val features = getFeature(query)
    val matchesSize = Range(measure.min(threshold, features.size), measure.max(threshold, features.size) + 1)
    val matches = matchesSize.flatMap{
      l =>
        val minOverlap = measure.t(threshold, features.toSet.size, l)
        overlapJoin(features, minOverlap, l)
    }.toSet
    matches.map(this.words(_))
  }
}

object SimString {
  def apply(words:List[String])= new SimString(words.toVector)
}
