/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.textprocessing

import com.mnemotix.analytix.nlp.model.{Document, Lemma, Ner, Pos, Sentence, Token, Word}

import java.io.StringReader
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.tartarus.snowball.SnowballProgram
import org.tartarus.snowball.ext.{EnglishStemmer, FrenchStemmer}

import scala.collection.mutable.ListBuffer

class LuceneRawTextProcessing(language: String) extends RawTextProcessing {

  def stemmer: SnowballProgram = {
    lang match {
      case lang if (lang == "english") => new EnglishStemmer()
      case lang if (lang == "french") => new FrenchStemmer()
      case _ => new FrenchStemmer()
    }
  }

  val analyzer = new StandardAnalyzer()

  override def init: Unit = {
    val l = language.toLowerCase match {
      case "french" | "fr" => {
        "french"
      }
      case "english" | "en" => {
        "english"
      }
      case _ => {
        "french"
      }
    }
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing, with ${l} language.")
  }

  def lang: String = {
    language.toLowerCase match {
      case "french" | "fr" => {
        "french"
      }
      case "english" | "en" => {
        "english"
      }
      case _ => {
        "french"
      }
    }
  }

  override def document(text: String): Document = ???

  override def sentences(text: String): Seq[Sentence] = {
    // https://github.com/apache/lucene/blob/main/lucene/analysis/common/src/test/org/apache/lucene/analysis/util/TestSegmentingTokenizerBase.java
    ???
  }

  override def tokens(text: String): Seq[Token] = {
    words(text).map { word =>
      Token(
        Word(word),
        Lemma(Some(lemma(word))),
        Ner(None),
        Pos(None)
      )
    }
  }

  override def lemma(word: String): String = {
    val stem = stemmer
    stem.setCurrent(word)
    stem.getCurrent
  }

  override def shutdown: Unit = {}


  def words(text: String): Seq[String] = {
    val result = new ListBuffer[String]()
    val stream = analyzer.tokenStream(null, new StringReader(text))
    stream.reset()
    while (stream.incrementToken()) {
      result.append(stream.getAttribute(classOf[CharTermAttribute]).toString)
    }
    result.toSeq
  }

  def lemmas(text: String): Seq[String] = {
    words(text).map { word =>
      val stem = stemmer
      stem.setCurrent(word)
      stem.getCurrent
    }
  }
}