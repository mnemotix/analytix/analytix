/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance

import java.io.File

import com.mnemotix.analytix.nlp.helpers.TextProcessingConfig
import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing
import org.deeplearning4j.models.fasttext.FastText

class FastTextVec(lang: String) {

  lazy val modelFile = new File(TextProcessingConfig.wordsEmbeddingModel)
  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing(lang)
  lazy val model = new FastText(modelFile)

  def similarity(sentence1: String, sentence2: String): Double = {
    def similarityHelper(listsentence: Seq[String]): Array[Double] = {
      val listVector = listsentence.map { word =>
        model.getWordVector(word)
      }.filterNot(_ == null)
      if (listVector.size > 0) {
        listVector reduceLeft { (x, y) =>
          x.lazyZip(y).map(_ + _)
        }
      }
      else Array.ofDim(model.vectorSize())
    }

    val listsentence1 = synaptixRawTextProcessing.words(sentence1)
    val listsentence2 = synaptixRawTextProcessing.words(sentence2)

    val vec1 = similarityHelper(listsentence1)
    val vec2 = similarityHelper(listsentence2)

    val sim = VectorSimilarity.cosineSimilarity(vec1, vec2)
    if (sim.isNaN) 0.0 else sim
  }
}