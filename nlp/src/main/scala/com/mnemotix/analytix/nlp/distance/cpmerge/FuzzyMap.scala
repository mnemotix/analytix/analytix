/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance.cpmerge

/**
 * The algorithm CPMerge can be found in the paper : Naoaki Okazaki, Jun’ichi Tsujii. Simple and Efficient Algorithm
 * for Approximate Dictionary Matching [https://aclanthology.org/C10-1096.pdf]
 *
 * https://github.com/dav009/FuzzyDict
 *
 */

class FuzzyMap[T](tuples: List[(String, T)]) extends Iterable[(String, T)]{

  private val hashMap = tuples.toMap
  private val simString = SimString(tuples.map(_._1))

  private def search(query:String, threshold:Double,  measure:Measure): Seq[String] ={
    simString.search(query, threshold, measure).toList
  }

  def getMatches(query:String, threshold:Double,  measure:Measure): Option[Seq[(String, T)]] ={
    val setFuzzyMatches = search(query, threshold, measure)
    val pairs = setFuzzyMatches.map{
      matchedString => ( matchedString, hashMap.get(matchedString).get )
    }
    if (pairs.size == 0)
      None
    else
      Some(pairs)
  }

  def get(query:String, threshold:Double,  measure:Measure): Option[Seq[T]] ={
    val setFuzzyMatches = search(query, threshold, measure)
    val values = setFuzzyMatches.flatMap(hashMap.get(_))
    if (values.size == 0)
      None
    else
      Some(values)
  }
  
  override def foreach[U](f: ((String, T)) => U): Unit = hashMap.foreach(f)

  override def iterator: Iterator[(String, T)] = hashMap.iterator
}

object FuzzyMap {
  def apply[T](tuples:List[(String, T)]) = new FuzzyMap(tuples)
}
