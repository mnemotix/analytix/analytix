/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.connector

import java.io.{File, FileInputStream}

import com.typesafe.scalalogging.LazyLogging
import org.apache.tika.Tika
import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.AutoDetectParser
import org.apache.tika.sax.BodyContentHandler

class TikaFileContent extends LazyLogging {

  val tika = new Tika()

  def parse(file: File): Option[String] = {
    val parser = new AutoDetectParser()
    val handler = new BodyContentHandler()
    val metada = new Metadata()

    try {
      val stream: FileInputStream = new FileInputStream(file)
      parser.parse(stream, handler, metada)
      Some(handler.toString.trim)
    }
    catch {
      case exception: Exception => {
        logger.error(s"Error arrised when parse file", exception)
        None
      }
    }
    finally {
     None
    }
  }
}