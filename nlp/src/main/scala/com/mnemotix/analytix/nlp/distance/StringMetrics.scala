package com.mnemotix.analytix.nlp.distance

import com.mnemotix.analytix.nlp.helpers.NGramTokenizer

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * https://github.com/rockymadden/stringmetric
 * https://github.com/mpkorstanje/simmetrics
 */

object StringMetrics {

  def jaccard(s1: String, s2: String) = {
    val size_in = s1.intersect(s2).size
    1 - (size_in.toDouble / (s1.length + s2.length - size_in).toDouble)
  }

  def jaroWinkler(s1: String, s2: String): Double = {
    val s1_len = s1.length
    val s2_len = s2.length
    if (s1_len == 0 && s2_len == 0) return 1.0
    val match_distance = Math.max(s1_len, s2_len) / 2 - 1
    val s1_matches = Array.ofDim[Boolean](s1_len)
    val s2_matches = Array.ofDim[Boolean](s2_len)
    var matches = 0
    for (i <- 0 until s1_len) {
      val start = Math.max(0, i - match_distance)
      val end = Math.min(i + match_distance + 1, s2_len)
      start until end find { j => !s2_matches(j) && s1(i) == s2(j) } match {
        case Some(j) =>
          s1_matches(i) = true
          s2_matches(j) = true
          matches += 1
        case None =>
      }
    }
    if (matches == 0) return 0.0
    var t = 0.0
    var k = 0
    0 until s1_len filter s1_matches foreach { i =>
      while (!s2_matches(k)) k += 1
      if (s1(i) != s2(k)) t += 0.5
      k += 1
    }

    val m = matches.toDouble
    1 - (m / s1_len + m / s2_len + (m - t) / m) / 3.0
  }

  def levenshtein(s1: String, s2: String): Double = {
    import scala.math._
    def minimum(i1: Int, i2: Int, i3: Int) = min(min(i1, i2), i3)

    val dist = Array.tabulate(s2.length + 1, s1.length + 1) { (j, i) => if (j == 0) i else if (i == 0) j else 0 }
    for (j <- 1 to s2.length; i <- 1 to s1.length)
      dist(j)(i) = if (s2(j - 1) == s1(i - 1)) dist(j - 1)(i - 1)
      else minimum(dist(j - 1)(i) + 1, dist(j)(i - 1) + 1, dist(j - 1)(i - 1) + 1)
    val result= dist(s2.length)(s1.length)
    val len =  max(s1.length, s2.length)
    1 - ((len.toDouble - result.toDouble) / len.toDouble)
  }

  type MatchTuple[T] = (Array[T], Array[T])
  type CompareTuple[T] = (Array[T], Array[T])
  private val scoreMatches: (MatchTuple[String] => Int) = (mt) => mt._1.intersect(mt._2).length

  def ngram(s1: String, s2: String, n: Int): Option[Double] = {
    val a = s1.toCharArray
    val b = s2.toCharArray
    if (n <= 0 || a.length < n || b.length < n) None
    else if (a.sameElements(b)) Some(0d)
    else NGramTokenizer(n).tokenize(a).flatMap { ca1bg =>
      NGramTokenizer(n).tokenize(b).map { ca2bg =>
        val ms = scoreMatches((ca1bg.map(_.mkString), ca2bg.map(_.mkString)))
        1 - (ms.toDouble / math.max(ca1bg.length, ca2bg.length))
      }
    }
  }

  def diceSorensen(s1: String, s2: String, n: Int) = {
    val a = s1.toCharArray
    val b = s2.toCharArray
    if (n <= 0 || a.length < n || b.length < n) None
    else if (a.sameElements(b)) Some(0d)
    else NGramTokenizer(n).tokenize(a).flatMap { ca1bg =>
      NGramTokenizer(n).tokenize(b).map { ca2bg =>
        val ms = scoreMatches(ca1bg.map(_.mkString), ca2bg.map(_.mkString))
        1 - ((2d * ms) / (ca1bg.length + ca2bg.length))
      }
    }
  }

  def ratcliffObershelp(s1: String, s2: String) = {
    val a = s1.toCharArray
    val b = s2.toCharArray

    if (a.length == 0 || b.length == 0) None
    else if (a.sameElements(b)) Some(0d)
    else Some(1 - (2d * commonSequences(a, b).foldLeft(0)(_ + _.length) / (a.length + b.length)))
  }

  private def longestCommonSubsequence(ct: CompareTuple[Char]) = {
    val m = Array.ofDim[Int](ct._1.length + 1, ct._2.length + 1)
    var lrc = (0, 0, 0) // Length, row, column.

    for (r <- 0 to ct._1.length - 1; c <- 0 to ct._2.length - 1) {
      if (ct._1(r) == ct._2(c)) {
        val l = m(r)(c) + 1
        m(r + 1)(c + 1) = l
        if (l > lrc._1) lrc = (l, r + 1, c + 1)
      }
    }

    lrc
  }

  private def commonSequences : (CompareTuple[Char] => Array[Array[Char]]) = (ct) => {
    val lcs = longestCommonSubsequence(ct)

    if (lcs._1 == 0) Array.empty
    else {
      val sct1 = (ct._1.take(lcs._2 - lcs._1), ct._1.takeRight(ct._1.length - lcs._2))
      val sct2 = (ct._2.take(lcs._3 - lcs._1), ct._2.takeRight(ct._2.length - lcs._3))

      Array(ct._1.slice(lcs._2 - lcs._1, lcs._2)) ++
        commonSequences(sct1._1, sct2._1) ++
        commonSequences(sct1._2, sct2._2)
    }
  }
}