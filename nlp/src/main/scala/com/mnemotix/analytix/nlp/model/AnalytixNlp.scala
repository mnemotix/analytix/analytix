/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.model

case class Word(text: String)

case class Lemma(text: Option[String])

case class Ner(ner: Option[String])

case class Pos(pos: Option[String])

case class Token(word: Word, lemma: Lemma, ner: Ner, pos: Pos)

case class Sentence(text: String, tokens: Seq[Token])

case class Document(text: String, sentences: Seq[Sentence])