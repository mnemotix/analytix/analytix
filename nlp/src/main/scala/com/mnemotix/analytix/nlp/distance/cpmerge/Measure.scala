/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.distance.cpmerge

/**
 * The algorithm CPMerge can be found in the paper : Naoaki Okazaki, Jun’ichi Tsujii. Simple and Efficient Algorithm
 * for Approximate Dictionary Matching [https://aclanthology.org/C10-1096.pdf]
 *
 * This trait represent table 1 in the paper. Condition for each similarity measure
 *
 * https://github.com/dav009/FuzzyDict
 *
 */

trait Measure {
  def t(treshold: Double, sizeOfA: Int, sizeOfB: Int): Int
  def min(treshold: Double, sizeOfA: Int): Int
  def max(treshold: Double, sizeOfA: Int): Int
}

object Cosine extends Measure {
  override def t(threshold:Double, sizeOfFeaturesA:Int, sizeOfFeaturesB:Int): Int = {
    Math.ceil(threshold * Math.sqrt(sizeOfFeaturesA * sizeOfFeaturesB)).toInt
  }

  override def max(threshold:Double, sizeOfFeaturesA:Int): Int = {
    Math.floor(sizeOfFeaturesA/(threshold * threshold)).toInt
  }

  override def min(threshold:Double, sizeOfFeaturesA:Int): Int = {
    Math.ceil(threshold * threshold * sizeOfFeaturesA).toInt
  }
}

object Jaccard extends Measure{
  override def t(threshold:Double, sizeOfFeaturesA:Int, sizeOfFeaturesB:Int): Int={
    Math.ceil( (threshold*(sizeOfFeaturesA + sizeOfFeaturesB)) / (1 + threshold)).toInt
  }

  override def max(threshold:Double, sizeOfFeaturesA:Int):Int={
    Math.floor(sizeOfFeaturesA/threshold).toInt
  }

  override def min(threshold:Double, sizeOfFeaturesA:Int):Int={
    Math.ceil(threshold * sizeOfFeaturesA).toInt
  }
}

object Dice extends Measure{
  override def t(threshold:Double, sizeOfFeaturesA:Int, sizeOfFeaturesB:Int):Int={
    Math.ceil( 0.5 *  threshold * (sizeOfFeaturesA + sizeOfFeaturesB) ).toInt
  }

  override def max(threshold:Double, sizeOfFeaturesA:Int):Int={
    Math.floor( ((2-threshold)/threshold) * sizeOfFeaturesA ).toInt
  }

  override def min(threshold:Double, sizeOfFeaturesA:Int):Int={
    Math.ceil( (threshold/(2-threshold)) * sizeOfFeaturesA ).toInt
  }
}