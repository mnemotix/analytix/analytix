/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.textprocessing

import com.mnemotix.analytix.commons.utils.TextUtils
import com.mnemotix.analytix.nlp.helpers.{AnalytixSentenceExtractorHelper, TextProcessingConfig}
import com.mnemotix.analytix.nlp.model.{Document, Lemma, Ner, Pos, Sentence, Token, Word}

import java.util.regex.Pattern
import com.mnemotix.analytix.nlp.stemmers.french.Carry
import com.mnemotix.synaptix.core.nlp.PorterStemmer

import scala.io.Source

class AnalytixRawTextProcessing(language: String) extends RawTextProcessing {

  lazy val frPattern: Pattern = Pattern.compile("[\\d:,\"\'\\`\\_\\|?!\n\r@;]+")

  override def init: Unit = {
    val l = language.toLowerCase match {
      case "french" | "fr" => {
        "french"
      }
      case "english" | "en" => {
        "english"
      }
      case _ => {
        "french"
      }
    }
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing, with ${l} language.")
  }

  def lang: String = {
    language.toLowerCase match {
      case "french" | "fr" => {
        "french"
      }
      case "english" | "en" => {
        "english"
      }
      case _ => {
        "french"
      }
    }
  }

  override def document(text: String): Document = {
    Document(text, sentences(text))
  }

  override def tokens(text: String): Seq[Token] = {
    words(text).map { w =>
      Token(
        Word(w),
        Lemma(Some(w)),
        Ner(None),
        Pos(None)
      )
    }
  }

  override def sentences(text: String): Seq[Sentence] = {
    val sentenceExtractorHelper = new AnalytixSentenceExtractorHelper()
    if (lang == "french") {
      val sentences = sentenceExtractorHelper.frSentenceExtractor(text)
      sentences.map { sentence =>
        Sentence(sentence, tokens(sentence))
      }
    }
    else {
      val sentences = sentenceExtractorHelper.enSentenceExtractor(text)
      sentences.map { sentence =>
        Sentence(sentence, tokens(sentence))
      }
    }
  }

  override def lemma(word: String): String = {
    if (lang == "french") {
      val carry = new Carry
      carry.carry(word)
    }
    else {
      PorterStemmer.stem(word)
    }
  }

  def words(text: String): Seq[String] = {
    val normalized = TextUtils.normalize(text)
    val input_text: String = normalized.trim.toLowerCase.replaceAll("\\{.*?\\}", "")
      .replaceAll("\\[.*?\\]", "").replaceAll("\\(.*?\\)", "")
      .replaceAll("[^A-Za-z0-9(),!?@\'\\`\"\\_\n]", " ")
      .replaceAll("[/]"," ").replaceAll(";"," ").replaceAll("[^\\w\\s]", "")
    input_text.split(" ").filter(label => label.length > 0).filterNot(label => stopWords.contains(label.trim))
      .filterNot(label => stopWordsConfig.contains(label.trim))
  }

  def lemmas(text: String): Seq[String] = {
    if (lang == "french") {
      val carry = new Carry
      words(text).map(w => carry.carry(w))
    }
    else {
      words(text).map(PorterStemmer.stem(_))
    }
  }

  def sentencesTextExtactor(text: String): Seq[String] = {
    //text.split("/(?<!\\..)([\\?\\!\\.]+)\\s(?!.\\.)/")
    val sentenceExtractorHelper = new AnalytixSentenceExtractorHelper()
    if (lang == "french") {
      sentenceExtractorHelper.frSentenceExtractor(text)
    }
    else if (lang == "english") {
      sentenceExtractorHelper.enSentenceExtractor(text)
    }
    else sentenceExtractorHelper.enSentenceExtractor(text)
  }

  def wordCount(text: String): Map[String, Int] = {
    words(text).groupBy((word) => word).view.mapValues(_.length).toMap
  }

  def wordCount(words: Seq[String]): Map[String, Int] = {
    words.groupBy(word => word).view.mapValues(_.length).toMap
  }

  def stopWords: Iterator[String] = {
    lang.toLowerCase match {
      case "french" => {
        val frenchFile = Source.fromResource("stopwords_fr.txt")
        frenchFile.getLines()
      }
      case "english" => {
        val englishFile = Source.fromResource("stopwords_en.txt")
        englishFile.getLines()
      }
      case _ => {
        val englishFile = Source.fromResource("stopwords_en.txt")
        englishFile.getLines()
      }
    }
  }

  def stopWordsConfig: Iterator[String] = {
    TextProcessingConfig.stopWords.split(",").map(_.trim).iterator
  }

  def filterWords(words: Iterable[String]) = {
    words.toSet.diff((stopWordsConfig ++ stopWords).toSet)
  }

  override def shutdown: Unit = {}
}