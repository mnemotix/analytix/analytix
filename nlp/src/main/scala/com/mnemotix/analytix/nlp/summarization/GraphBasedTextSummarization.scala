/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.summarization

import com.mnemotix.analytix.model.{WeightedEdge, WeightedGraph}
import com.mnemotix.analytix.nlp.helpers.NGram
import com.mnemotix.analytix.nlp.model.Sentence
import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing

/**
 * https://towardsdatascience.com/structuring-text-with-graph-representations-41dd4f2a3ab3
 * @param lang
 */

class GraphBasedTextSummarization(lang: String) {

  lazy val analytixRawTextProcessing = new AnalytixRawTextProcessing(lang)

  def buildGraph(doc: String) = {
    val pdoc = preprocessDocument(doc)
    val nodes = entities(pdoc)
    val edges  = weightRelations(pdoc)
  }

  def preprocessDocument(str: String) = {
    analytixRawTextProcessing.sentences(str.toLowerCase)
  }

  def createWeightGraph(node: Seq[String], edges: Map[String, Map[String, Int]]) = {

    val adjList = edges.map(edge => (edge._1, edge._2.map(l => WeightedEdge(l._1, l._2)).toList))
    val graph = new WeightedGraph[String](adjList)

  }

  /**
   * According to our definition, individual words are nodes in the graph. Thus, for each unique word in the document, there must be a node.
   * @param document
   */

  def entities(document: Seq[Sentence]): Set[String] = {
    document.map { sentence =>
      sentence.tokens.map(_.word.text)
    }.flatten.toSet
  }

  /**
   * According to our definition, there exists an edge between two nodes in the graph if the corresponding
   * words form a bigram in a sentence of the input document. Fortunately, in the pre-processing step, we split the
   * document accordingly, which facilitates this calculation.
   *
   * @param document
   */

  def weightRelations(document: Seq[Sentence]) = {
    document.map { sentence =>
      NGram.build(sentence.text, 2).model.flatMap { s =>
        s._1
      }.toSeq
    }.map(s => (s(0), s(1))).groupBy(c => (c._1, c._2)).view.mapValues(v => v.size).groupBy(_._1._1).view.mapValues(v => v.map(l => (l._1._1, l._2)).toMap).toMap
  }
}
