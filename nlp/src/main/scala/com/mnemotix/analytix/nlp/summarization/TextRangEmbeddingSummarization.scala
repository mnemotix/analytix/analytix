/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.summarization

import com.mnemotix.analytix.graph.PageRank
import com.mnemotix.analytix.nlp.distance.VectorSimilarity
import com.mnemotix.analytix.nlp.textprocessing.{AnalytixRawTextProcessing, WordVectorizer}
import scala.collection.parallel.CollectionConverters._


class TextRangEmbeddingSummarization(lang: String) {

  lazy val wordVectorizer = new WordVectorizer(lang)
  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing(lang)
  lazy val pageRank = new PageRank

  def sentenceVector(sentences: Seq[String]): Seq[Array[Double]] = {
    sentences.map { sentence =>
      val listOfWords = synaptixRawTextProcessing.words(sentence)
      val vectors: Seq[Array[Double]] = wordVectorizer.vectorize(listOfWords)
      wordVectorizer.vectorMean(vectors)
    }
  }

  /*
   for (i <- 0 to sentenceVectors.size; j <- 0 to sentenceVectors.size; if i < j) yield (i,j, VectorSimilarity.cosineSimilarity(sentenceVectors(i), sentenceVectors(j)))
   */

  def sentenceDistance(sentenceVectors: Seq[Array[Double]]) = {
    sentenceVectors.zipWithIndex.flatMap { i =>
      sentenceVectors.zipWithIndex.map { j =>
        (i._2,j._2, VectorSimilarity.cosineSimilarity(i._1, j._1))
      }
    }
  }

  def toGraph(similarities: Seq[(Int, Int, Double)]) = {
    similarities.filter(_._3 > 0.5).map(s => (s._1, s._2))
  }

  /**
   * pageRank from mbesancon created on 7/9/16.
   *
   */

    // todo test
    // todo select n best sentences

  def summarize(article: String): String = {
    val sentences: Seq[String] = synaptixRawTextProcessing.sentencesTextExtactor(article)
    val sentenceVectors = sentenceVector(sentences)
    val sentenceDistanceR = sentenceDistance(sentenceVectors)
    val graph = toGraph(sentenceDistanceR)
    val parGraph = graph.par
    val nodeId = parGraph.map(_._1).toSet.union(parGraph.map(_._2).toSet).toSeq
    val startRank = nodeId.map{x=>1.0f/nodeId.size}
    val endRank = pageRank.compRank(startRank, graph.par, 0.4f,0.001f)
    lazy val result: String = nodeId.zip(endRank).map(tup=>(tup._1 + " " + tup._2)).mkString("\n")
    result
  }
}