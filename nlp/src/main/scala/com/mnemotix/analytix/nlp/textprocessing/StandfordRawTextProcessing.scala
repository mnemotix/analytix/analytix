package com.mnemotix.analytix.nlp.textprocessing

import java.util.Properties
import com.mnemotix.analytix.commons.utils.TextUtils
import com.mnemotix.analytix.nlp.model.{Lemma, Ner, Pos, Sentence, Word}

import scala.collection.JavaConverters._
import edu.stanford.nlp.io.IOUtils
import edu.stanford.nlp.simple.Document

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class StandfordRawTextProcessing(lang: String) extends RawTextProcessing {

  val props = new Properties()

  override def init: Unit = {
    val l = lang.toLowerCase match {
      case "french" => {
        props.load(IOUtils.readerFromString("StanfordCoreNLP-french.properties"))
        "french"
      }
      case "english" => {
        props.load(IOUtils.readerFromString("StanfordCoreNLP-english.properties"))
        "english"
      }
      case _ => {
        props.load(IOUtils.readerFromString("StanfordCoreNLP-french.properties"))
        "french"
      }
    }

    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing, with ${l} language.")

  }

  def document(text: String): com.mnemotix.analytix.nlp.model.Document = {
    com.mnemotix.analytix.nlp.model.Document(text, sentences(text))
  }

  def sentences(text: String): Seq[com.mnemotix.analytix.nlp.model.Sentence] = {
    val doc = new Document(props, text)
    doc.sentences().asScala.map { sentence =>
      Sentence(
        sentence.text(),
        tokens(sentence.text())
      )
    }.toSeq
  }

  override def tokens(text: String): Seq[com.mnemotix.analytix.nlp.model.Token] = {
    val doc = new Document(props, text)
    doc.sentences().asScala.map { sentence =>
      sentence.tokens().asScala.map { token =>
        com.mnemotix.analytix.nlp.model.Token(
          Word(token.word()),
          Lemma(Some(token.lemma())),
          Ner(None),
          Pos(if (token.posTag().isEmpty) None else Some(token.posTag().trim))
        )
      }
    }.flatten.toSeq
  }

  def words(text: String): Seq[String] = {
    val doc = new Document(props, text)
    doc.sentences().asScala.map { sentence =>
      sentence.words().asScala
    }.flatten.toSeq
  }

  def lemmas(text: String): Seq[String] = {
    val doc = new Document(props, text)
    doc.sentences().asScala.map { sentence =>
      sentence.tokens().asScala.map { token =>
        token.lemma().trim
      }
    }.flatten.toSeq
  }

  def stripAccents(text: String): String = TextUtils.stripAccents(text)

  def stripAccent(text: Seq[String]): Seq[String] = {
    text.map { txt =>
      TextUtils.stripAccents(txt)
    }
  }

  def getProperties = props.toString

  override def shutdown: Unit = {}

  override def lemma(text: String): String = ???
}