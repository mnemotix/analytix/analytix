/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.helpers

object Marker {
  val start = "<s>"
  val end = "</s>"
}

object NGram {
  def build(t: String, n: Int): NGram = {
    NGram(addProbability(addCount(t.split("\n").flatMap(l =>
      sliding(l.split(" ").toList, n)).toList).toMap, n), n)
  }

  private def sliding(tokens: List[String], n: Int) =
    (List.fill(n - 1)(Marker.start) ++ tokens :+ Marker.end).sliding(n).toList

  private def addCount(slides: List[List[String]]) =
    slides.groupBy(identity).view.mapValues(_.size).toMap

  private def addProbability(withCount: Map[List[String], Int], n: Int) = {
    val sumIndex = withCount.groupBy { case (k, v) => k.take(n - 1) }.view.mapValues(_.values.sum)
    withCount.map { case (k, v) => (k, v.toDouble / sumIndex(k.take(n - 1))) }
  }
}

case class NGram(model: Map[List[String], Double], n: Int)