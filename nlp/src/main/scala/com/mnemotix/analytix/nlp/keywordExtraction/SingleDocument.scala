/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.keywordExtraction

import com.mnemotix.analytix.nlp.model.Token
import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing
import com.typesafe.scalalogging.LazyLogging

import scala.collection.immutable
import scala.collection.immutable.ListMap

// todo https://javadoc.scijava.org/Apache-Commons-Math/index.html?overview-summary.html

/**
 * inspire by http://www.miv.t.u-tokyo.ac.jp/papers/matsuoIJAIT04.pdf
 */

class SingleDocument(language: String) extends LazyLogging {
  lazy val analytixRawTextProcessing = new AnalytixRawTextProcessing(language)

  def extract(text: String): Map[String, Double] = {
    val tokensSentences = preprocessing(text)
    val sentenceString = tokensSentences.map(_.map(_.lemma.text.get))

    val frTerm = frequentTerm(sentenceString.flatten)

    val frTermList = frTerm.keys.toSeq
    val couple = couples(frTerm.keys.toSeq)

    // 0.95 * Math.log(2)
    val jensenShannon = jensenShannonDivergence(couple, sentenceString, frTerm, frTermList, (0.95 * Math.log(2)))
    val mutinfo = mutualInfo(couple, sentenceString, frTerm, frTermList, Math.log(2))
    val cluster = buildingSimpleCluster(jensenShannon, mutinfo)
    val chiResult = frTermList.map { w =>
      val chi = chi2Cluster(w, frTerm, cluster.toSeq, sentenceString)
      (w,robustnessOfChi2(chi.toSeq))
    }.toMap
    val chiResultOrder = ListMap(chiResult.toSeq.sortWith(_._2 > _._2):_*)

    chiResultOrder.map { chi =>
      (retrieveOriginalText(chi._1, tokensSentences.flatten).word.text, chi._2)
    }
  }

  def retrieveOriginalText(lemma: String, tokens: Seq[Token]): Token = {
    tokens.filter { token =>
      token.lemma.text.get == lemma
    }.last
  }

  def preprocessing(text: String): Seq[Seq[Token]] = {
    val sentence = analytixRawTextProcessing.sentences(text).filter(sentence => sentence.tokens.size > 3)
    sentence.map(_.tokens.filter(_.lemma.text.isDefined))
  }

  def frequentTerm(words: Seq[String]): Map[String, Int] = {
    val count = analytixRawTextProcessing.wordCount(words)
    val countOrder = ListMap(count.toSeq.sortWith(_._2 > _._2):_*)
    val toSlice = ((countOrder.size * 30) / 100).ceil.toInt
    countOrder.take(toSlice)
  }

  def jensenShannonDivergence(couple: Seq[(String, String)], sentenceString: Seq[Seq[String]], count: Map[String, Int], frequentTerm: Seq[String], treshold: Double): Map[(String, String), Double] = {
    def p(f: String, w: String): Double = {
      val fww = sentenceString.filter { s =>
        s.contains(f) && s.contains(w)
      }.size
      val fw = count.get(w).get
      fww.toDouble / fw.toDouble
    }

    couple.map { c =>
      val s: Seq[Double] = frequentTerm.map { f =>
        val pfc1 = p(f, c._1)
        val pfc2 = p(f, c._2)

        val x1 = pfc1 + pfc2
        val x2 = pfc1
        val x3 = pfc2

        (-x1 * math.log(x1)) - (-x2 * math.log(x2)) - (-x3 * math.log(x3))
      }.filterNot(value => value.isNaN)
      val res = scala.math.log(2) + (s.sum / 2)
      (c, res)
    }.filter(m => m._2 > treshold).toMap
  }

  def mutualInfo(couple: Seq[(String, String)], sentenceWord: Seq[Seq[String]],
                 count: Map[String, Int], frequentTerm: Seq[String], treshold: Double): Map[(String, String), Double] = {
    val nTotal = frequentTerm.size
    couple.map { c =>
      val fww = sentenceWord.filter { s =>
        s.contains(c._1) && s.contains(c._2)
      }.size
      (c, math.log((nTotal * fww) / (count.get(c._1).get * count.get(c._2).get)))
    }.filter(m => m._2 > treshold).toMap
  }

  def buildingSimpleCluster(jensenShannonDivergence: Map[(String, String), Double], mutualInfo: Map[(String, String), Double]): immutable.Iterable[Seq[String]] = {
    (jensenShannonDivergence ++ mutualInfo).keys
      .groupBy(_._1)
      .map( { case (k, v) => (Seq(k) ++ v.map(_._2)).distinct})

    //jensenShannonDivergence.keys.toList.union(mutualInfo.keys.toList)
  }
  // cluster: Seq[(String, String)]
  def chi2Cluster(w: String, frequences: Map[String, Int], cluster: Seq[Seq[String]], sentences:Seq[Seq[String]]): Seq[Double] = {
    cluster.flatten.distinct.map { c: String =>

      val pg = clusterExpectedProbability(c, cluster)
      val unconditionnalProb = pg
      (math.pow((frequences.get(w).get.toDouble - unconditionnalProb),2) / unconditionnalProb)
    }
  }

  def chi2Simple(w: String, frequences: Map[String, Int], frequentTerm: Seq[String], sentences:Seq[Seq[String]]): Seq[Double] = {
    frequentTerm.map{ g =>
      val pg = sentenceExpectedProbability(g, sentences) // expected frequencies
      val unconditionnalProb = nw(g, sentences:Seq[Seq[String]]).toDouble * pg
      (math.pow((frequences.get(w).get.toDouble - unconditionnalProb),2) / unconditionnalProb)
    }
  }

  def robustnessOfChi2(chi2: Seq[Double]): Double = {
    chi2.sum - chi2.max
  }

  def sentenceExpectedProbability(g: String, sentences: Seq[Seq[String]]): Double = {
    // sum of total number of term where g appear
    val f = sentences.filter { sentence =>
      sentence.contains(g)
    }.size

    f.toDouble / sentences.flatten.size.toDouble
  }

  // cluster: Seq[(String, String)]
  def clusterExpectedProbability(g: String, cluster: Seq[Seq[String]]) = {
    val f = cluster.filter {c =>
      c.contains(g)
    }.size

    f.toDouble / cluster.flatten.size.toDouble
  }

  def nw(w:String, sentences: Seq[Seq[String]]): Int = {
    sentences.filter { sentence =>
      sentence.contains(w)
    }.size
  }

  def couples(frequentTerm: Seq[String]): Seq[(String, String)] = {
    for {
      (x, idxX) <- frequentTerm.zipWithIndex
      (y, idxY) <- frequentTerm.zipWithIndex
      if idxX < idxY
    } yield (x, y)
  }
}