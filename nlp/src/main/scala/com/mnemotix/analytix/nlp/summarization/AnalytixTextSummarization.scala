/**
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.summarization

import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing

/**
 * This is based on term frequency and sentence weight
 * @param lang
 */

class AnalytixTextSummarization(lang: String) {

  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing(lang)

  def calculateSentenceScores(sentences: Seq[String], frequencyTable: Map[String, Int]): Map[String, Double] = {
    sentences.map { sentence =>
      val sentenceWords = synaptixRawTextProcessing.words(sentence)
      val sentenceWordCount = sentenceWords.size
      val weight = frequencyTable.filter(couple => sentenceWords.contains(couple._1)).values.sum.toDouble / sentenceWordCount.toDouble
      (sentence, weight)
    }.toMap
  }

  def calculateAverageScore(sentenceWeight: Map[String, Double]): Double = {
    sentenceWeight.values.sum / sentenceWeight.size
  }

  def articleSummary(sentences: Seq[String], sentenceWeight: Map[String, Double], threshold: Double): String = {
    sentences.filter(sentence => sentenceWeight.get(sentence).isDefined && sentenceWeight.get(sentence).get > threshold).mkString(" ")
  }

  def summarize(article: String): String = {
    val sentence = synaptixRawTextProcessing.sentencesTextExtactor(article)
    val frequencyTable = synaptixRawTextProcessing.wordCount(article)
    val sentenceScores = calculateSentenceScores(sentence, frequencyTable)
    val threshold = calculateAverageScore(sentenceScores)
    articleSummary(sentence, sentenceScores, 1.5 * threshold)
  }
}