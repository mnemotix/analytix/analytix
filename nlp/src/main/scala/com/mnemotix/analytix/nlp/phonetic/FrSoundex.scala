/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.nlp.phonetic

class FrSoundex {

  def sound(s: String): String = {
    var code=s.head.toUpper.toString
    var previous=getCode(code.head)
    for(ch <- s.drop(1); current=getCode(ch.toUpper)){
      if (!current.isEmpty && current!=previous)
        code+=current
      previous=current
    }
    code+="0000"
    code.slice(0,4)
  }

  def getCode(c:Char)={
    val code=
      Map("1"->List('B','P'),
        "2"->List('C','K','Q'),
        "3"->List('D', 'T'),
        "4"->List('L'),
        "5"->List('M', 'N'),
        "6"->List('R'),
        "7"->List('G','J'),
        "8"->List('X','Z','S'),
        "9"->List('F', 'V')
      )

    code.find(_._2.exists(_==c)) match {
      case Some((k,_)) => k
      case _ => ""
    }
  }
}

object FrSoundex {
  val frSoundex = new FrSoundex

  def sound(s: String) = frSoundex.sound(s)
}