/**
 * Copyright (C) 2013-2022 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.score

import com.mnemotix.analytix.graph.shortestPath.BFSShortest
import com.mnemotix.analytix.model.Graph
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class BetweennessCentralitySpec extends AnyFlatSpec with Matchers {

  def populate[N](nodes: Seq[Seq[N]], graph: Graph[N]): Graph[N] = {
    def populateHelper(rest: Seq[Seq[N]], newGraph: Graph[N]): Graph[N] = {
      rest match {
        case Nil => newGraph
        case x :: tail => {
          populateHelper(tail, newGraph.addEdge(x(1), x(2)))
        }
      }
    }
    populateHelper(nodes, graph)
  }

  it should "compute the centrality of a graph" in {
    val filePath = "modules/ml/src/test/resources/florentine_marriage_edgelist.csv"
    val bufferedSource = io.Source.fromFile(filePath)

    val nodes = bufferedSource.getLines.drop(1).map { line =>
      val input =  line.split(",").map(_.trim)
      input.toSeq
    }

    val marriageNet = populate(nodes.toSeq, Graph[String]())

    println(marriageNet.nodes.size)


    val graph = Graph[Int]()
      .addEdge(0, 1)
      .addEdge(0, 3)
      .addEdge(1, 2)
      .addEdge(3, 4)
      .addEdge(3, 7)
      .addEdge(4, 5)
      .addEdge(4, 6)
      .addEdge(5, 6)
      .addEdge(6, 7)

    val betweennessCentrality = new BetweennessCentrality[Int]()
    val res = betweennessCentrality.betweennessCentrality(graph, None, false, false)
    res.foreach(println(_))
    }


}
