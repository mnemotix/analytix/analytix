/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.shortestPath

import com.mnemotix.analytix.model.Graph
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BFSShortestSpec extends AnyFlatSpec with Matchers  {

  it should "compute shortest path" in {

    val graph = Graph[Int]()
      .addEdge(0, 1)
      .addEdge(0, 3)
      .addEdge(1, 2)
      .addEdge(3, 4)
      .addEdge(3, 7)
      .addEdge(4, 5)
      .addEdge(4, 6)
      .addEdge(5, 6)
      .addEdge(6, 7)


    //println(graph.nodes)

    println(graph.neighbours(0).mkString(" - "))
    println(graph.neighbours(5).mkString(" - "))

    val result = BFSShortest.findshortestPaths(0, graph)
    val result1 = BFSShortest.findshortestPaths(1, graph)
    val result5 = BFSShortest.findshortestPaths(5, graph)

    println(result5.parents)


    //println(result5)

    println("BFSShortest to 0 | " + BFSShortest.findPath(0, result5.parents).mkString(" - "))
    println("BFSShortest to 1 | " + BFSShortest.findPath(1, result5.parents).mkString(" - "))
    println("BFSShortest to 2 | " + BFSShortest.findPath(2, result5.parents).mkString(" - "))
    println("BFSShortest to 3 | " + BFSShortest.findPath(3, result5.parents).mkString(" - "))
    println("BFSShortest to 4 | " + BFSShortest.findPath(4, result5.parents).mkString(" - "))
    println("BFSShortest to 5 | " + BFSShortest.findPath(5, result5.parents).mkString(" - "))
    println("BFSShortest to 6 | " + BFSShortest.findPath(6, result5.parents).mkString(" - "))
    println("BFSShortest to 7 | " + BFSShortest.findPath(7, result5.parents).mkString(" - "))

/*
    println(result.sigma)

    println(BFSShortest.findPath(1, result.parents).mkString(" "))
    println(BFSShortest.findPath(2, result.parents).mkString(" "))
    println(BFSShortest.findPath(3, result.parents).mkString(" "))
    println(BFSShortest.findPath(4, result.parents).mkString(" "))
    println(BFSShortest.findPath(5, result.parents).mkString(" "))
    println(BFSShortest.findPath(6, result.parents).mkString(" "))
    println(BFSShortest.findPath(7, result.parents).mkString(" "))

 */
  }

  it should "compute shortest path for string graph" in {
    val graph = Graph[String]()
      .addEdge("London", "Lisbon")
      .addEdge("London", "Bucharest")
      .addEdge("Lisbon", "Madrid")
      .addEdge("Madrid", "London")
      .addEdge("Madrid", "Rome")
      .addEdge("Rome", "London")
      .addEdge("Paris", "Rome")

    println(graph.nodes)

  }
}
