/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.traversal

import com.mnemotix.analytix.model.Graph
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BFSTest extends AnyFlatSpec with Matchers {

  it should "return visited nodes" in {
    val graph = Graph[String]()
      .addEdge("A", "B")
      .addEdge("A", "C")
      .addEdge("B", "C")
      .addEdge("B", "D")
      .addEdge("C", "D")
      .addEdge("D", "B")
      .addEdge("D", "E")
      .addEdge("D", "F")
      .addEdge("E","B")
      .addEdge("E", "F")

    val visitedFromAActual = BFS.iterativeBFS("A", graph, print)
    val visitedFromAExpected = List("A", "B", "C", "D", "E", "F")

    visitedFromAActual.toList.sorted shouldBe visitedFromAExpected
  }

  it should "return visited nodes in int graph" in {
    val graph = Graph[Int]()
      .addEdge(0, 1)
      .addEdge(0, 3)
      .addEdge(1, 2)
      .addEdge(3, 4)
      .addEdge(3, 7)
      .addEdge(4, 5)
      .addEdge(4, 6)
      .addEdge(5, 6)
      .addEdge(6, 7)

    val visited = BFS.iterativeBFS(4, graph, println)
    println("number  : " + visited.size)
    visited.foreach(println(_))
    //println(visited.mkString(" - "))
  }


}
