/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.model

class DirectedGraph[N](adjList: Map[N, List[N]]) extends Graph[N] {

  def isWeighted = false

  def isDirected = true

  override def nodes: List[N] = adjList.keys.toList

  override def edges: List[(N, N)] = adjList.toList.flatMap {
    case (node, neighbours) =>
      neighbours.map(neighbour => (node, neighbour))
  }

  override def addNode(n: N): Graph[N] = new DirectedGraph(adjList + (n -> List()))

  override def addEdge(from: N, to: N): Graph[N] = {
    val fromNeighbours = to +: neighbours(from)
    val g = new DirectedGraph(adjList + (from -> fromNeighbours))

    if(g.nodes.contains(to)) g else g.addNode(to)
  }

  override def neighbours(node: N): List[N] = {
    adjList.getOrElse(node, Nil)
  }
}
