/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.traversal

import com.mnemotix.analytix.model.Graph

import scala.collection.immutable.Queue

object BFS {
  def iterativeBFS[N](start: N, graph: Graph[N], f: N => Unit): Set[N] = {
    if(!graph.nodes.contains(start))
      Set()
    else {
      // LazyList - an immutable linked list that evaluates elements in order and only when needed
      LazyList.iterate((Queue(start), Set[N](start))) {
        case(queue, visited) => {
          // get the first element of the queue
          val (node, rest) = queue.dequeue
          // new queue will contain non visited neighbours of `node` and the rest of the queue
          val newQueue = rest.enqueueAll(graph.neighbours(node).filterNot(visited.contains))
          // add all neighbours of `node` to `the visited` set
          val newVisited = graph.neighbours(node).toSet ++ visited

          (newQueue, newVisited)
        }
      }.takeWhile(tuple => tuple._1.nonEmpty).foldLeft(Set[N]())( (acc, curr) => {
        val head = curr._1.head
        f(head)
        acc ++ Set(head)
      })
    }
  }
}