/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph

import com.mnemotix.analytix.model.{SimpleEdge, SimpleNode}

import scala.collection.mutable.ListBuffer

class ClusterByEdgeWeight(weightThreshold: Float, nodes: List[SimpleNode], edges: List[SimpleEdge]) {

  def exec(): List[List[SimpleNode]] = {
    val clusters = initCluster()
    mergeCluster(clusters)
  }

  def initCluster(): List[List[SimpleNode]] = {
    nodes.map {
      node =>
        val cluster = ListBuffer.empty[SimpleNode]
        cluster.append(node)
        cluster.toList
    }
  }

  def mergeCluster(clusters: List[List[SimpleNode]]) = {
    edges.filterNot(_.weight < weightThreshold).filterNot {
      filterCluster(clusters, _)
    }.flatMap { edge =>
      val firstCluster = clusters(edge.node1.nodeIndex)
      val secondCluster = clusters(edge.node2.nodeIndex)
      val mergedCluster = firstCluster ++ secondCluster

      secondCluster.flatMap { node =>
        clusters.updated(node.nodeIndex, mergedCluster)
      }.toSet
    }
  }

  def filterCluster(clusters: List[List[SimpleNode]], edge: SimpleEdge) = {
    clusters(edge.node1.nodeIndex) == clusters(edge.node2.nodeIndex)
  }
}
