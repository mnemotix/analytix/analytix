/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph

import com.typesafe.scalalogging.LazyLogging

import scala.math.sqrt
import scala.collection.parallel.immutable.ParSeq
import scala.collection.parallel.CollectionConverters._


/**
 *
 * credits to @matbesancon
 *
 * https://matbesancon.github.io/post/2016-09-13-page-rank/
 * https://github.com/matbesancon/PageRank/blob/master/src/main/scala/PageRank/package.scala
 */

class PageRank extends LazyLogging {

  type LinkMat = ParSeq[(Int, Int)]

  def sumElements(R: ParSeq[Float], A: LinkMat, j: Int): Float = {
    val totalLinks = A.filter(_._2==j)

    if (totalLinks.isEmpty) {
      logger.error("No link in the page " + j + " at sumElements")
      throw new Exception("No link in the page " + j + " at sumElements")
    }
    else R(j)/totalLinks.size
  }

  def findConnected(i: Int, A: LinkMat):ParSeq[Int] = A.filter(_._1==i).map(_._2).toSeq

  def converged(r1: ParSeq[Float], r2: ParSeq[Float], eps: Float): Boolean = {
    val totSquare: Float = r1.zip(r2).map(p=>(p._1-p._2)*(p._1-p._2)).sum
    sqrt(totSquare/r1.size)<=eps
  }

  def compRank(R: ParSeq[Float], A: LinkMat, damp: Float, eps: Float, niter: Int = 0,niterMax: Int = 10000): ParSeq[Float] = {
    val rankIndex: ParSeq[Int] = (0 until R.size).par
    val rightRank: ParSeq[Float] = rankIndex map{i:Int =>
      val connected = findConnected(i,A)
      connected.map{j:Int => sumElements(R, A, j)}.sum
    }
    val newRank = rightRank map {damp*_ + (1-damp)/R.size}
    if(converged(newRank,R,eps)) newRank
    else if(niter>=niterMax) {
      logger.info("Max iteration reached")
      newRank
    } else compRank(newRank,A,damp,eps,niter+1,niterMax)
  }
}