/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.services.tasks

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.{AmqpClientConfiguration, SynaptixRPCTask}
import com.mnemotix.amqp.api.models.{AmqpMessage, ErrorMessage, MessageProcessResult, OkMessage}
import com.mnemotix.analytix.model.PercolationReport
import com.mnemotix.analytix.validator.PercolatorControllerAmqpMessageValidator
import com.mnemotix.synaptix.index.elasticsearch.models.ESSearchResponse
import com.sksamuel.elastic4s.Response
import play.api.libs.json.{JsString, Json}

import scala.concurrent.{ExecutionContext, Future}

trait RegisterTask extends SynaptixRPCTask[Seq[String]] {

  val system: ActorSystem
  val ec: ExecutionContext
  override val exchangeName: String = AmqpClientConfiguration.exchangeName

  override def beforeProcess(msg: AmqpMessage): Option[Seq[String]] = {
    if(isVerbose) logger.debug(s"AMQP Message received : ${msg.toString}")
    val percoValidator = PercolatorControllerAmqpMessageValidator.validate(msg)
    Some(Seq(percoValidator.percolatorIndex))
  }
/*
case class PercolationReport(resume: String, successNbr:Int, errorNbr:Int, successMessage: Seq[String], errorMessage: Seq[String])

 */
  def mapESClientResponseToString(resps: Seq[Response[_]]) = {
    val successFailure = reportWriter(resps)
    val report = PercolationReport(s"There is ${successFailure._1.size} successes and ${successFailure._2.size} failures",
      successFailure._1.size,
      successFailure._2.size,
      successFailure._1,
      successFailure._2
    )
    OkMessage(Json.toJson(report))
  }

  def reportWriter(resps: Seq[Response[_]]): (Seq[String], Seq[String]) = {
    def responseHelper(success: Seq[String], error: Seq[String], resps: Seq[Response[_]]): (Seq[String], Seq[String]) = {
      resps match {
        case Nil => (success, error)
        case x :: tail => {
          if (x.isSuccess) {
            val resp = x.body.getOrElse(" ")
            responseHelper(success:+resp, error, tail)
          }
          else {
            val err = s"${x.error.`type`} : ${x.error.reason}"
            responseHelper(success, error:+err, tail)
          }
        }
      }
    }
    val l = List()
    val l2 = List()
    responseHelper(l, l2, resps)
  }

  def mapESClientResponseToString(resp: Response[_]): MessageProcessResult = {
    if (resp.isError) {
      logger.warn(s"""Server returned an error : ${resp.error.`type`} ${resp.error.reason}""")
      ErrorMessage(resp.error.asException)
    }
    else {
      OkMessage(JsString(resp.body.getOrElse("")))
    }
  }

  def mapESClientResponseToJson(resp: Response[_]): MessageProcessResult = {
    if (resp.isError) {
      logger.warn(s"""Server returned an error : ${resp.error.`type`} ${resp.error.reason}""")
      ErrorMessage(resp.error.asException)
    }
    else {
      OkMessage(ESSearchResponse(resp.body).toJson)
    }
  }
}