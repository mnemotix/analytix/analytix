/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.services.tasks


import akka.actor.ActorSystem
import com.mnemotix.amqp.api.exceptions.MalformedBodyException
import com.mnemotix.amqp.api.models.{AmqpMessage, ErrorMessage, MessageProcessResult, OkMessage}
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.analytix.percolation.services.{ESClient, ESPercolatorTextExtractor}
import com.mnemotix.analytix.percolation.services.es.PercolateQueryRegister
import com.mnemotix.analytix.validator.PercolatorControllerAmqpMessageValidator
import play.api.libs.json.JsString

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class BasicRegisterTask(override val topic: String, override val exchangeName: String)(implicit override val client: RabbitMQClient, val ec: ExecutionContext, override val system: ActorSystem,
                                                               esClient: ESClient) extends RegisterTask {
  //implicit val conn = Some(client.getConnection())

  //init()

  def processMessage(msg: AmqpMessage, context: Option[Seq[String]]): Future[MessageProcessResult] = {
    val percoTodo = PercolatorControllerAmqpMessageValidator.validate(msg)
    if (percoTodo.docId.isDefined) {
      if (percoTodo.queryString.isDefined) {
        implicit val esPercolatorTextExtractor = new ESPercolatorTextExtractor(percoTodo.percolatorIndex, percoTodo.fields.toSeq.flatten, percoTodo.sortField)
        val percolateQueryRegister = new PercolateQueryRegister(percoTodo.percolatorIndex)
        percolateQueryRegister.init
        val future = percolateQueryRegister.register(percoTodo.docId.get, percoTodo.queryString.get, percoTodo.logical)

        future.transform {
          case Success(value) => {
            Try(OkMessage(JsString(value.toString)))
          }
          case Failure(exception) => {
            Try(ErrorMessage(exception))
          }
        }
      }
      else throw MalformedBodyException("The message should contain a queryString")
    }
    else throw MalformedBodyException("The message should contain a doc id")
  }

}