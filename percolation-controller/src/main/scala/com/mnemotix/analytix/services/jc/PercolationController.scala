/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.services.jc

import akka.actor.ActorSystem
import akka.stream.AbruptTerminationException
import com.mnemotix.amqp.api.{AmqpClientConfiguration, SynaptixRPCController}
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.analytix.percolation.exceptions.PercolationException
import com.mnemotix.analytix.percolation.services.ESClient
import com.mnemotix.analytix.services.tasks.{BasicRegisterTask, BatchRegisterTask, BulkRegisterTask, RegisterRawQueryTask}
import com.mnemotix.synaptix.index.IndexClient
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

object PercolationController extends App with LazyLogging {

  implicit val system: ActorSystem = ActorSystem("PercolationController")
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val client: RabbitMQClient = new RabbitMQClient
  implicit val esClient: ESClient = new ESClient()

  IndexClient.init()

  logger.info("Percolation controller starting...")

  //println(AmqpClientConfiguration.exchangeName)

  val controller = new SynaptixRPCController("Percolation Controller")
  controller.registerTask("analytix.percolation.register.simple", new BasicRegisterTask("analytix.percolation.register.simple", AmqpClientConfiguration.exchangeName))
  controller.registerTask("analytix.percolation.register.bulk", new BulkRegisterTask("analytix.percolation.register.bulk", AmqpClientConfiguration.exchangeName))
  controller.registerTask("analytix.percolation.register.batch", new BatchRegisterTask("analytix.percolation.register.batch", AmqpClientConfiguration.exchangeName))
  controller.registerTask("analytix.percolation.register.query", new RegisterRawQueryTask("analytix.percolation.register.query", AmqpClientConfiguration.exchangeName))
  val starting = controller.start()

  starting.onComplete{
    case Success(_) =>
    case Failure(error) => {
      error match {
        //        case _:com.mnemotix.synaptix.index.IndexSearchException => // broker was disconnected
        //          logger.error(s"""Broker has been disconnected. Trying to reconnect...""")
        //          sys.exit(-1)
        case _:com.rabbitmq.client.ShutdownSignalException => // broker was disconnected
          logger.error(s"""Broker has been disconnected.""")
        //          sys.exit(-1)
        case _: AbruptTerminationException =>
          logger.error(s"""A task has been terminated abruptely.""")
          sys.exit(-1)
        case ie: PercolationException =>
          logger.error(ie.getMessage, ie)
          sys.exit(-1)
        case e: Throwable =>
          e.printStackTrace()
          sys.exit(-1)
      }
    }
  }

  logger.info("Percolation Controller started successfully.")

  sys addShutdownHook {
    esClient.shutdown
    controller.shutdown()
    Await.result(system.terminate(), Duration.Inf)
  }
}