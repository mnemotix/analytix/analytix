/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.validator

import com.mnemotix.amqp.api.exceptions.{MalformedBodyException, MessageValidationException}
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.analytix.model.PercoRegistrar
import play.api.libs.json.{Json}

object PercolatorControllerAmqpMessageValidator {

  @throws(classOf[MessageValidationException])
  def validate(message: AmqpMessage): PercoRegistrar = {
    if (message.body.validate[PercoRegistrar].isSuccess) {
      val registrar = message.body.as[PercoRegistrar]
      registrar
    }
    else throw MalformedBodyException(s"""Message body was malformed. ${Json.prettyPrint(message.body)}""")
  }

}
