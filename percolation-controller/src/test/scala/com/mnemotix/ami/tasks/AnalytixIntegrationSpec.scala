/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.tasks

import akka.actor.ActorSystem
import com.mnemotix.amqp.api.AmqpClientConfiguration
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.amqp.api.rabbitmq.{RabbitMQClient, RabbitMQRPCCall}
import com.rabbitmq.client.{Channel, Connection}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import play.api.libs.json.{JsString, JsValue, Json}

import scala.collection.mutable
import scala.concurrent.ExecutionContext

class AnalytixIntegrationSpec  extends AnyFlatSpec with Matchers {

  implicit val system = ActorSystem("BulkPercolationProducer")
  implicit val executionContext =  ExecutionContext.global

  implicit val rabbit = new RabbitMQClient()
  implicit val conn: Connection = rabbit.getConnection()
  implicit val channel: Channel = rabbit.createChannel()

  val topic = "analytix.percolation.register.batch"
  val replyBuffer = mutable.Buffer[String]()

  val query = """{\n    \"query\": {\n      \"query_string\": \"$$prefLabel$$ OR \"$$altLabel$$\",\n      \"default_field\": \"percoLabel\"\n    }\n  }\n}""""

  val message =
    """{
      |  "percolatorIndex" : "demo-mnx-concept",
      |  "logical" : "or",
      |  "queryString" : "{\n        \"query\": {\n           \"query_string\": {\n           \"query\": \"($$prefLabel$$)[[ OR ($$altLabel$$)]]\",\n         \"default_field\": \"percoLabel\"\n        }\n      }\n    }",
      |  "fields" : ["altLabel", "prefLabel"],
      |  "sortField" : "entityId"
      |}""".stripMargin

  def headers(topic: String): Map[String, JsValue] = Map[String, JsValue]("routing.key" -> JsString(topic))

  val amqpmessage  = Vector(new AmqpMessage(
    headers(topic),
    Json.parse(message)
  ))

  var replyOK:Boolean = false
  val rpc: RabbitMQRPCCall = new RabbitMQRPCCall(topic, AmqpClientConfiguration.exchangeName) {
    override def onReply(message: AmqpMessage): Unit = {
      println(s"onReply: ${message.toString()}")
    }
  }

  it should "send a message and reply" in {
    rpc.init()
    rpc.start()
    rpc.call(amqpmessage)
    Thread.sleep(120000)

    rpc.shutdown()

  }

}
