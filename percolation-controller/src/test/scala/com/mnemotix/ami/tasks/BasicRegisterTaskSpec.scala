/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami.tasks

import com.mnemotix.ami.PercolationControllerTestSpec
import com.mnemotix.amqp.api.models.AmqpMessage
import com.mnemotix.analytix.model.PercoRegistrar
import com.mnemotix.analytix.services.tasks.BasicRegisterTask
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class BasicRegisterTaskSpec extends PercolationControllerTestSpec {


  it should "register an query" in {
    /*val task = new BasicRegisterTask()

    val id = "http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e"
    val ids = Seq("http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e", "http://ontology.datasud.fr/openemploi/data/occupation/376acd9a9d1618c601fc641c871dda34")
    val predictate = "or"
    val queryString = """{
        "query": {
           "query_string": {
           "query": "($$prefLabel$$) OR ($$altLabel$$)",
         "default_field": "content"
        }
      }
    }"""
    val fields = Seq("altLabel", "prefLabel")
    val sortField = "entityId"
    val params = new PercoRegistrar("dev-openemploi-skill", Some(id), None, Some(predictate), Some(queryString), Some(fields), None)

    println(Json.prettyPrint(Json.toJson(params)))
    val message = AmqpMessage(Map.empty, Json.toJson(params))
    val resultMessage = Await.result(task.processMessage(message, None), Duration.Inf)
    println(Json.prettyPrint(resultMessage.content))*/
  }
}
