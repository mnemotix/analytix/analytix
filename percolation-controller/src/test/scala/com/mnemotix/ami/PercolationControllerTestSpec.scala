/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.ami

import akka.actor.ActorSystem
import akka.dispatch.ExecutionContexts
import akka.stream.ActorMaterializer
import com.mnemotix.amqp.api.rabbitmq.RabbitMQClient
import com.mnemotix.analytix.percolation.services.ESClient
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PercolationControllerTestSpec extends AnyFlatSpec with Matchers with BeforeAndAfterAll {
  implicit val system = ActorSystem("SynaptixTestSpec-"+System.currentTimeMillis())
  implicit val ec = ExecutionContexts.global()
  implicit val client: RabbitMQClient = new RabbitMQClient
  implicit val esClient = new ESClient()

  override protected def afterAll(): Unit =
    system.terminate()
}
