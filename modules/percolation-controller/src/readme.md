# Container 

Retrouver le container docker `analytix-percolation-controller:3.0-SNAPSHOT` à l'adresse suivante : `registry.gitlab.com/mnemotix/analytix/analytix`

# Variable d'environnement

ES_MASTER_URI
ES_CLUSTER_USER
ES_CLUSTER_PWD
RABBITMQ_DEFAULT_USER
RABBITMQ_DEFAULT_PASS

# Presentation du serrvice amqp

Le service `analytix-percolation-controller:3.0-SNAPSHOT` permet d'enregistrer les requêtes qui seront indéxées afin de faire la percolation dans Elastic Search.

# Prerequis

Avant de lancer le service, il faut s'assurer que l'index qui contiendra les requêtes a dans son mapping l'élément suivant :
```     
   "query": {
         "type": "percolator"
    }, 
```        
le champ qui va accueillir le texte sur lequel on va faire la percolation, p.ex :

```
    "percoLabel": {
        "type": "text",
        "analyzer": "french"
    },
```

# Le processus

Le service permet au client de spécifier un index qui contient les ou le document dans lesquels seront indexés les requêtes de percolation. Le client pourra spécifier les champs qui seront insérer dans la requête de percolation.
Le service permet au client de fournir une requete "à trou" qui sera indexée et comblée avec les résultats des champs des documents.

Par exemple :

Si le client veut enregistrer des requetes de percolation dans un index qui contient des concepts skos d'un thésaurus. L'index contient à minima, le champ qui contient le prefLabel des concepts, un champ query et un champ percoLabel. Le client pourra demander de requeter l'index, de recupérer les prefLabel et de les utiliser afin de créer la requete suivante :

```{
  "query": {
    "query_string": {
      "query": "$$prefLabel$$",
      "default_field": "percoLabel"
    }
  }
}
```

Pour les concepts concernés de l'index le système pourra mettre à jour ces dernier en ajoutant la requete qui suivra le schéma suivant en prenant bien soin de remplacer $$prefLabel$$ par le contenu du prefLabel du concept.

# Les topics 

## "analytix.percolation.register.query"

Ce topic permettra d'enregistrer une requête

Le contenu du message : 

```
{
  "percolatorIndex" : "dev-openemploi-skill",
  "docId" : "http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e",
  "queryString" : "{\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"$$prefLabel$$ OR \"$$altLabel$$\",\n      \"default_field\": \"percoLabel\"\n    }\n  }\n}"
}
```

## "analytix.percolation.register.bulk"

Ce topic permettra d'enregistrer un ensemble de requête

Le contenu du message : 

```
{
  "percolatorIndex" : "dev-openemploi-skill",
  "docIds" : [ "http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e", "http://ontology.datasud.fr/openemploi/data/occupation/376acd9a9d1618c601fc641c871dda34" ],
  "logical" : "or",
  "queryString" : "{\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"$$prefLabel$$ OR \"$$altLabel$$\",\n      \"default_field\": \"percoLabel\"\n    }\n  }\n}",
  "fields" : [ "altLabel", "prefLabel" ]
}
```

## "analytix.percolation.register.batch"


```
{
  "percolatorIndex" : "dev-openemploi-skill",
  "logical" : "or",
  "queryString" : "{\n  \"query\": {\n    \"query_string\": {\n      \"query\": \"$$prefLabel$$ OR \"$$altLabel$$\",\n      \"default_field\": \"percoLabel\"\n    }\n  }\n}",
  "fields" : [ "altLabel", "prefLabel" ],
  "sortField" : "entityId"
}
```


Ce topic permettra d'enregistrer l'ensemble des documents d'un index
