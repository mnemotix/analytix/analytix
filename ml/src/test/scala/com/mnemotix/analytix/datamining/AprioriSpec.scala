/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.datamining

import com.mnemotix.analytix.MlSpec

class AprioriSpec extends MlSpec {

  val boulangeTxt = """POSTE : Responsable Production H/F
                      |PROFIL : 
                      |DESCRIPTION : Dynamique et autonome, vous managez votre équipe de 4 à 12 personnes, boulangers et préparateurs, en supervisant et réalisant notre fabrication traditionnelle sous la responsabilité du Responsable Magasin.
                      |Passionné(e) par le métier de boulanger, rigoureux et méthodique, vous êtes le garant de la qualité de nos produits et de nos recettes dans le respect des normes d'hygiéne.
                      |Doté(e) d'un bon relationnel, vous savez transmettre vos acquis, vous menez vos équipes avec tact et obtenez les résultats escomptés. Vous avez une expérience confirmée en management d'équipe.
                      |Niveau de formation expérience professionnelle :
                      |CAP / BEP / BP / Brevet de Maîtrise exigé ou équivalent.
                      |Expérience souhaitée de 2 ans à un poste similaire dans le même domaine d'activité.
                      |Formation interne assurée.
                      |Contexte et spécificité de l'emploi :
                      |Le lieu de travail est le laboratoire d'un point de vente Boulangerie de Marie Blachère.
                      |Amplitude horaire : 3h - 20 heures
                      |Congé dimanche
                      |Le Responsable Production peut être amené se déplacer temporairement sur un autre magasin pour effectuer un remplacement.
                      |Tous nos postes sont ouverts aux personnes en situation de handicap.
                      |Informations contractuelles :
                      |Temps de travail : 39 heures
                      |Catégorie : Agent de maîtrise
                      |Salaire brut mensuel/12 mois + prime sur objectif liée au Chiffre d'Affaires + Mutuelle + CE + Participation
                      |
                      |En bref : Manager une équipe, Boulanger, Boulanger industriel""".stripMargin

  it should "test a priori methode on a text" in {

  }
  
}