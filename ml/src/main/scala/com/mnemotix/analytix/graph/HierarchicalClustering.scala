/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph

import com.mnemotix.analytix.model.{SimpleEdge, SimpleNode}

import scala.::
import scala.collection.mutable

class HierarchicalClustering {

  def refine(clusters: List[List[SimpleNode]], nodeCount: Int, edges: List[SimpleEdge], weightThresholdForMergingClusters: Float,
             singleEdgeThreshold: Float, clusterSize1MergeThreshold: Float): List[List[SimpleNode]] = {
    val t1 = System.currentTimeMillis()
    val nodeToClusterIndex: mutable.Seq[Int] = setNodeToClusterIndex(clusters, nodeCount)
    val clusterSizes: Seq[Int] = clusters.map(_.size)
    val t2 = System.currentTimeMillis()
    val clusterToClusterWeightMap: Map[String, Float] = setClusterToClusterWeightMap(edges, nodeToClusterIndex, singleEdgeThreshold)
    val clusterNodes = clusters.zipWithIndex.map(cluster => SimpleNode(cluster._2, List.empty[SimpleEdge]))
    val clusterEdges = setClusterEdge(clusters, clusterToClusterWeightMap, clusterSizes, weightThresholdForMergingClusters,
      clusterSize1MergeThreshold, clusterNodes).toList

    val t3 = System.currentTimeMillis()
    val clusterByEdgeWeight = new ClusterByEdgeWeight(0F, clusterNodes, clusterEdges)
    val clustersOfClusters = clusterByEdgeWeight.exec()
    val t4 = System.currentTimeMillis()

    val mergedCluster = clustersOfClusters.flatMap { clusterOfClusters =>
      clusterOfClusters.map { node =>
        clusters(node.nodeIndex)
      }
    }
    val t5 = System.currentTimeMillis()
    mergedCluster
  }

  def setNodeToClusterIndex(clusters: List[List[SimpleNode]], nodeCount: Int) = {
    val nodeToClusterIndex = List.fill(nodeCount)(-1).toBuffer
    clusters.zipWithIndex.map {
      case (cluster, i) => {
        cluster.map { node =>
          nodeToClusterIndex(node.nodeIndex) = i
        }
      }
    }
    nodeToClusterIndex
  }

  def setClusterToClusterWeightMap(edges: List[SimpleEdge], nodeToClusterIndex: mutable.Seq[Int], singleEdgeThreshold: Float) = {
    edges.filterNot(_.weight < singleEdgeThreshold).filterNot(filterClusterToClusterWeightMap(_, nodeToClusterIndex)).map { edge =>
      val fromCluster = nodeToClusterIndex(edge.node1.nodeIndex)
      val toCluster = nodeToClusterIndex(edge.node2.nodeIndex)

      val edgeName = Math.min(fromCluster, toCluster) + "_" + Math.max(fromCluster, toCluster)
      (edgeName, edge.weight)
    }.groupBy(_._1).map { case (k,v) => (k, v.map(_._2).sum)}
  }

  def filterClusterToClusterWeightMap(edge: SimpleEdge, nodeToClusterIndex: mutable.Seq[Int]) = {
    val fromCluster = nodeToClusterIndex(edge.node1.nodeIndex)
    val toCluster = nodeToClusterIndex(edge.node2.nodeIndex)
    fromCluster < 0 || toCluster < 0 || fromCluster == toCluster
  }

  def setClusterEdge(clusters: List[List[SimpleNode]], clusterToClusterWeightMap: Map[String, Float], clusterSizes: Seq[Int], weightThresholdForMergingClusters: Float,
                     clusterSize1MergeThreshold: Float, clusterNodes: Seq[SimpleNode]) = {
    clusterToClusterWeightMap.filter { entry =>

      val clusterToClusterName = entry._1
      val i1 = clusterToClusterName.indexOf("_")
      val fromCluster = clusterToClusterName.substring(0, i1).toInt
      val toCluster = clusterToClusterName.substring(i1 + 1).toInt

      val weight = (clusterSizes(fromCluster) * clusterSizes(toCluster)).toFloat / entry._2

      val cond1 =  weight > weightThresholdForMergingClusters
      val cond2 = if (weight >= clusterSize1MergeThreshold && Math.min(clusters(fromCluster).size, clusters(toCluster).size) <= 1) true else false
      cond1 || cond2
    }.map { entry =>

        val clusterToClusterName = entry._1
        val i1 = clusterToClusterName.indexOf("_")
        val fromCluster = clusterToClusterName.substring(0, i1).toInt
        val toCluster = clusterToClusterName.substring(i1 + 1).toInt
        val weight = (clusterSizes(fromCluster) * clusterSizes(toCluster)).toFloat / entry._2
        val nodei = clusterNodes(fromCluster)
        val nodej = clusterNodes(toCluster)
        val edge = SimpleEdge(nodei, nodej, weight)
        val edgesi = nodei.edge
        val edgesj = nodej.edge
        val nodeiCopy = nodei.copy(edge = edgesi.::(edge))
        val nodejCopy = nodej.copy(edge = edgesj.::(edge))
        edge
    }
  }
}