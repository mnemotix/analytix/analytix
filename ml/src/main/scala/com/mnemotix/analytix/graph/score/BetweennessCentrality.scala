/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.score

import com.mnemotix.analytix.graph.shortestPath.{BFSShortest, Dijkstra, ShortStep, ShortestPathHelper}
import com.mnemotix.analytix.model.{Graph, WeightedGraph}
import com.typesafe.scalalogging.LazyLogging


class BetweennessCentrality[N] extends LazyLogging {

  case class Accumulate(delta: Map[N, Double], betweeness: Map[N, Double])

  def betweennessCentrality(graph: Graph[N], k: Option[Int], normalized: Boolean,
                            endpoint: Boolean): Map[N, Double] = {

    val betwenness = graph.nodes.map(_ -> 0.0).toMap

    val nodes: Seq[N] = {
      if (k.isDefined) {
        scala.util.Random.shuffle(graph.nodes).take(k.get)
      }
      else {
        graph.nodes
      }
    }



    val newBetwenness = nodes.zipWithIndex.flatMap { case(s, idx) =>

      logger.info(s"we process $idx / ${nodes.size}")

      val step: ShortStep[N] = {
        shortStepHelper(s, graph, nodes)
      }

      println(" node s = " + s)

      println(" PARENTS " + " " + step.parents)

      println(" UNPROCESSED " + " " + step.unprocessed)

      println(" DISTANCES " + " " + step.distances)

      println(" SIGMA " + " " + step.sigma)

      println(" S " + " " + step.S)



      val accumulation = {
        if (endpoint) {
          _accumulate_endpoints(step, s, betwenness)
        }
        else  {
          _accumulate_basic(step, s, betwenness)
        }
      }
      accumulation.betweeness
    }
    _rescale(newBetwenness.toMap, graph.nodes.size, normalized, graph.isDirected, k, endpoint)

  }

  def shortStepHelper( s: N, graph: Graph[N],  nodes: Seq[N]):ShortStep[N] = {
    if (graph.isWeighted) {
      Dijkstra.findShortestPaths(s, graph.asInstanceOf[WeightedGraph[N]])
    }
    else {
      BFSShortest.findshortestPaths(s, graph)
    }
  }

  def _accumulate_endpoints(step: ShortStep[N], s: N, betweeness: Map[N, Double]) = {
    val newBetweeness = betweeness.updatedWith(s)(_.map(_ + step.S.length - 1))
    def _accumulate_endpoints_helper(S: Seq[N], delta: Map[N, Double], betweeness: Map[N, Double]): Accumulate = {
      S match {
        case Nil => Accumulate(delta, betweeness)
        case w :: tail => {
          val coeff = (1 + delta(w)) / step.sigma(w)
          val newDelta = ShortestPathHelper.findPath(w, step.parents).flatMap { v =>
            //val updater = delta(v) + (step.sigma(v) * coeff)
            //delta.updated(v, updater)
            delta.updatedWith(v)(_.map(_ + (step.sigma(v) * coeff)))
          }.toMap
          if (w != s) {
            val newBetweeness = betweeness.updatedWith(w)(_.map(_ + newDelta(w) + 1))
            _accumulate_endpoints_helper(tail, newDelta, newBetweeness)
          }
          else _accumulate_endpoints_helper(tail, newDelta, betweeness)
        }
      }
    }
    _accumulate_endpoints_helper(step.S.reverse, step.S.map(_ -> 0.0).toMap, newBetweeness)
  }

  /*
  def _accumulate_basic(betweenness, S, P, sigma, s):
    delta = dict.fromkeys(S, 0)
    while S:
        w = S.pop()
        coeff = (1 + delta[w]) / sigma[w]
        for v in P[w]:
            delta[v] += sigma[v] * coeff
        if w != s:
            betweenness[w] += delta[w]
    return betweenness, delta
   */

  def _accumulate_basic(step: ShortStep[N], s: N, betweeness: Map[N, Double]): Accumulate = {
    def _accumulate_basic_helper(S: Seq[N], delta: Map[N, Double], betweeness: Map[N, Double]): Accumulate = {
      println("DELTA " + delta)
      println("we are on the - " + s + " Node ")
      println(S.mkString(" - "))
      S match {
        case Nil => Accumulate(delta, betweeness)
        case w :: tail => {
          println("POPED " + w)
          val coeff = (1 + delta(w)) / step.sigma(w)
          val Pw = ShortestPathHelper.findPath(w, step.parents)
          println("parents w = " + Pw.mkString(" - "))
          val newDelta = ShortestPathHelper.findPath(w, step.parents).flatMap { v =>
            delta.updatedWith(v)(_.map(_ + (step.sigma(v) * coeff)))
          }.toMap
          println("NEW DELTA w " + newDelta)

          if (w != s) {
            println("DIFF " + w)
            println("BETWEENESS " + betweeness)
            println("NEW DELTA " + newDelta)
            val newBetweeness = betweeness.updatedWith(w)(_.map(_ + newDelta(w)))
            // val newBetweeness = betweeness.updated(w, newDelta(w) + old)
            _accumulate_basic_helper(tail, newDelta, newBetweeness)
          }
          else _accumulate_basic_helper(tail, newDelta, betweeness)
        }
      }
    }
    _accumulate_basic_helper(step.S.reverse, step.S.map(_ -> 0.0).toMap, betweeness)
  }

  def _rescale(betweeness: Map[N, Double], n: Int, normalized: Boolean, directed: Boolean, k: Option[Int], endpoints: Boolean): Map[N, Double] = {
    val scale: Option[Double] = if (normalized) {
      if (endpoints) {
        if (n < 2) None
        else Some(1 / (n * (n - 1)))
      }
      else if (n <= 2) None
      else Some(1 / ((n - 1) * (n - 2)))
    }
    else {
      if (!directed) Some(0.5)
      else None
    }

    if (scale.isDefined) {
      if (k.isDefined && k.get > 0) {
        val newScale = scale.get * n / k.get
        betweeness.keySet.flatMap { v =>
          betweeness.updatedWith(v)(_.map(_ * newScale))
        }.toMap
      }
      else betweeness
    }
    else betweeness
  }
}