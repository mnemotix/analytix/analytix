/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.shortestPath

import com.mnemotix.analytix.model.Graph


object BFSShortest {

  def findshortestPaths[N](source: N, graph: Graph[N]): ShortStep[N] = {
    val sDistances: Map[N, Int] = graph.nodes.map(_ -> Int.MaxValue).toMap + (source -> 0)
    val sigma = graph.nodes.map(_ -> 0.0).toMap + (source -> 1.0)

    def shortestPath(step: ShortStep[N]): ShortStep[N] = {
      step.extractMin().map {
        case (node, currentDistance) =>

          val newDist: Seq[(N, Int)] = graph.neighbours(node).collect {
            case n: N if step.distances.get(n).exists(_ > currentDistance + 1) =>
              n -> (currentDistance + 1)
          }


          val newSigma = newDist.map { case (neighbour, _) =>
            val sigmav = sigma(node)
            val sigmaw = sigma(neighbour)
            neighbour -> (sigmav + sigmaw)
          }

          val newParents: Seq[(N, N)] = newDist.map { case (neighbour, _) => neighbour -> node }

          shortestPath(ShortStep(
            step.parents ++ newParents,
            step.unprocessed - node,
            step.distances ++ newDist,
            step.sigma ++ newSigma,
            step.S :+ node
          ))
      }.getOrElse(step)
    }

    shortestPath(ShortStep(Map(), graph.nodes.toSet, sDistances, sigma, Seq()))
  }

  private def findPathRec[N](node:N, parents: Map[N, N]): List[N] =
    parents.get(node).map(parent => node +: findPathRec(parent, parents)).getOrElse(List(node))

  /**
   * Function return the way from source node to given destination node.
   *
   * @param destination     destination node
   * @param parents         parent nodes
   * @tparam N
   * @return                path from source to destination
   */
  def findPath[N](destination: N, parents: Map[N, N]): List[N] = {
    findPathRec(destination, parents).reverse
  }
}
