/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.shortestPath

import scala.util.Try

case class ShortStep[N](parents: Map[N,N],
                        unprocessed: Set[N],
                        distances: Map[N, Int],
                        sigma: Map[N, Double],
                        S: Seq[N]
                       ) {

  /**
   * Function extracts minimal value from unprocessed nodes.
   *
   * @return
   */
  def extractMin(): Option[(N, Int)] = Try(unprocessed.minBy(n => distances(n))).toOption.map(n => (n, distances(n)))
}