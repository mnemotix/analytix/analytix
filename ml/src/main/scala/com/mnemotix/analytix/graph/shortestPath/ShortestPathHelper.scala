/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.graph.shortestPath

object ShortestPathHelper {

  /*
    def populate[N](nodes: Seq[Seq[N]], graph: Graph[N]): Graph[N] = {
    def populateHelper(rest: Seq[Seq[N]], newGraph: Graph[N]): Graph[N] = {
      rest match {
        case Nil => newGraph
        case x :: tail => {
          populateHelper(tail, newGraph.addEdge(x(1), x(2)))
        }
      }
    }
    populateHelper(nodes, graph)
  }
   */

  private def findPathRec[N](node:N, parents: Map[N, N]): List[N] = {
    def helper(node:N, path: List[N]): List[N] = {
      if (parents.get(node).isDefined) {
        helper(parents.get(node).get, node +: path)
      }
      else path
    }
/*
    parents.get(node).map { parent =>
      node +: findPathRec(parent, parents)
    }.getOrElse(List(node))
*/
    helper(node, List())

  }

  /**
   * Function return the way from source node to given destination node.
   *
   * @param destination     destination node
   * @param parents         parent nodes
   * @tparam N
   * @return                path from source to destination
   */
  def findPath[N](destination: N, parents: Map[N, N]): List[N] = {
    findPathRec(destination, parents).reverse
  }

}
