/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.datamining

import apriori4s.{AnalysisResult, AprioriAlgorithm, AssociationRule, FrequentItemSet, Transaction}

class Apriori(minSupport: Double, minConfidence: Double, transactions: Seq[com.mnemotix.analytix.model.Transaction]) {

  val apriori = AprioriAlgorithm(minSupport, minConfidence)

  private def transaction(): Seq[Transaction] = {
    transactions.map(t => apriori4s.Transaction(t.items.toSet))
  }

  def fit: AnalysisResult = {
    apriori.analyze(transaction())
  }

  def frequentItemSet(model: AnalysisResult): Map[Integer, Set[FrequentItemSet]] = {
    model.frequentItemSets
  }

  def associationRule(model: AnalysisResult): Set[AssociationRule] = {
    model.associationRules
  }
}