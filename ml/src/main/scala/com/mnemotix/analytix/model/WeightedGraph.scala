/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.model

case class WeightedEdge[N](destination: N, weight: Int)

class WeightedGraph[N](adjList: Map[N, List[WeightedEdge[N]]]) extends Graph[N] {

  def isWeighted = true

  override def isDirected: Boolean = true

  override def nodes: List[N] = adjList.keys.toList

  override def edges: List[(N, N)] = adjList.toList.flatMap { case (v, edgeList) =>
    edgeList.map(e => v -> e.destination)
  }

  def addEdge(from: N, weightedEdge: WeightedEdge[N]): WeightedGraph[N] = {
    val fromNeighbours = weightedEdge +: adjList.getOrElse(from, Nil)
    val g = new WeightedGraph(adjList + (from -> fromNeighbours))

    val to = weightedEdge.destination
    if(g.nodes.contains(to)) g else g.addNode(to)  }

  override def addEdge(from: N, to: N): WeightedGraph[N] = addEdge(from, WeightedEdge(to, 0))

  override def neighbours(node: N): List[N] = adjList.getOrElse(node, Nil).map(_.destination)

  def neighboursWithWeights(node: N): List[WeightedEdge[N]] = adjList.getOrElse(node, Nil)

  override def addNode(n: N): WeightedGraph[N] = new WeightedGraph(adjList + (n -> List()))
}