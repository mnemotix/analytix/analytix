import sbt._

object Version {
  lazy val analytixVersion = "3.0-SNAPSHOT"
  lazy val apriori = "0.4"
  lazy val duke = "1.2"
  lazy val lucene = "8.6.3"
  lazy val scalaTest = "3.1.1"
  lazy val scalaVersion = "2.13.7"
  lazy val synaptixVersion = "3.0-SNAPSHOT"
  lazy val play = "2.8.2"
  lazy val stanfordNLP = "4.0.0"
  lazy val dl4j = "1.0.0-beta7"
  lazy val spark  = "3.2.0"
  lazy val sparkNlp = "2.7.1"
  lazy val tika = "1.25"
  lazy val akka = "2.6.15"
  lazy val scalaGraph = "1.13.3"
  lazy val scalaXML = "2.0.1"
}

object Akka {
  lazy val akkaActorTyped = "com.typesafe.akka" %% "akka-actor-typed" % Version.akka
  //lazy val akkaJackson = "com.typesafe.akka" %% "akka-serialization-jackson" % Version.akka % Test
  lazy val akkaJackson = "com.typesafe.akka" %% "akka-serialization-jackson" % Version.akka
  lazy val akkaActor = "com.typesafe.akka" %% "akka-actor" % Version.akka
  lazy val akkaProtobuf = "com.typesafe.akka" %% "akka-protobuf-v3" % Version.akka
  lazy val akkaSLF4J = "com.typesafe.akka" %% "akka-slf4j" % Version.akka
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Version.akka
  lazy val alpakkaCsv = "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "3.0.4"

}

object Synaptix {
  lazy val synaptixCore = "com.mnemotix" % "synaptix-core_2.13" % Version.synaptixVersion
  lazy val synaptixRdfToolKit = "com.mnemotix" % "synaptix-rdf-toolkit_2.13" % Version.synaptixVersion
  lazy val synaptixIndexing = "com.mnemotix" % "synaptix-indexing-toolkit_2.13" % Version.synaptixVersion
  lazy val synaptixCacheToolkit = "com.mnemotix" % "synaptix-cache-toolkit_2.13" % Version.synaptixVersion
  lazy val synaptixJobsController = "com.mnemotix" % "synaptix-jobs-controller_2.13" % Version.synaptixVersion
  lazy val synaptixJobsToolkit = "com.mnemotix" % "synaptix-jobs-toolkit_2.13" % Version.synaptixVersion
  lazy val amqpLib = "com.mnemotix" % "synaptix-amqp-toolkit_2.13" % Version.synaptixVersion
}

object Dl4j {
  lazy val dl4j = "org.deeplearning4j" % "deeplearning4j-core" % Version.dl4j
  lazy val dl4jnn = "org.deeplearning4j" % "deeplearning4j-nn" % Version.dl4j
  lazy val dl4jmodel = "org.deeplearning4j" % "deeplearning4j-modelimport" % Version.dl4j
  lazy val dl4jNlp = "org.deeplearning4j" % "deeplearning4j-nlp" % Version.dl4j
  lazy val nd4fj = "org.nd4j" % "nd4j-native" % Version.dl4j
}

object Spark {
  lazy val sparkCore = "org.apache.spark" %% "spark-core" % Version.spark
  lazy val sparkSql = "org.apache.spark" %% "spark-sql" % Version.spark
  lazy val sparkNlp = "com.johnsnowlabs.nlp" % "spark-nlp_2.12" % Version.sparkNlp
  lazy val sparkML = "org.apache.spark" %% "spark-mllib" % Version.spark % "provided"
}

object Lucene {
  lazy val luceneCore = "org.apache.lucene" % "lucene-core" % Version.lucene
  lazy val luceneAnalyzersCommon = "org.apache.lucene" % "lucene-analyzers-common" % Version.lucene
  lazy val luceneQueryParser = "org.apache.lucene" % "lucene-queryparser" % Version.lucene
}

object Stanford {
  lazy val stanfordNLP = "edu.stanford.nlp" % "stanford-corenlp" % Version.stanfordNLP
  lazy val stanfordNLPFrench = "edu.stanford.nlp" % "stanford-corenlp" % Version.stanfordNLP classifier "models-french"
}

object Apache {
  lazy val commonsCodec = "commons-codec" % "commons-codec" % "1.15"
  lazy val commonsMath3 = "org.apache.commons" % "commons-math3" % "3.6.1"
  lazy val tikaParsers = "org.apache.tika" % "tika-parsers" % Version.tika
  lazy val tikaCore = "org.apache.tika" % "tika-core" % Version.tika
}

object ScalaGraph {
  lazy val core = "org.scala-graph" % "graph-core_2.13" % Version.scalaGraph
}

object Library {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val annoy4s = "net.pishen" %% "annoy4s" % "0.9.0"
  lazy val play = "com.typesafe.play" %% "play" % Version.play
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % "1.2.3"
  lazy val duke = "no.priv.garshol.duke" % "duke" % Version.duke
  lazy val jFreeChart =  "org.jfree" % "jfreechart" % "1.5.1"
  lazy val hnswlib = "com.github.jelmerk" % "hnswlib-core" % "1.0.1"
  lazy val hnswlibScala = "com.github.jelmerk" %% "hnswlib-scala" % "1.0.1"
  lazy val apriori = "com.github.seratch" % "apriori4s_2.12" % Version.apriori
  lazy val parCollection = "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.2"
  lazy val scalaXml = "org.scala-lang.modules" %% "scala-xml" % Version.scalaXML
}