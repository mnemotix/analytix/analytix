import Akka.{akkaActor, akkaActorTyped, akkaJackson, akkaProtobuf, akkaSLF4J, akkaStream, alpakkaCsv}
import Apache.{commonsCodec, commonsMath3, tikaCore, tikaParsers}
import Library._
import Synaptix._
import Dl4j._
import Lucene._
import Stanford._
import sbt.Keys.resolvers
import com.gilcloud.sbt.gitlab.GitlabPlugin

val meta = """META.INF(.)*""".r
ThisBuild / useCoursier := false
ThisBuild / scalaVersion := Version.scalaVersion


lazy val commons = Seq(
  version := Version.analytixVersion,
  organization := "com.mnemotix",
  licenses := List("Apache 2" -> new URL("https://www.apache.org/licenses/LICENSE-2.0.txt")),
  developers := List(
    Developer(
      id = "prlherisson",
      name = "Pierre-René Lhérisson",
      email = "pr.lherisson@gmail.com",
      url = url("http://www.mnemotix.com")
    ),
    Developer(
      id = "ndelaforge",
      name = "Nicolas Delaforge",
      email = "nicolas.delaforge@mnemotix.com",
      url = url("http://mnemotix.com")
    ),
  ),
  onChangedBuildSource := ReloadOnSourceChanges,
  credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
  GitlabPlugin.autoImport.gitlabGroupId     :=  Some(8346337),
  GitlabPlugin.autoImport.gitlabProjectId   :=  Some(19477634),
  GitlabPlugin.autoImport.gitlabDomain      :=  "gitlab.com",

  resolvers ++= Seq(
    Resolver.mavenLocal,
    Resolver.typesafeRepo("releases"),
    Resolver.typesafeIvyRepo("releases"),
    Resolver.sbtPluginRepo("releases"),
    "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven",
    //"MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
    //"MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/"
  ),
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
    case n if n.endsWith(".conf") => MergeStrategy.concat
    case n if n.endsWith(".properties") => MergeStrategy.concat
    case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case meta(_) => MergeStrategy.discard
    case x => MergeStrategy.first
  }
)

lazy val root = (project in file("."))
  .aggregate(commonsModule, percolation, percolationController, nlp, ml)
  .settings(
    // crossScalaVersions must be set to Nil on the aggregating project
    crossScalaVersions := Nil,
    publish / skip := true
  )


lazy val commonsModule = (project in file("commons")).settings(
  commons,
  name := "analytix-commons",
  description := "commons",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixCore,
    logbackClassic,
    scalaXml
  )
)


lazy val percolation = (project in file("percolation")).settings(
  commons,
  name := "analytix-percolation",
  description := "analytix percolation",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    synaptixIndexing,
    synaptixRdfToolKit,
    synaptixCacheToolkit,
    hnswlib % "provided",
    hnswlibScala % "provided",
    dl4j % "provided",
    dl4jNlp % "provided",
    play % "provided",
    nd4fj % "test",
    stanfordNLP % "test",
    stanfordNLPFrench % "test",
    akkaActorTyped,
    akkaJackson,
    akkaSLF4J,
    akkaStream,
    akkaProtobuf,
    akkaActor,
    synaptixJobsController,
    synaptixJobsToolkit
  ) ++ {
    CrossVersion.partialVersion(scalaVersion.value) match {
      case Some((2, major)) if major <= 12 =>
        Seq()
      case _ =>
        Seq(parCollection)
    }
  }
).dependsOn(commonsModule, nlp).aggregate(commonsModule, nlp)

lazy val percolationController = (project in file("percolation-controller")).settings(
  commons,
  name := "analytix-percolation-controller",
  description := "analytix percolation controller",
  libraryDependencies ++= Seq(
    amqpLib,
    scalaTest % Test,
    synaptixIndexing,
    synaptixRdfToolKit,
    synaptixCacheToolkit,
    hnswlib,
    hnswlibScala,
    stanfordNLP,
    stanfordNLPFrench,
    akkaActorTyped,
    akkaJackson,
    akkaActor,
    akkaProtobuf,
    akkaSLF4J,
    akkaStream
  ),
  mainClass in(Compile, run) := Some("com.mnemotix.analytix.services.jc.PercolationController"),
  mainClass in assembly := Some("com.mnemotix.analytix.services.jc.PercolationController"),
  assemblyJarName in assembly := "mnemotix-percolation-controller.jar",
  imageNames in docker := Seq(
    ImageName(
      namespace = Some("registry.gitlab.com/mnemotix/analytix/analytix"),
      repository = name.value,
      tag = Some(version.value)
    )
  ),
  buildOptions in docker := BuildOptions(cache = false),
  dockerfile in docker := {
    val artifact: File = assembly.value
    val artifactTargetPath = s"/app/${artifact.name}"

    new Dockerfile {
      from("adoptopenjdk/openjdk11:alpine-slim")
      add(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  }
).dependsOn(commonsModule, nlp, percolation).aggregate(commonsModule, nlp, percolation).enablePlugins(DockerPlugin)


lazy val nlp = (project in file("nlp")).settings(
  commons,
  name := "analytix-nlp",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    stanfordNLP % "provided",
    stanfordNLPFrench % "provided",
    luceneCore % "provided",
    luceneAnalyzersCommon % "provided",
    dl4j % "provided",
    dl4jnn,
    dl4jmodel,
    nd4fj % "test",
    dl4jNlp % "provided",
    tikaCore % "provided",
    tikaParsers % "provided",
    commonsCodec
  ) ++ {
    CrossVersion.partialVersion(scalaVersion.value) match {
      case Some((2, major)) if major <= 12 =>
        Seq()
      case _ =>
        Seq(parCollection)
    }
  }
).dependsOn(commonsModule, ml).aggregate(commonsModule, ml)

/*
lazy val lifty = (project in file("modules/lifty")).settings(
  commons,
  name := "analytix-lifty",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    alpakkaCsv % "provided",
    synaptixRdfToolKit  % "provided",
    play % "provided",
    duke,
    stanfordNLP % "provided",
    stanfordNLPFrench % "provided"
  )
).dependsOn(commonsModule, nlp).aggregate(commonsModule, nlp)
*/

lazy val ml = (project in file("ml")).settings(
  commons,
  name := "analytix-ml",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    commonsMath3,
    jFreeChart,
    dl4j,
    nd4fj % "test",
    alpakkaCsv % "test",
    dl4jNlp,
    apriori,
  ) ++  {
    CrossVersion.partialVersion(scalaVersion.value) match {
      case Some((2, 12)) => Seq("org.scala-graph" % "graph-core_2.12" % "1.13.1")
      case Some((2, 13)) => Seq(parCollection, ScalaGraph.core)
      case _ => Seq()
    }
  }
).dependsOn(commonsModule).aggregate(commonsModule)


/*
lazy val sparkToolkit = (project in file("modules/spark-toolkit")).settings(
  commons,
  name := "analytix-spark-toolkit",
  libraryDependencies ++= Seq(
    scalaTest % Test,
    sparkCore,
    sparkSql,
    sparkNlp,
    sparkML,
    logbackClassic
  )
).dependsOn(commonsModule).aggregate(commonsModule)

 */