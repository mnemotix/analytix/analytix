package com.mnemotix.analytix.percolation.utils

import com.fasterxml.jackson.core.json.JsonReadContext
import com.mnemotix.analytix.percolation.model.Term
import play.api.libs.json.{JsObject, JsValue, Reads}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo https://www.reddit.com/r/scala/comments/3ual3j/proper_deserialization_of_a_trait/

class DiscrimatingTermExtractor[T <: Term: Reads](predicate: JsValue => Boolean) {
  def unapply(json: JsValue): Option[T] = pf.lift.apply(json)
  val pf: PartialFunction[JsValue, T] = {
    case obj: JsObject if predicate(obj.as[JsValue])  => obj.as[T]
  }
}