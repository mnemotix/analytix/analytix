/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.stringmatching

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import com.mnemotix.analytix.nlp.distance.cpmerge.{Cosine, Dice, FuzzyMap, Jaccard, Measure}
import com.mnemotix.analytix.nlp.keywordExtraction.NPFSTMultiWordExtraction
import com.mnemotix.analytix.percolation.api.AbstractConceptExtractor
import com.mnemotix.synaptix.core.GenericService

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class QuickSimMatchingModel(lang: String)(implicit val conceptExtractor: AbstractConceptExtractor) extends GenericService {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  val nfpst = new NPFSTMultiWordExtraction(lang)
  nfpst.init

  def fit: Future[FuzzyMap[String]] = {
    conceptExtractor.extractAsync.map {
      concepts => concepts.filter {
        concept => concept.prefLabel.isDefined
      }.flatMap { concept =>
        concept.prefLabel.get.map { prefLabel =>
          (prefLabel, concept.entityId.get)
        }
      }
    }.runWith(Sink.seq).map(_.flatten.toList).map(FuzzyMap(_))
  }

  def predict(fuzzyMap: FuzzyMap[String], query: String, threshold:Double,  measure:String): Seq[(String, String)] = {
    val meas = measureConv(measure)
    val phrases = nfpst.phrases(query)
    val words = phrases.tokens
    phrases.tokensSpans.map { tokenSpan =>
      val multiWord = words.slice(tokenSpan._1, tokenSpan._2)
      multiWord.map(_.text).mkString(" ")
    }.flatMap { phrase =>
      println(phrase)
      fuzzyMap.getMatches(phrase, threshold, meas).toSeq
    }.flatten.distinct
  }

  def measureConv(m: String): Measure = {
    if (m == "Cosine") {
      Cosine
    }
    else if (m == "Jaccard") {
      Jaccard
    }
    else if (m == "Dice") {
      Dice
    }
    else Cosine
  }

  override def init: Unit = logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  override def shutdown: Unit = {}
}