/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.embedding

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import com.github.jelmerk.knn.scalalike._
import com.github.jelmerk.knn.scalalike.hnsw._
import com.mnemotix.analytix.percolation.api.{AbstractConceptEmbeddingSummarizer, AbstractConceptExtractor}
import com.mnemotix.analytix.percolation.model.PercoVector
import com.mnemotix.synaptix.core.GenericService

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class HsnwPercolationModel(implicit val conceptExtractor: AbstractConceptExtractor, implicit val conceptEmbeddingSummarizer: AbstractConceptEmbeddingSummarizer) extends GenericService {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  def fit: Future[HnswIndex[String, Array[Float], PercoVector, Float]] = {
    conceptExtractor.extractAsync.via(conceptEmbeddingSummarizer.embeddingSummarizer).runWith(Sink.seq).map(_.flatten).map(percoVectors => populateIndex(percoVectors))
  }

  def predict(hnswIndex: HnswIndex[String, Array[Float], PercoVector, Float], percoVector: PercoVector, k: Int): immutable.Seq[SearchResult[PercoVector, Float]] = {
    hnswIndex.findNearest(percoVector.vector, k)
  }

  def populateIndex(percoVectors: immutable.Seq[PercoVector]): HnswIndex[String, Array[Float], PercoVector, Float] = {
    val dimensions = percoVectors(0).vector.length
    val hnswIndex: HnswIndex[String, Array[Float], PercoVector, Float] = HnswIndex[String, Array[Float], PercoVector, Float](dimensions, floatCosineDistance, maxItemCount = percoVectors.size)
    hnswIndex.addAll(percoVectors, listener = (workDone: Int, max: Int) =>
      logger.info(s"Added $workDone out of $max words tot the index")
    )
    hnswIndex
  }

  override def init: Unit = logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")

  override def shutdown: Unit = {}
}