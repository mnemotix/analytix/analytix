/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.es

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.mnemotix.analytix.percolation.api.AbstractPercolatorTextExtractor
import com.mnemotix.analytix.percolation.exceptions.QueryIndexationException
import com.mnemotix.analytix.percolation.services.{ESClient, ESPercolatorTextIndexer, IndexResponseOutput}
import com.mnemotix.synaptix.core.GenericService
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.update.UpdateResponse
import play.api.libs.json.{JsObject, Json}

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class PercolateQueryRegister(percolatorIndex: String)(implicit val extractor: AbstractPercolatorTextExtractor,esClient: ESClient) extends GenericService {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  lazy val output: IndexResponseOutput = new IndexResponseOutput()

  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    extractor.init
    output.init
  }

  def registerRawQuery(idDoc: String, query: String): Future[Done] = {
    var failure:Int = 0
    var success: Int = 0
    val rawQry = new RawQuery(Seq(percolatorIndex), Json.parse(query).as[JsObject])
    val source: Source[Response[UpdateResponse], NotUsed] = Source.future(esClient.register(idDoc, rawQry))
    val flow = Flow[Response[UpdateResponse]].map {
      updateResponse =>
        if (updateResponse.isSuccess) {
          success = success + 1
        }
        else {
          failure = failure + 1
        }
        logger.info(s"There where $success success and $failure failure")
    }
    source.via(flow).run()
  }

  def registerGraph(idDoc: String, query: String, logical: Option[String]): Future[Done] = {
    lazy val esIndexer = new ESPercolatorTextIndexer(percolatorIndex, query, logical)
    esIndexer.init

    val topHeadSink = Sink.ignore
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        extractor.extract(idDoc) ~> esIndexer.index ~> output.outputUpdateResponse ~> topHS
        ClosedShape
    }).run()
  }

  def bulkRegisterGraph(idDoc: Seq[String], query: String, logical: Option[String]): Future[Done] = {
    val topHeadSink = Sink.ignore
    lazy val esIndexer = new ESPercolatorTextIndexer(percolatorIndex, query, logical)
    esIndexer.init

    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        extractor.bulkExtract(idDoc) ~> esIndexer.index ~> output.outputUpdateResponse ~> topHS
        ClosedShape
    }).run()
  }

  def batchRegisterGraph(query: String, logical: Option[String]): Future[Done] = {
    lazy val esIndexer = new ESPercolatorTextIndexer(percolatorIndex, query, logical)
    esIndexer.init

    val topHeadSink = Sink.ignore
    try {
      RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
        (topHS) =>
          import GraphDSL.Implicits._
          extractor.batchExtract ~> esIndexer.index ~> output.outputUpdateResponse ~> topHS
          ClosedShape
      }).run()
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the concept indexation process", t)
        throw new QueryIndexationException("An error occured during the concept indexation process", Some(t))
    }
  }

  def register(idDoc: String, query: String, logical: Option[String]): Future[immutable.Seq[Response[UpdateResponse]]] = {
    lazy val esIndexer = new ESPercolatorTextIndexer(percolatorIndex, query, logical)
    val flowIndexer = extractor.extract(idDoc) via esIndexer.index
    flowIndexer.map(indexResponses => Future.sequence(indexResponses)).runWith(Sink.seq).map(fut => Future.sequence(fut).map(_.flatten)).flatten
  }

  def bulkRegister(idDoc: Seq[String], query: String, logical: Option[String]): Future[immutable.Seq[Response[UpdateResponse]]] = {
    lazy val esIndexer = new ESPercolatorTextIndexer(percolatorIndex, query, logical)
    val flowIndexer: Source[Seq[Future[Response[UpdateResponse]]], NotUsed] = extractor.bulkExtract(idDoc) via esIndexer.index
    flowIndexer.map(indexResponses => Future.sequence(indexResponses)).runWith(Sink.seq).map(fut => Future.sequence(fut).map(_.flatten)).flatten
  }

  def batchRegister(query: String, logical: Option[String]): Future[immutable.Seq[Response[UpdateResponse]]] = {
    try {
      lazy val esIndexer = new ESPercolatorTextIndexer(percolatorIndex, query, logical)
      val flowIndexer: Source[Seq[Future[Response[UpdateResponse]]], NotUsed] = extractor.batchExtract via esIndexer.index
      flowIndexer.map(indexResponses => Future.sequence(indexResponses)).runWith(Sink.seq).map(fut => Future.sequence(fut).map(_.flatten)).flatten
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the concept indexation process", t)
        throw new QueryIndexationException("An error occured during the concept indexation process", Some(t))
    }
  }

  override def shutdown: Unit = {}
}