/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.job

import akka.Done
import akka.actor.ActorSystem
import com.mnemotix.analytix.percolation.services.es.PercolateQueryRegister
import com.mnemotix.analytix.percolation.services.{ESClient, ESPercolatorTextExtractor}
import com.mnemotix.synaptix.core.SynaptixException
import com.mnemotix.synaptix.jobs.api.{JobMonitor, SynaptixJob}
import com.mnemotix.synaptix.jobs.configuration.{JobConfig, JobsConfiguration}
import com.mnemotix.synaptix.jobs.exceptions.JobInstanciationException

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

class PercolatorBatchIndexingJob extends SynaptixJob with JobMonitor {

  override val prefix: String = "percolator"
  //override val id: String = "percolatorBatchIndexing"
  override val label: String = "Batch job to index percolator query"

  implicit val system: ActorSystem = ActorSystem("PercolatorBatchIndexingJob")
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val esClient: ESClient = new ESClient()

  @throws(classOf[SynaptixException])
  override def process(): Future[Done] = {
    val config = JobsConfiguration.monitored.get(id)
    if (config.isDefined) {
      if (validate(config.get)) {
        val map = config.get.args
        implicit val esPercolatorTextExtractor: ESPercolatorTextExtractor = new ESPercolatorTextExtractor(
          map("percolatorIndex").value.as[String],
          map("fields").value.as[Seq[String]],
          Some(map("sortField").value.as[String])
        )
        val percolateQueryRegister = new PercolateQueryRegister(map("percolatorIndex").value.as[String])
        percolateQueryRegister.init
        percolateQueryRegister.batchRegisterGraph(map("queryString").value.as[String], None)
      }
      else {
        //onError("Config error", Some(throw JobInstanciationException("Config error")))
        throw JobInstanciationException("Config error")
      }
    }
    else {
      //onError("The config is missing", Some(throw JobInstanciationException("Config is missing")))
      throw JobInstanciationException("Config is missing")
    }
  }

  override def onStart(): Unit = {
    ???
  }

 // override def onStatus(percentage: Int, processed: Int, total: Int, message: String): Unit = ???

  override def onDone(): Unit = ???

  override def onError(errorMessage: String, cause: Option[Throwable]): Unit = ???

  def validate(config: JobConfig): Boolean = {
    val map = config.args
    if (!map.contains("percolatorIndex")) throw JobInstanciationException("`percolatorIndex` is missing form the config file")
    else if(!map.contains("queryString")) throw JobInstanciationException("`queryString` is missing form the config file")
    else if(!map.contains("fields")) throw JobInstanciationException("`fields` is missing form the config file")
    else if(!map.contains("sortField")) throw JobInstanciationException("`sortField` is missing form the config file")
    else true
  }

  override val monitors: mutable.Map[String, JobMonitor] = ???
  override val job: SynaptixJob = ???

  override def onTerminate(): Unit = ???

  override def onShutdown(): Unit = ???

  override def onAbort(): Unit = ???

  override def onUpdate(message: String): Unit = ???

  override def onProgress(processedItems: Int, totalItems: Int, message: Option[String]): Unit = ???
}