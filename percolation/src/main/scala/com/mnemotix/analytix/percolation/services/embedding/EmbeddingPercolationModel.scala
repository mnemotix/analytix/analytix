/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.embedding

import akka.actor.ActorSystem
import akka.stream.scaladsl.Sink
import com.mnemotix.analytix.nlp.distance.VectorSimilarity
import com.mnemotix.analytix.percolation.api.{AbstractConceptEmbeddingSummarizer, AbstractConceptExtractor}
import com.mnemotix.analytix.percolation.model.{PercoResult, PercoVector}

import scala.collection.immutable
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class EmbeddingPercolationModel(implicit val conceptExtractor: AbstractConceptExtractor, implicit val conceptEmbeddingSummarizer: AbstractConceptEmbeddingSummarizer) {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  def fit: Future[immutable.Seq[PercoVector]] = {
    conceptExtractor.extractAsync.via(conceptEmbeddingSummarizer.embeddingSummarizer).runWith(Sink.seq).map(_.flatten)
  }

  def predictAsync(futPercoVectors: Future[immutable.Seq[PercoVector]], percoVector: PercoVector, k: Int): Future[immutable.Seq[PercoResult]] = {
    futPercoVectors.map { percoVectors =>
      predict(percoVectors, percoVector, k)
    }
  }

  def predict(percoVectors: immutable.Seq[PercoVector], percoVector: PercoVector, k: Int): immutable.Seq[PercoResult] = {
    percoVectors.map { percoV1 =>
      PercoResult(percoV1.id, percoVector.id, VectorSimilarity.cosineSimilarity(percoV1.vector, percoVector.vector))
    }.sortBy(_.sim).reverse.take(k)
  }
}