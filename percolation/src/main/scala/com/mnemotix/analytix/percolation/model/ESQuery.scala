package com.mnemotix.analytix.percolation.model

import play.api.libs.json.Json

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

sealed trait ESQuery

case class QueryString(query: String, default_field: Option[String])

object QueryString {
  implicit lazy val format = Json.format[QueryString]
}

case class ESQueryString(query_string: QueryString) extends ESQuery

object ESQueryString {
  implicit lazy val format = Json.format[ESQueryString]
}

case class ESQueryMatch(`match`: Term) extends ESQuery

object ESQueryMatch {
  implicit lazy val format = Json.format[ESQueryMatch]
}

case class ESQueryBoolean(bool: BooleanClause) extends ESQuery

object ESQueryBoolean {
  implicit lazy val format = Json.format[ESQueryBoolean]
}

object ESQuery {
  implicit val ESQuery = Json.format[ESQuery]
}