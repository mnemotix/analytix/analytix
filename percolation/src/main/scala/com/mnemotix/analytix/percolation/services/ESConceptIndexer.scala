package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.analytix.percolation.model.{Concept}
import com.mnemotix.synaptix.core.GenericService
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.update.UpdateResponse

import scala.concurrent.Future

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESConceptIndexer(percolatorIndex: String)(implicit percolatorIndexer: ESClient) extends GenericService  {
  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    percolatorIndexer.init
  }

  def index: Flow[Seq[Concept], Seq[Future[Response[UpdateResponse]]], NotUsed] = {
    Flow[Seq[Concept]].map { concepts =>
      concepts.filter(c => c.hiddenLabel.isDefined && c.prefLabel.isDefined).filter(concept => conceptQueryStringBuilder(concept) != "").map { concept =>
        val queryString = conceptQueryStringBuilder(concept)
        percolatorIndexer.register(percolatorIndex, concept.entityId.get, queryString)
      }
    }
  }

  def conceptFilter(hiddenLabel: Seq[String], prefLabel:Seq[String]) = {
    val h = hiddenLabel.filter(label => label.split(" ").size < 5)
    val p = prefLabel.filter(label => label.split(" ").size < 5)
    h.size == hiddenLabel.size && p.size == prefLabel.size
  }

  def conceptQueryStringBuilder(concept: Concept): String = {
    val hidden = if (concept.hiddenLabel.isDefined) s"${queryStringBuilderUtil(concept.hiddenLabel.get)}" else ""
    val pref = if (concept.prefLabel.isDefined) s"${queryStringBuilderUtil(concept.prefLabel.get)}" else ""
    s"$pref".trim
  }

  def queryStringBuilderUtil(labels: Seq[String]): String = {
    if (labels.size > 0) {
      labels.map { label =>
          label.replaceAll("-", " ").replaceAll(":", " ")
            .replaceAll("\"", "")
            .replaceAll("\\W?/\\W?", " ")
            .replaceAll("\\(", "")
            .replaceAll("\\)", "").toLowerCase
      }.mkString(" OR ")
    }
    else ""
  }

  override def shutdown: Unit = {}
}