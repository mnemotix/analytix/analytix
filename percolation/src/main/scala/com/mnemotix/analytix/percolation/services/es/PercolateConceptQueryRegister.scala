/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.es

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ClosedShape
import akka.stream.scaladsl.{GraphDSL, RunnableGraph, Sink}
import com.mnemotix.analytix.percolation.api.AbstractConceptExtractor
import com.mnemotix.analytix.percolation.services.{ESClient, ESConceptIndexer, IndexResponseOutput}
import com.mnemotix.synaptix.core.GenericService

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class PercolateConceptQueryRegister(percolatorIndex: String)(implicit val extractor: AbstractConceptExtractor) extends GenericService {

  implicit val system: ActorSystem = ActorSystem(this.getClass.getSimpleName)
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  implicit val esClient = new ESClient()

  lazy val esIndexer: ESConceptIndexer = new ESConceptIndexer(percolatorIndex)
  lazy val output: IndexResponseOutput = new IndexResponseOutput()

  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    extractor.init
    esIndexer.init
    output.init
  }

  def registerAsync(): Future[Done] = {
    val topHeadSink = Sink.ignore
    RunnableGraph.fromGraph(GraphDSL.create(topHeadSink) { implicit builder =>
      (topHS) =>
        import GraphDSL.Implicits._
        extractor.extractAsync ~> esIndexer.index ~> output.outputUpdateResponse ~> topHS
        ClosedShape
    }).run()
  }

  override def shutdown: Unit = {
  }
}