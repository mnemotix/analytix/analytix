package com.mnemotix.analytix.percolation.services

import com.mnemotix.analytix.percolation.model.{Concept, PercolatorText}
import com.mnemotix.synaptix.core.GenericService
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESMappingDefinitions, RawQuery}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import play.api.libs.json.{JsArray, JsNumber, JsObject, JsString, JsValue, Json}

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESClient(implicit ec: ExecutionContext) extends GenericService {
  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
  }

  def createVectorIndex(indexName: String, mapping: ESMappingDefinitions) = {

  }

  def register(indexName: String, id: String, queryString: String) = {
    //IndexClient.registerQueryMatch(indexName, id, queryString, "percoLabel")
    IndexClient.registerQueryString(indexName, id, queryString, Some("percoLabel"))
  }

  def register(id: String, qry: RawQuery) = {
    IndexClient.registerRawQuery(id, qry)
  }

  def percolate(indexName: String, document: String): Future[Response[SearchResponse]] = {
    IndexClient.percolate(Seq(indexName), Map("percoLabel" -> document), Some(0), Some(100))
  }

  def count(indexName: String): Future[Long] = {
    val countResponseFut = IndexClient.count(Seq(indexName))
    countResponseFut.map(countResonse => countResonse.result.count)
  }

  private def searchConcepts(indexName: String, qry: String) = {
    val futIndexResponse = IndexClient.rawQuery(RawQuery(Seq(indexName), Json.parse(qry).as[JsObject]))
    futIndexResponse.map(indexResponse =>
      indexResponse.result.hits.hits.map { hit =>
        val json = Json.parse(hit.sourceAsString)
        val concept = json.as[Concept]
        concept
      }.toSeq)
  }

  def batchSearch(indexName: String, sortField: String, fields: Seq[String], total: Int): Future[Seq[PercolatorText]] = {
    def scrollHelper(from: Int, size: Int, scrollId: Future[Option[String]], percolatorText: Future[Seq[PercolatorText]]): Future[Seq[PercolatorText]] = {
      if (from == 0) {
        val res: Future[Response[SearchResponse]] = IndexClient.scrollQuery(Seq(indexName), Some("1m"), 500, sortField)
        val scrollId: Future[Option[String]] = res.map { r =>
          r.result.scrollId
        }
        val newPercolatorText = percolatorTextHelper(res, fields)
        scrollHelper(from + size, size, scrollId, newPercolatorText)
      }
      else if (from < total) {
        val sID = Await.result(scrollId, Duration.Inf)
        if (sID.isDefined) {
          val res: Future[Response[SearchResponse]] = IndexClient.searchScroll(sID.get, None)
          val scrollId: Future[Option[String]] = res.map { r =>
            r.result.scrollId
          }
          val newPercolatorText = percolatorTextHelper(res, fields)
          val seq = Seq(newPercolatorText, percolatorText)
          val merge = mergeFut(seq)
          scrollHelper(from + size, size, scrollId, merge)
        }
        else percolatorText
      }
      else {
        percolatorText
      }
    }
    scrollHelper(0, 500, Future(Some("")), Future(Seq.empty))
  }

  def search(indexName: String, idDocument: String, fields: Seq[String]): Future[Seq[PercolatorText]] = {
    val qry: String =
      s"""
         |{
         |"query" : {
         |  "ids": {
         |    "values" : ["$idDocument"]
         | }
         |}
         |}""".stripMargin

    logger.info(qry)

    val futIndexResponse: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(indexName), Json.parse(qry).as[JsObject]))
    percolatorTextHelper(futIndexResponse, fields)
  }

  def bulkSearch(indexName: String, idDocument: Seq[String], fields: Seq[String]) = {
    val qry: String =
      s"""
         |{
         |"query" : {
         |  "ids": {
         |    "values" : [${termsBuilder(idDocument)}]
         | }
         |}
         |}""".stripMargin

    logger.info(qry)

    val futIndexResponse: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(indexName), Json.parse(qry).as[JsObject]))
    percolatorTextHelper(futIndexResponse, fields)
  }

  private def mergeFut(c: Seq[Future[Seq[PercolatorText]]]): Future[Seq[PercolatorText]] = c.reduceLeft((a, b) => a.flatMap(xa => b.map(xb => xa ++ xb)))

  private def percolatorTextHelper(futIndexResponse: Future[Response[SearchResponse]], fields: Seq[String]): Future[Seq[PercolatorText]] = {
    futIndexResponse.map(indexResponse =>
      indexResponse.result.hits.hits.map { hit =>
        val json: JsValue = Json.parse(hit.sourceAsString)
        val texts = fields.map { field =>
          val lookupResult = (json \ field)
          val content: Option[Seq[String]] = {
            if (lookupResult.isDefined) {
            lookupResult.get match {
              case JsString(s) => {
                Some(Seq(s))
              }
              case JsNumber(s) => Some(Seq(s.toString()))
              case JsArray(value) => Some(value.map(_.toString()).toSeq)
              case _ => {
                None
              }
           }
          }
          else None
          }
          (field, content.toSeq.flatten)
        }
        PercolatorText(hit.id, texts.toMap)
      }.toSeq)
  }

  def termsBuilder(terms: Seq[String]): String = {
    if (terms.size > 1) {
      terms.map(term => s""""$term"""").mkString(",")
    }
    else {
      s""""${terms(0)}""""
    }
  }

  def conceptFirstPage(indexName: String, size : Int)  = {
    val qry =
      s"""
         |{
         | "size" : ${size},
         | "query": {
         |   "match_all": {}
         | },
         | "sort": [
         |   {"entityId": "asc"}
         | ]
         |}
         |""".stripMargin
    val futConcept = searchConcepts(indexName, qry)
    (futConcept, lastID(futConcept))

  }

  def conceptNextPages(indexName: String, size : Int, entityId: Future[Option[String]]): (Future[Seq[Concept]], Future[Option[String]]) = {
    def qry(enID: String) =
      s"""
         |{
         | "size" : ${size},
         | "query": {
         |   "match_all": {}
         | },
         | "sort": [
         |   {"entityId": "asc"}
         | ],
         | "search_after": ["${enID}"]
         |}
         |""".stripMargin

    val futConcept = entityId.map { id =>
      searchConcepts(indexName, qry(id.get))
    }.flatten

    (futConcept, lastID(futConcept))
  }

  def lastID(futConcept: Future[Seq[Concept]]): Future[Option[String]] = {
    futConcept.map { concept =>
      concept.last.entityId
    }
  }

  override def shutdown: Unit = IndexClient.shutdown()
}