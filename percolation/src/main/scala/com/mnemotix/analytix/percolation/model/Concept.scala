package com.mnemotix.analytix.percolation.model

import play.api.libs.json.{JsValue, Json, JsonValidationError, Reads, Writes, __}
import play.api.libs.functional.syntax._

import scala.collection.immutable
import scala.util.{Success, Try}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

case class Concept(
                   entityId: Option[String],
                   types: Option[Seq[String]],
                   inScheme: Option[Seq[String]],
                   related: Option[Seq[String]],
                   broader: Option[Seq[String]],
                   hiddenLabel: Option[Seq[String]],
                   hiddenLabel_locales : Option[Seq[String]],
                   prefLabel: Option[Seq[String]],
                   prefLabel_locales: Option[Seq[String]],
                   altLabel: Option[Seq[String]],
                   altLabel_locales: Option[Seq[String]],
                   notation: Option[String],
                   changeNote: Option[Seq[String]],
                   changeNote_locales: Option[Seq[String]],
                   closeMatch: Option[Seq[String]],
                   collection: Option[Seq[String]],
                   color: Option[Seq[String]],
                   comment: Option[Seq[String]],
                   definition: Option[String],
                   definition_locales: Option[String],
                   editorialNote: Option[String],
                   editorialNote_locales: Option[String],
                   exactMatch: Option[Seq[String]],
                   example: Option[String],
                   example_locales: Option[String],
                   hasAccessPolicy: Option[String],
                   hasAction: Option[String],
                   hasCreationAction: Option[String],
                   hasCreatorPerson: Option[String],
                   hasCreatorUserAccount: Option[String],
                   hasDeletionAction: Option[String],
                   hasExternalLink: Option[String],
                   hasOccupation: Option[Seq[String]],
                   hasPrereq: Option[Seq[String]],
                   hasSkill: Option[Seq[String]],
                   hasUpdateAction :Option[Seq[String]],
                   hasUpdatePerson: Option[String],
                   historyNote :Option[String],
                   historyNote_locales: Option[String],
                   isDraft: Option[String],
                   isMemberOf: Option[String],
                   isOccupationOf:Option[Seq[String]],
                   isQualificationOf :Option[String],
                   isSkillOf: Option[Seq[String]],
                   notation_locales:Option[String],
                   note: Option[Seq[String]],
                   note_locales: Option[Seq[String]],
                   scopeNote:Option[String],
                   scopeNote_locales:Option[String],
                   seeAlso:Option[String],
                   topConceptOf: Option[Seq[String]],
                   vocabulary:Option[String]
                  )

object Concept {

  val readSeqFromString: Reads[Seq[String]] =  implicitly[Reads[String]]
    .map(x => Try(Seq(x)))
    .collect (JsonValidationError(Seq("Parsing error"))){
      case seq: immutable.Seq[_] => Seq()
      case Success(a) => a
    }

  val readSeq: Reads[Seq[String]] = implicitly[Reads[Seq[String]]].orElse(readSeqFromString)

  val read1: Reads[(Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[String], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[String], Option[String], Option[String], Option[String])] = (
    (__ \ "entityId").readNullable[String] and
      (__ \ "types").readNullable[Seq[String]](readSeq) and
      (__ \ "inScheme").readNullable[Seq[String]](readSeq) and
      (__ \ "related").readNullable[Seq[String]](readSeq) and
      (__ \ "broader").readNullable[Seq[String]](readSeq) and
      (__ \ "hiddenLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "hiddenLabel_locales").readNullable[Seq[String]](readSeq) and
      (__ \ "prefLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "prefLabel_locales").readNullable[Seq[String]](readSeq) and
      (__ \ "altLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "altLabel_locales").readNullable[Seq[String]](readSeq) and
      (__ \ "notation").readNullable[String] and
      (__ \ "changeNote").readNullable[Seq[String]](readSeq) and
      (__ \ "changeNote_locales").readNullable[Seq[String]](readSeq) and
      (__ \ "closeMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "color").readNullable[Seq[String]](readSeq) and
      (__ \ "comment").readNullable[Seq[String]](readSeq) and
      (__ \ "definition").readNullable[String] and
      (__ \ "definition_locales").readNullable[String] and
      (__ \ "editorialNote").readNullable[String] and
      (__ \ "editorialNote_locales").readNullable[String]
    ).tupled

  val read2: Reads[(Option[Seq[String]], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String],
    Option[String], Option[String], Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[String],
    Option[String], Option[String], Option[String], Option[String], Option[Seq[String]], Option[String], Option[Seq[String]])] = (
    (__ \ "exactMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "example").readNullable[String] and
      (__ \ "example_locales").readNullable[String] and
      (__ \ "hasAccessPolicy").readNullable[String] and
      (__ \ "hasAction").readNullable[String] and
      (__ \ "hasCreationAction").readNullable[String] and
      (__ \ "hasCreatorPerson").readNullable[String] and
      (__ \ "hasCreatorUserAccount").readNullable[String] and
      (__ \ "hasDeletionAction").readNullable[String] and
      (__ \ "hasExternalLink").readNullable[String] and
      (__ \ "hasOccupation").readNullable[Seq[String]](readSeq) and
      (__ \ "hasPrereq").readNullable[Seq[String]](readSeq) and
      (__ \ "hasSkill").readNullable[Seq[String]](readSeq) and
      (__ \ "hasUpdateAction").readNullable[Seq[String]](readSeq) and
      (__ \ "hasUpdatePerson").readNullable[String] and
      (__ \ "historyNote").readNullable[String] and
      (__ \ "historyNote_locales").readNullable[String] and
      (__ \ "isDraft").readNullable[String] and
      (__ \ "isMemberOf").readNullable[String] and
      (__ \ "isOccupationOf").readNullable[Seq[String]](readSeq) and
      (__ \ "isQualificationOf").readNullable[String] and
      (__ \ "isSkillOf").readNullable[Seq[String]](readSeq)
    ).tupled

  val read3: Reads[(Option[String], Option[Seq[String]], Option[Seq[String]], Option[String], Option[String], Option[String], Option[Seq[String]], Option[String])] = (
    (__ \ "notation_locales").readNullable[String] and
      (__ \ "note").readNullable[Seq[String]](readSeq) and
      (__ \ "note_locales").readNullable[Seq[String]](readSeq) and
      (__ \ "scopeNote").readNullable[String] and
      (__ \ "scopeNote_locales").readNullable[String] and
      (__ \ "seeAlso").readNullable[String] and
      (__ \ "topConceptOf").readNullable[Seq[String]](readSeq) and
      (__ \ "vocabulary").readNullable[String]
    ).tupled

  val f: (
    (Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[String], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[String], Option[String], Option[String], Option[String]),
      (Option[Seq[String]], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String],
        Option[String], Option[String], Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[String],
        Option[String], Option[String], Option[String], Option[String], Option[Seq[String]], Option[String], Option[Seq[String]]),
      (Option[String], Option[Seq[String]], Option[Seq[String]], Option[String], Option[String], Option[String], Option[Seq[String]], Option[String])

    ) => Concept = {
    case (
      (
        entityId,
        types,
        inScheme,
        related,
        broader,
        hiddenLabel,
        hiddenLabel_locales,
        prefLabel,
        prefLabel_locales,
        altLabel,
        altLabel_locales,
        notation,
        changeNote,
        changeNote_locales,
        closeMatch,
        collection,
        color,
        comment,
        definition,
        definition_locales,
        editorialNote,
        editorialNote_locales
        ),
      (
        exactMatch,
        example,
        example_locales,
        hasAccessPolicy,
        hasAction,
        hasCreationAction,
        hasCreatorPerson,
        hasCreatorUserAccount,
        hasDeletionAction,
        hasExternalLink,
        hasOccupation,
        hasPrereq,
        hasSkill,
        hasUpdateAction,
        hasUpdatePerson,
        historyNote,
        historyNote_locales,
        isDraft,
        isMemberOf,
        isOccupationOf,
        isQualificationOf,
        isSkillOf
        ),
      (
        notation_locales,
        note,
        note_locales,
        scopeNote,
        scopeNote_locales,
        seeAlso,
        topConceptOf,
        vocabulary
        )
      ) => Concept(
      entityId,
      types,
      inScheme,
      related,
      broader,
      hiddenLabel,
      hiddenLabel_locales,
      prefLabel,
      prefLabel_locales,
      altLabel,
      altLabel_locales,
      notation,
      changeNote,
      changeNote_locales,
      closeMatch,
      collection,
      color,
      comment,
      definition,
      definition_locales,
      editorialNote,
      editorialNote_locales,
      exactMatch,
      example,
      example_locales,
      hasAccessPolicy,
      hasAction,
      hasCreationAction,
      hasCreatorPerson,
      hasCreatorUserAccount,
      hasDeletionAction,
      hasExternalLink,
      hasOccupation,
      hasPrereq,
      hasSkill,
      hasUpdateAction,
      hasUpdatePerson,
      historyNote,
      historyNote_locales,
      isDraft,
      isMemberOf,
      isOccupationOf,
      isQualificationOf,
      isSkillOf,
      notation_locales,
      note,
      note_locales,
      scopeNote,
      scopeNote_locales,
      seeAlso,
      topConceptOf,
      vocabulary,
    )
  }

  implicit val read: Reads[Concept] = (read1 and read2 and read3) {
    f
  }

  implicit val writes: Writes[Concept] = new Writes[Concept] {
    override def writes(o: Concept): JsValue = Json.obj(
      "entityId" -> o.entityId,
      "types" -> o.types,
      "inScheme" -> o.inScheme,
      "related" -> o.related,
      "broader" -> o.broader,
      "hiddenLabel" -> o.hiddenLabel,
      "hiddenLabel_locales" -> o.hiddenLabel_locales,
      "prefLabel" -> o.prefLabel,
      "prefLabel_locales" -> o.prefLabel_locales,
      "altLabel" -> o.altLabel,
      "altLabel_locales" -> o.altLabel_locales,
      "notation" -> o.notation,
      "changeNote" -> o.changeNote,
      "changeNote_locales" -> o.changeNote_locales,
      "closeMatch" -> o.closeMatch,
      "collection" -> o.collection,
      "color" -> o.color,
      "comment" -> o.comment,
      "definition" -> o.definition,
      "definition_locales" -> o.definition_locales,
      "editorialNote" -> o.editorialNote,
      "editorialNote_locales" -> o.editorialNote_locales,
      "exactMatch" -> o.exactMatch,
      "hasAction" -> o.hasAction,
      "hasCreationAction" -> o.hasCreationAction,
      "hasCreatorPerson" -> o.hasCreatorPerson,
      "hasCreatorUserAccount" -> o.hasCreatorUserAccount,
      "hasDeletionAction" -> o.hasDeletionAction,
      "hasExternalLink" -> o.hasExternalLink,
      "hasOccupation" -> o.hasOccupation,
      "hasPrereq" -> o.hasPrereq,
      "hasSkill" -> o.hasSkill,
      "hasUpdateAction" -> o.hasUpdateAction,
      "hasUpdatePerson" -> o.hasUpdatePerson,
      "historyNote" -> o.historyNote,
      "historyNote_locales" -> o.historyNote_locales,
      "isDraft" -> o.isDraft,
      "isMemberOf" -> o.isMemberOf,
      "isOccupationOf" -> o.isOccupationOf,
      "isQualificationOf" -> o.isQualificationOf,
      "isSkillOf" -> o.isSkillOf,
      "notation_locales" -> o.notation_locales,
      "note" -> o.note,
      "note_locales" -> o.note_locales,
      "scopeNote" -> o.scopeNote,
      "scopeNote_locales" -> o.scopeNote_locales,
      "seeAlso" -> o.seeAlso,
      "topConceptOf" -> o.topConceptOf,
      "vocabulary" -> o.vocabulary
    )
  }
}


