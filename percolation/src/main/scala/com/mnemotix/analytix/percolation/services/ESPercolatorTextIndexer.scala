/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.analytix.percolation.exceptions.PercolationIndexException
import com.mnemotix.analytix.percolation.model.PercolatorText
import com.mnemotix.synaptix.core.GenericService
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.update.UpdateResponse
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.Future

class ESPercolatorTextIndexer(percolatorIndex: String, query: String, logical: Option[String])(implicit percolatorIndexer: ESClient) extends GenericService {

  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
  }

  def index: Flow[Seq[PercolatorText], Seq[Future[Response[UpdateResponse]]], NotUsed] = {
    try {
      Flow[Seq[PercolatorText]].map { percosText =>
        percosText.map { percoText =>
          val qrys = queryBuilder(percoText.texts)
          val qrysClean = queryCleaner(qrys)
          val qrysSyntaxChecked = querySyntaxCorrector(qrysClean)
          percolatorIndexer.register(percoText.docId, rawQuery(qrysSyntaxChecked))
        }
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the concept indexation process", t)
        throw new PercolationIndexException("An error occured during the concept indexation process", Some(t))
    }
  }

  def queryBuilder(texts: Map[String,Seq[String]]): String = {
    def queryBuilderHelper(t: List[(String,Seq[String])], q: String): String = {
      t match {
        case Nil => q
        case h :: tail =>
          if (q.contains("$$"+h._1+"$$")) {
            val newQ = if (h._2.size > 0) {
             q.replace("$$"+h._1+"$$", h._2.map(_.replace("\"","")).mkString(logicalHandler(logical)))
            } else {
              q.replace("$$"+h._1+"$$", "$$$$")
            }
            queryBuilderHelper(tail, newQ)
          }
          else queryBuilderHelper(tail,q)
      }
    }
    queryBuilderHelper(texts.toList, query)
  }

  def logicalHandler(logical: Option[String]) = {
    if (logical.isDefined) {
      logical.get.trim.toLowerCase match {
        case "or" => " OR "
        case "and" => " AND "
        case _ => " OR "
      }
    }
    else " OR "
  }

  def queryCleaner(qryString: String): String = {
    qryString.replaceAll("\\[\\[(.*?)\\$\\$\\$\\$(.*?)\\]\\]", "").trim
      .replaceAll("\\[\\[", "").trim
      .replaceAll("]]", "").trim
  }

  /*
  Ajouter des espaces à la place des charactères
   */
  def querySyntaxCorrector(qryString: String): String = {
    qryString.replaceAll(",\\n(\\s*?)\\}", "\n    \\}")
      .replaceAll(",\\n(\\s*?)\\]", "\n    \\]")
  }

  def rawQuery(qry: String) = {
    try {
      RawQuery(Seq(percolatorIndex), Json.parse(qry).as[JsObject])
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during query creation process", t)
        throw new PercolationIndexException("An error occured during query creation process", Some(t))
    }
  }

  override def shutdown: Unit = {}
}