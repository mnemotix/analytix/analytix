/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.mnemotix.analytix.percolation.api.AbstractPercolatorTextExtractor
import com.mnemotix.analytix.percolation.exceptions.PercolationExtractException
import com.mnemotix.analytix.percolation.model.PercolatorText

import scala.concurrent.{ExecutionContext, Future}

class ESPercolatorTextExtractor(indexName: String, fields: Seq[String], sortField: Option[String])(implicit esClient: ESClient, ec: ExecutionContext) extends AbstractPercolatorTextExtractor {

  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
  }

  override def extract(id: String): Source[Seq[PercolatorText], NotUsed] = {
    try {
      Source.future(esClient.search(indexName, id, fields))
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the extraction process", t)
        throw new PercolationExtractException("An error occured during the concept extraction process", Some(t))
    }
  }

  override def bulkExtract(ids: Seq[String]): Source[Seq[PercolatorText], NotUsed] = {
    try {
      Source.future(esClient.bulkSearch(indexName, ids, fields))
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the extraction process", t)
        throw new PercolationExtractException("An error occured during the extraction process", Some(t))
    }
  }

  override def batchExtract: Source[Seq[PercolatorText], NotUsed] = {
    try {
      Source.future(esClient.count(indexName).map { total =>
        esClient.batchSearch(indexName, sortField.get, fields, total.toInt)}.flatten)
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the extraction process", t)
        throw new PercolationExtractException("An error occured during the extraction process", Some(t))
    }
  }

  override def shutdown: Unit = {}
}