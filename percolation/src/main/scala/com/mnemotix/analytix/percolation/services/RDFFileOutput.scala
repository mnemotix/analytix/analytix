package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.analytix.percolation.api.AbstractOutput

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@deprecated
class RDFFileOutput extends AbstractOutput {


  override def init: Unit = {logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")}

  /*
  override def bulkWrite: Flow[Seq[ConceptRecognition], Unit, NotUsed] = {
    //Flow[Seq[LiftyConceptRecognition]].via(liftyConcerter.liftyRecogToAnnotationAsync)
    //  .via(liftyRDFFileOutuput.bulkWrite)
    ???
  }


  def liftyAnnotations: Flow[Seq[LiftyConceptRecognition], Seq[LiftyAnnotation], NotUsed] = {
    Flow[Seq[LiftyConceptRecognition]].map(liftyConceptRecognitions =>
      liftyConceptRecognitions.map( liftyConceptRecognition =>
        ???
      )
    )
  }

  def defaultLiftyConceptRecognition(liftyConceptRecognition: LiftyConceptRecognition): LiftyAnnotation = {
    LiftyAnnotation(UriFactory.createURI(liftyConceptRecognition.res.id),
      liftyConceptRecognition.vocabularyEntity.property,
      AnnotationsUtils.createIRI( liftyConceptRecognition.vocabularyEntity.concept.toString),
      liftyConceptRecognition.weight
    )
  }

   */

  override def shutdown: Unit = {}
}
