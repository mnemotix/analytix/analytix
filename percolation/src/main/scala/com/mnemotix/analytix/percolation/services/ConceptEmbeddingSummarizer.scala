/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services

import java.io.FileInputStream
import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.mnemotix.analytix.nlp.helpers.TextProcessingConfig
import com.mnemotix.analytix.nlp.textprocessing.AnalytixRawTextProcessing
import com.mnemotix.analytix.percolation.api.AbstractConceptEmbeddingSummarizer
import com.mnemotix.analytix.percolation.exceptions.PercolationConceptEmbeddingException
import com.mnemotix.analytix.percolation.model.{Concept, PercoVector}
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer

class ConceptEmbeddingSummarizer(lang: String) extends AbstractConceptEmbeddingSummarizer {

  lazy val synaptixRawTextProcessing = new AnalytixRawTextProcessing(lang)
  lazy val modelFile = new FileInputStream(TextProcessingConfig.wordsEmbeddingModel)
  lazy val model = WordVectorSerializer.readBinaryModel(modelFile, true, true)

  def embeddingSummarizer: Flow[Seq[Concept], Seq[PercoVector], NotUsed] = {
    try {
      Flow[Seq[Concept]].map { concepts => concepts.map { concept =>
        val preflabels: Seq[String] = concept.prefLabel.toSeq.flatten
        val altLabels: Seq[String] = concept.altLabel.toSeq.flatten
        val features = Seq(embeddingHelper(preflabels), embeddingHelper(altLabels))  reduceLeft { (x, y) =>
          (x).lazyZip(y).map(_ + _)
        }
        PercoVector(concept.entityId.get, features.map(f => (f / features.size.toFloat).toFloat))
        }.filter(percoVector => percoVector.vector.exists(_ != 0.toFloat))
      }
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the embedding process", t)
        throw new PercolationConceptEmbeddingException("An error occured during the embedding process", Some(t))
    }
  }

  def embeddingHelper(listOfWords: Seq[String]): Array[Double] = {
    val listVector = listOfWords.map { words =>
      embeddingHelper(words)
    }
    if (listVector.size > 0) {
      listVector reduceLeft { (x, y) =>
        (x).lazyZip(y).map(_+_)
      }
    }.map(_ /listVector.size.toDouble)
    else Array.ofDim(model.vectorSize())
  }

  def embeddingHelper(words: String): Array[Double] = {
    val listVector: Seq[Array[Double]] = synaptixRawTextProcessing.words(words).map { word =>
      model.getWordVector(word)
    }.filterNot(_ == null)
    if (listVector.size > 0) {
      listVector reduceLeft { (x, y) =>
        (x).lazyZip(y).map(_+_)
      }
    }.map(_ /listVector.size.toDouble)
    else Array.ofDim(model.vectorSize())
  }

  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
  }

  override def shutdown: Unit = {}
}
