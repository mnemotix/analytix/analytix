package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.mnemotix.analytix.percolation.api.AbstractConceptExtractor
import com.mnemotix.analytix.percolation.exceptions.PercolationExtractException
import com.mnemotix.analytix.percolation.model.Concept

import scala.concurrent.{ExecutionContext, Future}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESConceptExtractor(conceptIndexName: String)(implicit esPercolatorClient: ESClient, ec: ExecutionContext) extends AbstractConceptExtractor {

  override def init: Unit = {
    logger.debug(s"Service ${this.getClass.getSimpleName} is initializing.")
    esPercolatorClient.init
  }

  override def extractAsync: Source[Seq[Concept], NotUsed] = {
    try {
      Source.future(esPercolatorClient.count(conceptIndexName).map(total => requester(total)).flatten)
    }
    catch {
      case t: Throwable =>
        logger.error("An error occured during the concept extraction process", t)
        throw new PercolationExtractException("An error occured during the concept extraction process", Some(t))
    }
  }

  def requester(total: Long): Future[Seq[Concept]] = {
    def requesterhelper(from: Int, size: Int, uri: Future[Option[String]], concepts: Future[Seq[Concept]]):  Future[Seq[Concept]] = {

      if (from == 0) {
        val tuple = esPercolatorClient.conceptFirstPage(conceptIndexName, size)
        requesterhelper(from + size, size, tuple._2, tuple._1)
      }
      else if (from < total) {
        val tuple = esPercolatorClient.conceptNextPages(conceptIndexName, size, uri)
        val r: Seq[Future[Seq[Concept]]] = Seq(concepts, tuple._1)
        val newConcepts = mergeFut(r)
        requesterhelper(from + size, size, tuple._2, newConcepts)
      }
      else {
        val tuple = esPercolatorClient.conceptNextPages(conceptIndexName, size, uri)
        val r: Seq[Future[Seq[Concept]]] = Seq(concepts, tuple._1)
        mergeFut(r)
      }
    }
    requesterhelper(0, 100, Future(Some("")), Future(Seq.empty))
  }

  def mergeFut(c: Seq[Future[Seq[Concept]]]): Future[Seq[Concept]] = c.reduceLeft((a, b) => a.flatMap(xa => b.map(xb => xa ++ xb)))

  override def shutdown: Unit = {}
}