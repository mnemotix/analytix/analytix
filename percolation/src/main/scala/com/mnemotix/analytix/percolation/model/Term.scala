package com.mnemotix.analytix.percolation.model

import com.mnemotix.analytix.percolation.utils.DiscrimatingTermExtractor
import play.api.data.validation.ValidationError
import play.api.libs.json.{JsError, JsObject, JsPath, JsResult, JsSuccess, JsValue, Json, Reads, Writes}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// todo https://queirozf.com/entries/fully-customized-json-validator-for-play-framework-2
// todo https://www.playframework.com/documentation/2.8.x/ScalaJsonAutomated
// todo https://stackoverflow.com/questions/30349953/how-to-parse-a-trait-and-objects-to-json-in-scala-play-framework

sealed trait Term

case class HiddenLabel(hiddenLabel: String) extends Term

object HiddenLabel {
  implicit lazy val format = Json.format[HiddenLabel]
}

case class PrefLabel(prefLabel: String) extends Term

object PrefLabel {
  implicit lazy val format = Json.format[PrefLabel]
}

case class AltLabel(altLabel: String) extends Term

object AltLabel {
  implicit lazy val format = Json.format[AltLabel]
}

object Term {
  implicit val format = Json.format[Term]
}

