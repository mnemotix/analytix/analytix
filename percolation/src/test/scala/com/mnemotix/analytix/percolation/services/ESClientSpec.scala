package com.mnemotix.analytix.percolation.services

import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.data.test.ConceptDataTest
import com.mnemotix.analytix.percolation.model.{Concept, PercolatorText}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import play.api.libs.json.{JsObject, Json}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESClientSpec extends PercolationSpec  {
  behavior of "ESPercolatorClient"

  it should "init an index client" ignore {
    val espClient = new ESClient
    espClient.init
  }

  it should "insert concept in percolator index" ignore {
    val res = IndexClient.insert("dev-openemploi-concept-perco",  ConceptDataTest.concept.entityId.get,  Json.toJson(ConceptDataTest.concept).as[JsObject])
    val res2 = IndexClient.insert("dev-openemploi-concept-perco",  ConceptDataTest.concept1.entityId.get,  Json.toJson(ConceptDataTest.concept1).as[JsObject])
    val res3 = IndexClient.insert("dev-openemploi-concept-perco",  ConceptDataTest.concept2.entityId.get,  Json.toJson(ConceptDataTest.concept2).as[JsObject])
    val resultat = Await.result(res, Duration.Inf)
    val resultat2 = Await.result(res2, Duration.Inf)
    val resultat3 = Await.result(res3, Duration.Inf)
    println(resultat.isSuccess)
    println(resultat2.isSuccess)
    println(resultat3.isSuccess)
  }

  it should "count concept in ES index" ignore {
    val espClient = new ESClient()
    val futrs = espClient.count("dev-openemploi-concept-perco")
    val nbr = Await.result(futrs, Duration.Inf)
    println(nbr)
    nbr must be > (0L)
  }


  it should "test a query" ignore {
    val percolatorQuery =
      """
        |{
        | "query": {
        |   "percolate": {
        |     "field": "query",
        |     "document": {
        |       "prefLabel": "Nous recherchons un collaborateur d'architecte dans notre agence"
        |     }
        |   }
        | }
        |}
        |""".stripMargin

    val obj = Json.parse(percolatorQuery).as[JsObject]

    val futrs = IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-concept-perco"), obj))
    val searchResponse = Await.result(futrs, Duration.Inf)
    searchResponse.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      println(s"${hit.id} --- $json --- ${hit.score}")
    }
  }

  it should "do a percolate query" ignore {
    val espClient = new ESClient()
    val futrs = espClient.percolate("dev-openemploi-concept-perco", "Nous recherchons un collaborateur aideur d'architecte notre agence. J'ai bien dit un collaborateur d'architecte.")
    val searchResponse = Await.result(futrs, Duration.Inf)
    searchResponse.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      println(s"${hit.id} --- $json --- ${hit.score}")
    }
    searchResponse.result.hits.hits.size must be > 0
  }

  it should "do a percolate query and save result in Concept" ignore  {
    val espClient = new ESClient()
    val futrs = espClient.percolate("dev-openemploi-concept-perco", "Nous recherchons un architecte pour notre agence. J'ai bien dit un collaborateur d'architecte")
    val searchResponse = Await.result(futrs, Duration.Inf)
    val concepts = searchResponse.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      println(s"${hit.id} --- $json --- ${hit.score}")
      json.as[Concept]
    }
    searchResponse.result.hits.hits.size must be > 0
    concepts.size > 1
  }

  it should "test a percolator query with pagination" in {
    val document = """Ingénieur de recherche chez Mnémotix, j’y amène mes compétences en data mining, analyse de graphes, en système de recommandation et en analyse de texte. J’ai participé durant mes années précédents aux travaux de recherche de l’équipe Connected Intelligence du Laboratoire Hubert Curien autour du Web de données, du Web sémantique et des systèmes de recommandation. Mes travaux de recherche portent sur la modélisation de profil d’utilisateur en se basant sur du contenu textuel pour la recommandation de produits nouveaux, divers et différents de ce que l’utilisateur avait apprécié dans le passé. Compétences""".stripMargin

    IndexClient.init()
    val percolatorQuery =
      s"""
        |{
        | "from": 0,
        | "size": 20,
        | "query": {
        |   "percolate": {
        |     "field": "query",
        |     "document": {
        |       "percoLabel": "$document"
        |     }
        |   }
        | }
        |}
        |""".stripMargin

    val obj = Json.parse(percolatorQuery).as[JsObject]
    val fut = Await.result(IndexClient.rawQuery(RawQuery(Seq("dev-openemploi-skill"), obj)), Duration.Inf)
    println(fut.result.hits.hits.size)
    fut.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      println(s"${hit.id} --- $json --- ${hit.score}")
    }
  }

  it should "test a percolator query with pagination with the method" in {
    val document = """Ingénieur de recherche chez Mnémotix, j’y amène mes compétences en data mining, analyse de graphes, en système de recommandation et en analyse de texte. J’ai participé durant mes années précédents aux travaux de recherche de l’équipe Connected Intelligence du Laboratoire Hubert Curien autour du Web de données, du Web sémantique et des systèmes de recommandation. Mes travaux de recherche portent sur la modélisation de profil d’utilisateur en se basant sur du contenu textuel pour la recommandation de produits nouveaux, divers et différents de ce que l’utilisateur avait apprécié dans le passé. Compétences""".stripMargin

    IndexClient.init()

    val fut = Await.result(IndexClient.percolate(Seq("dev-openemploi-skill"), Map("percoLabel" ->document), Some(0), Some(5)), Duration.Inf)
    println(fut.result.hits.hits.size)
    fut.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      println(s"${hit.id} --- $json --- ${hit.score}")
    }
  }

  it should "request a document in an index for a specific ID Document and transform it to PercolatorText" in {
    /**
     * request are sent to OPEN EMPLOI Stack
     */
    //http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e
    //search(indexName: String, idDocument: String, fields: Seq[String])
    val espClient = new ESClient()
    val futRS: Future[Seq[PercolatorText]] = espClient.search("dev-openemploi-occupation","http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e", Seq("altLabel", "prefLabel"))
    val res = Await.result(futRS, Duration.Inf)
    res.foreach(pe => println(pe))
    res.size must be > 0
  }

  it should "request a list of document in an index for a specific ID Document and transform them to PercolatorText" in {
    /**
     * request are sent to OPEN EMPLOI Stack
     */
      val ids = Seq("http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e", "http://ontology.datasud.fr/openemploi/data/occupation/376acd9a9d1618c601fc641c871dda34")

    val espClient = new ESClient()
    val futRS: Future[Seq[PercolatorText]] = espClient.bulkSearch("dev-openemploi-occupation", ids, Seq("altLabel", "prefLabel"))
    val res = Await.result(futRS, Duration.Inf)
    res.foreach(pe => println(pe))
    res.size must be > 0
  }

  it should "request all documents in an index and transform them to PercolatorText" in {
    val indexName = "dev-openemploi-occupation"
    val espClient = new ESClient()
    val size = Await.result(espClient.count(indexName), Duration.Inf)
    println(size)
    val futRS = espClient.batchSearch(indexName, "entityId", Seq("altLabel", "prefLabel"), size.toInt)
    val documents = Await.result(futRS, Duration.Inf)
    println(documents.size)

    documents.size  must be > 0
  }
}