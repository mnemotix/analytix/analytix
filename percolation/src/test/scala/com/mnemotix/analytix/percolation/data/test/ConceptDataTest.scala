package com.mnemotix.analytix.percolation.data.test

import com.mnemotix.analytix.percolation.model.Concept

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object ConceptDataTest {

  val concept = new Concept(Some("www.entity1.com"),
  None,
    Some(Seq("www.scheme1.com")),
    Some(Seq("www.related2.com", "www.related42.com")),
    Some(Seq("www.broader.com")),
    Some(Seq(""""+"chargé d'affaires environnement énergie"~3"""")),
    None,
    Some(Seq("Chargé d'affaires en environnement et énergie")),
    None,
    Some(Seq("Chargé d'affaires en environnement et énergie")),
    None,
    Some("30"),
    None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None
  )

  val concept1 = new Concept(Some("www.entity2.com"),
    None,
    Some(Seq("www.scheme1.com")),
    Some(Seq("www.related1.com", "www.related42.com")),
    Some(Seq("www.broader.com")),
    Some(Seq(""""+"Architecte d'intérieur"~3"""")),
    None,
    Some(Seq("Architecte d'intérieur")),
    None,
    Some(Seq("Architecte d'intérieur")),
    None,
    Some("32"),
    None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None
  )

  val concept2 = new Concept(Some("www.entity3.com"),
    None,
    Some(Seq("www.scheme1.com")),
    Some(Seq("www.related1.com", "www.related42.com")),
    Some(Seq("www.broader.com")),
    None,
    None,
    None,
    None,
    None,
    None,
    Some("32"),
    None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None, None, None, None, None, None, None, None, None, None,
    None, None, None
  )
}