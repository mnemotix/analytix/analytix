package com.mnemotix.analytix.percolation.model

import com.mnemotix.analytix.percolation.PercolationSpec
import play.api.libs.json.Json

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class TermSpec extends PercolationSpec {

  it should "write an HiddenLabel" in {
    val hiddenLabel = new HiddenLabel("hidden label")
    val json = Json.toJson(hiddenLabel)
    println(json)
  }

  it should "write an PrefLabel" in {
    val prefLabel = new PrefLabel("pref label")
    val json = Json.toJson(prefLabel)
    println(json)
  }

  it should "write an AltLabel" in {
    val altLabel = new AltLabel("alt Label")
    val json = Json.toJson(altLabel)
    println(json)
  }

  it should "read an HiddenLabel" in {
   val json = """{
      |"hiddenLabel": "this an hidden label"
      |}
      |""".stripMargin
    val hl = Json.parse(json).as[HiddenLabel]
    println(hl)
  }

  it should "read an PrefLabel" in {
    val json = """{
               |"prefLabel": "this an pref label"
               |}
               |""".stripMargin
    val pl = Json.parse(json).as[PrefLabel]
    println(pl)
  }

  it should "read an AltLabel" in {
    val json =
      """{
        | "altLabel": "this an alt label"
        |}
        |""".stripMargin
    val aL = Json.parse(json).as[AltLabel]
    println(aL)
  }
}