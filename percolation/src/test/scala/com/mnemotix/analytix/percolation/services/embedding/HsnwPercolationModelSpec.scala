/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.embedding

import com.github.jelmerk.knn.scalalike.SearchResult
import com.github.jelmerk.knn.scalalike.hnsw.HnswIndex
import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.model.PercoVector
import com.mnemotix.analytix.percolation.services.{ConceptEmbeddingSummarizer, ESClient, ESConceptExtractor}

import scala.collection.immutable
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.io.Source

class HsnwPercolationModelSpec extends PercolationSpec {

  implicit val esPercolatorClient = new ESClient()

  it should "learn embeddings" ignore  {
    val startTime = System.currentTimeMillis()

    implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
    implicit val conceptEmbedding = new ConceptEmbeddingSummarizer("french")

    val hsnwPercolationModel = new HsnwPercolationModel()
    val modelFut = hsnwPercolationModel.fit
    val model: HnswIndex[String, Array[Float], PercoVector, Float] = Await.result(modelFut, Duration.Inf)
    logger.info(s">> Time took to compute ${model.size} embeddings => {} s", (System.currentTimeMillis() - startTime)/ 100)
    model.size must be > 0
  }

  it should "get the skills related to an document" in {
    val modelStartTime = System.currentTimeMillis()

    implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
    implicit val conceptEmbedding = new ConceptEmbeddingSummarizer("french")

    val hsnwPercolationModel = new HsnwPercolationModel()
    val modelFut = hsnwPercolationModel.fit
    val model: HnswIndex[String, Array[Float], PercoVector, Float] = Await.result(modelFut, Duration.Inf)
    logger.info(s">> Time took to compute ${model.size} embeddings => {} s", (System.currentTimeMillis() - modelStartTime)/ 100)

    val cvStartTime = System.currentTimeMillis()
    val alaincv = Source.fromFile("modules/percolation/src/test/resources/cv/cvalain.txt").getLines.mkString(" ")
    val cvEmbedding = conceptEmbedding.embeddingHelper(alaincv)
    val percoVector = new PercoVector("cv Alain", cvEmbedding.map(_.toFloat))
    logger.info(s">> Time took to compute cv embeddings => {} s", (System.currentTimeMillis() - cvStartTime)/ 100)

    val knnStartTime = System.currentTimeMillis()
    val results = hsnwPercolationModel.predict(model, percoVector, 20)
    logger.info(s">> Time took to compute simlarity for cv embeddings => {} s", (System.currentTimeMillis() - knnStartTime)/ 100)

    logger.info(results.size.toString)

    results.foreach { result =>
      println(s"${result.item().id} ${result.distance()}")
    }

    results.size must be > 0
  }

}
