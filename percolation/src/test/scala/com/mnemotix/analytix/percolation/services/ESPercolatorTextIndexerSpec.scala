/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services

import com.mnemotix.analytix.percolation.PercolationSpec

class ESPercolatorTextIndexerSpec extends PercolationSpec {

  behavior of "ESPercolatorTextIndexer"

  implicit val esPercolatorClient = new ESClient()

  it should "compute query builder" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "($$prefLabel$$) OR ($$altLabel$$)",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val expAgricole = Map("prefLabel"-> Seq(""""Gérant d'exploitation agricole"""", """"Gérante d'exploitation agricole""""),
                          "altLabel" -> Seq("Gérant d'exploitation agricole"))
    val res = esPercoTxtIdxr.queryBuilder(expAgricole)
    println(res)
    res mustEqual """{
                    |  "query": {
                    |    "query_string": {
                    |      "query": "(Gérant d'exploitation agricole OR Gérante d'exploitation agricole) OR (Gérant d'exploitation agricole)",
                    |      "default_field": "content"
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "compute query builder for empty altLabel list" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "($$prefLabel$$) [[OR ($$altLabel$$)]]",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val map = Map("altLabel" -> List(), "prefLabel" -> List("tendance de la recherche"))
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val res = esPercoTxtIdxr.queryBuilder(map)
    res mustEqual """{
                    |  "query": {
                    |    "query_string": {
                    |      "query": "(tendance de la recherche) [[OR ($$$$)]]",
                    |      "default_field": "content"
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "compute query builder for a special word" in {
    val q = """{
              |  "query": {
              |    "bool" : {
              |      "should": [
              |        { "term": { "message": "$$prefLabel$$" }},
              |        { "term": { "message": "$$altLabel$$" }},
              |        { "prefix": { "message": "f"}}
              |      ]
              |    }
              |  }
              |}""".stripMargin
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", q, None)

    val chefDeProjetInfo = Map("prefLabel" -> Seq("Chef de projet en maîtrise d'œuvre informatique F/H"),
                               "altLabel" -> Seq("Chef de projet en maîtrise d'œuvre informatique"))
    val res = esPercoTxtIdxr.queryBuilder(chefDeProjetInfo)
    println(res)
    res mustEqual """{
                    |  "query": {
                    |    "bool" : {
                    |      "should": [
                    |        { "term": { "message": "Chef de projet en maîtrise d'œuvre informatique F/H" }},
                    |        { "term": { "message": "Chef de projet en maîtrise d'œuvre informatique" }},
                    |        { "prefix": { "message": "f"}}
                    |      ]
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "clean query with queryCleaner" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "(tendance de la recherche)[[ OR ($$$$)]]",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val map = Map("altLabel" -> List(), "prefLabel" -> List("tendance de la recherche"))
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val res = esPercoTxtIdxr.queryCleaner(query)
    res mustEqual """{
                    |  "query": {
                    |    "query_string": {
                    |      "query": "(tendance de la recherche)",
                    |      "default_field": "content"
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "queryBuilder with altLabel empty" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "(tendance de la recherche)[[ OR ($$$$)]]",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val map = Map("altLabel" -> List(), "prefLabel" -> List("tendance de la recherche"))
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val queryBuilderTxt = esPercoTxtIdxr.queryBuilder(map)
    println(queryBuilderTxt)
    val queryCleanerTxt = esPercoTxtIdxr.queryCleaner(queryBuilderTxt)
    queryCleanerTxt mustEqual """{
                                |  "query": {
                                |    "query_string": {
                                |      "query": "(tendance de la recherche)",
                                |      "default_field": "content"
                                |    }
                                |  }
                                |}""".stripMargin
  }

  it should "clean Syntax query corrector" in {
    val query = """{
                  |  "query": {
                  |    "bool" : {
                  |      "should": [
                  |        { "term": { "message": "tendance de la recherche" }},
                  |      ]
                  |    }
                  |  }
                  |}""".stripMargin

    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val txt = esPercoTxtIdxr.querySyntaxCorrector(query)
    txt mustEqual """{
                    |  "query": {
                    |    "bool" : {
                    |      "should": [
                    |        { "term": { "message": "tendance de la recherche" }}
                    |    ]
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "clean a second syntax query corrector" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "(tendance de la recherche)",
                  |      "default_field": "content",
                  |    }
                  |  }
                  |}""".stripMargin

    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val txt = esPercoTxtIdxr.querySyntaxCorrector(query)
    txt mustEqual """{
                    |  "query": {
                    |    "query_string": {
                    |      "query": "(tendance de la recherche)",
                    |      "default_field": "content"
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "build clean and check syntax of a simple query with no deletation" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "($$prefLabel$$) [[OR ($$altLabel$$)]]",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val map = Map("altLabel" -> List("tendance recherche"), "prefLabel" -> List("tendance de la recherche"))
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))

    val qrys = esPercoTxtIdxr.queryBuilder(map)
    val qrysClean = esPercoTxtIdxr.queryCleaner(qrys)
    val qrysSyntaxCheck = esPercoTxtIdxr.querySyntaxCorrector(qrysClean)

    qrysSyntaxCheck mustEqual """{
                    |  "query": {
                    |    "query_string": {
                    |      "query": "(tendance de la recherche) OR (tendance recherche)",
                    |      "default_field": "content"
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "build clean and check syntax of a simple query with a deletation" in {
    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "($$prefLabel$$)[[ OR ($$altLabel$$)]]",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val map = Map("altLabel" -> List(), "prefLabel" -> List("tendance de la recherche"))
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val qrys = esPercoTxtIdxr.queryBuilder(map)
    val qrysClean = esPercoTxtIdxr.queryCleaner(qrys)
    val qrysSyntaxCheck = esPercoTxtIdxr.querySyntaxCorrector(qrysClean)

    qrysSyntaxCheck mustEqual """{
                    |  "query": {
                    |    "query_string": {
                    |      "query": "(tendance de la recherche)",
                    |      "default_field": "content"
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "build and clean check syntax a boolean query with a deletation" in {
    val query = """{
                  |  "query": {
                  |    "bool" : {
                  |      "should": [
                  |        { "term": { "message": "$$prefLabel$$" }},
                  |        { "term": { "message": "$$altLabel$$" }},
                  |        [[{ "prefix": { "message": "$$prenom$$"}}]]
                  |      ]
                  |    }
                  |  }
                  |}""".stripMargin

    val map = Map("altLabel" -> List("tendance recherche"), "prefLabel" -> List("tendance de la recherche"), "prenom" -> List())
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))

    val qrys = esPercoTxtIdxr.queryBuilder(map)
    println(qrys)
    val qrysClean = esPercoTxtIdxr.queryCleaner(qrys)
    println(qrysClean)
    val qrysSyntaxCheck = esPercoTxtIdxr.querySyntaxCorrector(qrysClean)
    println(qrysSyntaxCheck)
    qrysSyntaxCheck mustEqual """{
                    |  "query": {
                    |    "bool" : {
                    |      "should": [
                    |        { "term": { "message": "tendance de la recherche" }},
                    |        { "term": { "message": "tendance recherche" }}
                    |    ]
                    |    }
                    |  }
                    |}""".stripMargin
  }

  it should "build a query with an sequence of prefLabel" in {
    val prefLabels = Seq("\"Per animal\"","\"Par animal\"")

    val query = """{
                  |  "query": {
                  |    "query_string": {
                  |      "query": "($$prefLabel$$)[[ OR ($$altLabel$$)]]",
                  |      "default_field": "content"
                  |    }
                  |  }
                  |}""".stripMargin
    val map = Map("altLabel" -> List(), "prefLabel" -> prefLabels)
    val esPercoTxtIdxr = new ESPercolatorTextIndexer("dev-openemploi-occupation", query, Some(" OR "))
    val qrys = esPercoTxtIdxr.queryBuilder(map)
    val qrysClean = esPercoTxtIdxr.queryCleaner(qrys)
    val qrysSyntaxCheck = esPercoTxtIdxr.querySyntaxCorrector(qrysClean)

    qrysSyntaxCheck mustEqual """{
                                |  "query": {
                                |    "query_string": {
                                |      "query": "(Per animal OR Par animal)",
                                |      "default_field": "content"
                                |    }
                                |  }
                                |}""".stripMargin

  }
}