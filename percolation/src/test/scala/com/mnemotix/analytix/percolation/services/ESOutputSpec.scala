package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.data.test.{ConceptRecognitionDataTest, TestMapping}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.{ESMappingDefinitions, RawQuery}
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import play.api.libs.json.{JsObject, Json}

import scala.collection.immutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESOutputSpec extends PercolationSpec {

  it should "insert two documents in ES" in {
    IndexClient.init()
    val createIndex = IndexClient.createIndex("offre-emploi-fake", ESMappingDefinitions(Json.parse(TestMapping.offresDemploi)).toMappingDefinition())
    Await.result(createIndex, Duration.Inf)


    Await.result(IndexClient.insert("offre-emploi-fake", "1", Json.obj(
      "idOffre" -> "1",
      "description" -> "Nous recherchons un architecte pour notre agence. J'ai bien dit un collaborateur d'architecte"
    )), Duration.Inf)

    Await.result(IndexClient.insert("offre-emploi-fake", "2", Json.obj(
      "idOffre" -> "2",
      "description" -> "chargé d'affaires pour l'environnement et la nature"
    )), Duration.Inf)
  }

  it should "get element in fake index" in {
    val qry =
      s"""
         |{
         | "query": {
         |   "match_all": {}
         | }
         |}
         |""".stripMargin

    val requestresultat: Response[SearchResponse] = Await.result(IndexClient.rawQuery(RawQuery(Seq("offre-emploi-fake"), Json.parse(qry).as[JsObject])), Duration.Inf)
    println(requestresultat.result.hits.total)

    requestresultat.result.hits.hits.map { hit =>
      val json = Json.parse(hit.sourceAsString)
      println(json)
    }
  }

  it should "update document with recognition" in {}

}

/*
   "idOffre": {
     "type": "keyword"
   },
   "description": {
 */
