/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services

import akka.stream.scaladsl.Sink
import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.model.PercolatorText
import com.mnemotix.synaptix.index.IndexClient
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import play.api.libs.json.{JsArray, JsNumber, JsString, JsValue, Json}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

class ESPercolatorTextExtractorSpec extends PercolationSpec {

  implicit val esClient = new ESClient()

  val idxName = "dev-openemploi-occupation"
  val fields = Seq("altLabel", "prefLabel")
  val sortField = "entityId"

  val id = "http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e"
  val ids = Seq("http://ontology.datasud.fr/openemploi/data/occupation/a7e00935bd22418f319b10051ed4349e", "http://ontology.datasud.fr/openemploi/data/occupation/376acd9a9d1618c601fc641c871dda34")

  it should "extract percolator text" in {
    val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, None)
    espercolatorTextExtractor.init
    val extractedFut = espercolatorTextExtractor.extract(id).runWith(Sink.seq)
    val extracted = Await.result(extractedFut, Duration.Inf)
    extracted.flatten.foreach(e => println(e.docId))
    println(extracted.flatten.size)
    extracted.size must be > 0
  }

  it should "bulk extract text" in {
    val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, None)
    espercolatorTextExtractor.init
    val extractedFut = espercolatorTextExtractor.bulkExtract(ids).runWith(Sink.seq)
    val extracted = Await.result(extractedFut, Duration.Inf)
    extracted.flatten.foreach(e => logger.debug(e.docId))
    logger.debug(extracted.flatten.size.toString)
    extracted.size must be > 0
  }

  it should "batch extract text" in {
    val idxName = "demo-mnx-concept"
    val fields = Seq("altLabel", "prefLabel")
    val sortField = "entityId"


    val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, Some(sortField))
    espercolatorTextExtractor.init
    val extractedFut = espercolatorTextExtractor.batchExtract.runWith(Sink.seq)
    val extracted = Await.result(extractedFut, Duration.Inf)
    extracted.flatten.toSeq.size must be > 0
  }
}