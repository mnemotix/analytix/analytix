/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.stringmatching

import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.services.{ESClient, ESConceptExtractor}
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.io.Source

class QuickSimMatchingModelSpec extends PercolationSpec {

  it should "fit the model" in {
    val modelStartTime = System.currentTimeMillis()
    implicit val esPercolatorClient = new ESClient()
    implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
    val simMatchingModel = new QuickSimMatchingModel("fr")
    val future = simMatchingModel.fit
    val model = Await.result(future, Duration.Inf)
    logger.info(s">> Time took to fit the model of ${model.size} => {} s", (System.currentTimeMillis() - modelStartTime)/ 100)
    model.size should be > 0
  }

  it should "percolate with QuickSimMatchingModel" in {
    val modelStartTime = System.currentTimeMillis()
    implicit val esPercolatorClient = new ESClient()
    implicit val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
    val simMatchingModel = new QuickSimMatchingModel("fr")
    val future = simMatchingModel.fit
    val model = Await.result(future, Duration.Inf)
    logger.info(s">> Time took to fit the model of ${model.size} => {} s", (System.currentTimeMillis() - modelStartTime)/ 100)

    val cvStartTime = System.currentTimeMillis()
    val alaincv = Source.fromFile("modules/percolation/src/test/resources/cv/cvalain.txt").getLines.mkString(" ")
    val res = simMatchingModel.predict(model, alaincv, 0.85, "Cosine")
    logger.info(s">> Time took to compute simlarity for CV => {} s", (System.currentTimeMillis() - cvStartTime)/ 100)
    res.foreach(println(_))
    println(res.size)
  }
}
