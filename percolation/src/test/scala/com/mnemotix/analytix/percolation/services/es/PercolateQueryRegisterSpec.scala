/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services.es

import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.services.{ESClient, ESPercolatorTextExtractor}
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.models.RawQuery
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.searches.SearchResponse
import play.api.libs.json.{JsObject, JsValue, Json}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.util.Success

class PercolateQueryRegisterSpec extends PercolationSpec {

  behavior of "PercolateQueryRegister"
  //val index = "dev-openemploi-skill-group"


  it should "register a percolate query for a given doc id" ignore {
    val index = "dev-openemploi-skill"

    val id = "http://ontology.datasud.fr/openemploi/data/skill/aff22ea636089e246312bf1728fdd8aa"
    //val id = "http://ontology.datasud.fr/openemploi/data/skill/e80499e7e8ea89f964f7ef8849b4a679"


    Thread.sleep(3000)

    val qry: String =
      s"""
         |{
         |"query" : {
         |  "ids": {
         |    "values" : ["$id"]
         | }
         |}
         |}""".stripMargin

    logger.info(qry)

    val futIndexResponse: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(index), Json.parse(qry).as[JsObject]))
    val idxResponse = Await.result(futIndexResponse, Duration.Inf)
    val l = idxResponse.result.hits.hits.length
    println(l)
    idxResponse.result.hits.hits.foreach { h =>
      println(h.id)
      val json: JsValue = Json.parse(h.sourceAsString)
      println(Json.prettyPrint(json))
      val query: JsValue = (json \ "query").get
      val top = Json.prettyPrint(query)
      println(top)
    }
  }

  it should "register a percolate query for a seq of ids" ignore {
    val index = "dev-openemploi-skill-group"
    implicit val esClient = new ESClient()
    implicit val extractor = new ESPercolatorTextExtractor(index, Seq("prefLabel", "altLabel"), Some("entityId"))
    val ids = Seq("http://ontology.datasud.fr/openemploi/data/skill/7e9fc0cc737b79a15064acc03f77eaec",
    "http://ontology.datasud.fr/openemploi/data/skill/9d9c764e8c337a04c6383b6aac59e616")
    val registerer = new PercolateQueryRegister(index)



    Thread.sleep(3000)

    val qry: String =
      s"""
         |{
         |"query" : {
         |  "ids": {
         |    "values" : ["${ids(0)}, ${ids(1)}"]
         | }
         |}
         |}""".stripMargin

    logger.info(qry)

    val futIndexResponse: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(index), Json.parse(qry).as[JsObject]))
    val idxResponse = Await.result(futIndexResponse, Duration.Inf)
    val l = idxResponse.result.hits.hits.length
    println(l)
    idxResponse.result.hits.hits.foreach { h =>
      println(h.id)
      val json: JsValue = Json.parse(h.sourceAsString)
      val query: JsValue = (json \ "query").get
      val top = Json.prettyPrint(query)
      println(top)
    }

  }

  it should "register a given percolate query for a given doc id" ignore {
    val id =  "http://ontology.datasud.fr/openemploi/data/skill/e80499e7e8ea89f964f7ef8849b4a679"
    //val id = "http://ontology.datasud.fr/openemploi/data/skill/c64334c0109c54323d826f3bcbc2e86a"
    val qryS = """foo | bar + baz*"""

    val index = "dev-openemploi-skill"

    Thread.sleep(3000)

    val qry: String =
      s"""
         |{
         |"query" : {
         |  "ids": {
         |    "values" : ["$id"]
         | }
         |}
         |}""".stripMargin

    logger.info(qry)

    val futIndexResponse: Future[Response[SearchResponse]] = IndexClient.rawQuery(RawQuery(Seq(index), Json.parse(qry).as[JsObject]))
    val idxResponse = Await.result(futIndexResponse, Duration.Inf)
    val l = idxResponse.result.hits.hits.length
    println(l)
    idxResponse.result.hits.hits.foreach { h =>
      println(h.id)
      val json: JsValue = Json.parse(h.sourceAsString)
      println(Json.prettyPrint(json))
      val query: JsValue = (json \ "query").get
      val top = Json.prettyPrint(query)
      println(top)
    }
  }

  it should "do a batch percolate query register" in {
    val idxName = "demo-mnx-concept"
    val fields = Seq("altLabel", "prefLabel")
    val sortField = "entityId"

    /*val queryString = """{
                        |  "query": {
                        |    "query_string": {
                        |      "query": "($$prefLabel$$) OR ($$altLabel$$)",
                        |      "default_field": "percoLabel"
                        |    }
                        |  }
                        |}""".stripMargin*/

    val queryString = " { \"query\": { \n \"query_string\": { \n \"query\": \"($$prefLabel$$)[[ OR ($$altLabel$$)]]\", \n \"default_field\": \"percoLabel\" \n } \n } \n } "


    implicit val esClient = new ESClient()
    implicit val espercolatorTextExtractor = new ESPercolatorTextExtractor(idxName, fields, Some(sortField))
    val qryRegister = new PercolateQueryRegister(idxName)
    qryRegister.init
    val fut = qryRegister.batchRegisterGraph(queryString, Some("or"))
    val d = Await.result(fut, Duration.Inf)
    d
  }
}