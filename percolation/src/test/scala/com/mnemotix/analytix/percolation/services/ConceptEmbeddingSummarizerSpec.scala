/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.data.test.ConceptDataTest
import com.mnemotix.analytix.percolation.model.{Concept, PercoVector}

import scala.collection.immutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

class ConceptEmbeddingSummarizerSpec extends PercolationSpec {
  behavior of "ConceptEmbedding"

  val conceptEmbedding = new ConceptEmbeddingSummarizer("french")

  it should "embedded an label" in {
    val embeddings = conceptEmbedding.embeddingHelper("Entretenir des locaux")
    println(embeddings.length)
    embeddings.length mustBe(200)
  }

  it should "compute embeddings of a list of concept" in {
    val concepts = Seq(ConceptDataTest.concept, ConceptDataTest.concept1, ConceptDataTest.concept2)
    val s: Source[Seq[Concept], NotUsed] =  Source.single(concepts)
    val f = s.runWith(Sink.seq)
    val r = Await.result(f, Duration.Inf)
    println(r.flatten.size)
    val conceptEmbeddingSummarizer = new ConceptEmbeddingSummarizer("french")
    val embeddingsFut: Future[immutable.Seq[Seq[PercoVector]]] = s.via(conceptEmbeddingSummarizer.embeddingSummarizer).runWith(Sink.seq)
    val embeddings = Await.result(embeddingsFut, Duration.Inf)

    val result = embeddings.flatten
    result.size mustBe 2
    result(0).vector.length mustBe(200)
  }
}