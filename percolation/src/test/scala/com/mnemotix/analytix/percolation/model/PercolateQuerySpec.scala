package com.mnemotix.analytix.percolation.model

import com.mnemotix.analytix.percolation.PercolationSpec
import play.api.libs.json.{JsObject, JsValue, Json}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PercolateQuerySpec extends PercolationSpec {

  it should "write an PercolateQuery" in {
    val document = Map(
      "hiddenLabel" -> "an hidden label"
    )

    val percolatorQuery: PercolatorQuery = PercolatorQuery(QueryP(Percolate("query", document)))
    println(Json.toJson(percolatorQuery))
  }

  it should "read en PercolateQuery" in {
    val percolatorQuery =
      """
        |{
        | "query": {
        |   "percolate": {
        |     "field": "query",
        |     "document": {
        |       "hiddenLabel": "an hidden label",
        |       "price": "240"
        |     }
        |   }
        | }
        |}
        |""".stripMargin

    val percoQuery = Json.parse(percolatorQuery).as[PercolatorQuery]
    println(percoQuery.toString)
  }
}

