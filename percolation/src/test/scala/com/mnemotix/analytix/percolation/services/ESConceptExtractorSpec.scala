package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.{Sink, Source}
import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.model.Concept

import scala.concurrent.{Await}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESConceptExtractorSpec extends PercolationSpec {
  behavior of "ESConceptExtractor"

  implicit val esPercolatorClient = new ESClient()

  def conceptQueryStringBuilder(concept: Concept) = {

    //if (concept.hiddenLabel.isDefined) concept.hiddenLabel.get.foreach(println(_))
    val hidden = if (concept.hiddenLabel.isDefined) s"${queryStringBuilderUtil(concept.hiddenLabel.get)}" else ""
    val pref = if (concept.prefLabel.isDefined) s"${queryStringBuilderUtil(concept.prefLabel.get)}" else ""
    s"""$hidden OR \"$pref\"""".trim
  }

  //s"$hidden $pref $alt".trim

  def queryStringBuilderUtil(labels: Seq[String]) = {
    if (labels.size > 0) {
      labels.map(label => s"(${label.replaceAll("-","")
        .replaceAll(",", "")
        .replaceAll("d'", "")
        .replaceAll("l'", "")
        .replaceAll("\\(", "")
        .replaceAll("\\)", "").toLowerCase})").mkString(" OR ")
    }
    else ""
  }

  it should "extract concept in a async way" in {
    val esConceptExtract = new ESConceptExtractor("dev-openemploi-skill")
    esConceptExtract.init
    val source: Source[Seq[Concept], NotUsed] = esConceptExtract.extractAsync
    val resFut = source.runWith(Sink.seq)
    val res = Await.result(resFut, Duration.Inf)

    val labels = res.flatten.filter(c => c.hiddenLabel.isDefined && c.prefLabel.isDefined).filter(concept => conceptQueryStringBuilder(concept) != "").map { concept =>
      conceptQueryStringBuilder(concept)
    }
    labels.foreach(println(_))

    println(res.flatten.size)
    res.size must be > 0
  }
}

/*
concepts.filter(c => c.hiddenLabel.isDefined && c.prefLabel.isDefined).filter(concept => conceptQueryStringBuilder(concept) != "").map { concept =>
       val queryString = conceptQueryStringBuilder(concept)
       percolatorIndexer.register(percolatorIndex, concept.entityId.get, queryString)
 */