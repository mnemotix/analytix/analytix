package com.mnemotix.analytix.percolation.services

import akka.NotUsed
import akka.stream.scaladsl.{RunnableGraph, Sink, Source}
import com.mnemotix.analytix.percolation.PercolationSpec
import com.mnemotix.analytix.percolation.data.test.ConceptDataTest
import com.sksamuel.elastic4s.Response
import com.sksamuel.elastic4s.requests.update.UpdateResponse

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ESConceptIndexerSpec extends PercolationSpec {

  it should "create query string" in {
    implicit val esClient = new ESClient()
    val esConceptIndexer = new ESConceptIndexer("dev-openemploi-concept")
    val rs = esConceptIndexer.queryStringBuilderUtil(Seq("label 1", "second label", "dernier label"))
    println(rs)
    rs mustBe("(label 1) OR (second label) OR (dernier label)")
  }

  it should "build query string based on labels in concept obj" in {
    implicit val esClient = new ESClient()
    val esConceptIndexer = new ESConceptIndexer("dev-openemploi-concept")
    val rs1 = esConceptIndexer.conceptQueryStringBuilder(ConceptDataTest.concept)
    val rs2 = esConceptIndexer.conceptQueryStringBuilder(ConceptDataTest.concept2)
    rs1 mustBe """("+"chargé d'affaires environnement énergie"~3") OR (Chargé d'affaires en environnement et énergie) OR (Chargé d'affaires en environnement et énergie)""".stripMargin
    rs2 mustBe("")
  }

  it should "init an perco index" in {
    implicit val esClient = new ESClient()
    val esConceptIndexer = new ESConceptIndexer("dev-openemploi-concept-perco")
    esConceptIndexer.init
  }

  it should "index seq of concept" in {
   val concepts = Seq(ConceptDataTest.concept, ConceptDataTest.concept1, ConceptDataTest.concept2)
    implicit val esClient = new ESClient()
    val esConceptIndexer = new ESConceptIndexer("dev-openemploi-concept-perco")
    val source: Source[Seq[Future[Response[UpdateResponse]]], NotUsed] = Source.single(concepts).via(esConceptIndexer.index)
    val sink: RunnableGraph[NotUsed] = source.to(Sink.seq[Seq[Future[Response[UpdateResponse]]]])
    sink.run()
  }

}
