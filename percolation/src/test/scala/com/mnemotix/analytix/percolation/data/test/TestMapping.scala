package com.mnemotix.analytix.percolation.data.test

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object TestMapping {

  val offresDemploi =
    """
      |{
      | "properties": {
      |   "idOffre": {
      |     "type": "keyword"
      |   },
      |   "description": {
      |      "type": "text",
      |       "fields": {
      |        "keyword": {
      |           "type": "keyword",
      |           "ignore_above": 256
      |        }
      |      }
      |    }
      | }
      |}
      |""".stripMargin

  val concept = """{
                  |      "properties": {
                  |        "altLabel": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "altLabel_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "broader": {
                  |          "type": "keyword"
                  |        },
                  |        "changeNote": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "changeNote_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "closeMatch": {
                  |          "type": "keyword"
                  |        },
                  |        "collection": {
                  |          "type": "keyword"
                  |        },
                  |        "color": {
                  |          "type": "keyword",
                  |          "ignore_above": 256
                  |        },
                  |        "comment": {
                  |          "type": "keyword",
                  |          "ignore_above": 256
                  |        },
                  |        "createdAt": {
                  |          "type": "date"
                  |        },
                  |        "definition": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "definition_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "editorialNote": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "editorialNote_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "entityId": {
                  |          "type": "keyword"
                  |        },
                  |        "exactMatch": {
                  |          "type": "keyword"
                  |        },
                  |        "example": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "example_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "hasAccessPolicy": {
                  |          "type": "keyword"
                  |        },
                  |        "hasAction": {
                  |          "type": "keyword"
                  |        },
                  |        "hasCreationAction": {
                  |          "type": "keyword"
                  |        },
                  |        "hasCreatorPerson": {
                  |          "type": "keyword"
                  |        },
                  |        "hasCreatorUserAccount": {
                  |          "type": "keyword"
                  |        },
                  |        "hasDeletionAction": {
                  |          "type": "keyword"
                  |        },
                  |        "hasExternalLink": {
                  |          "type": "keyword"
                  |        },
                  |        "hasOccupation": {
                  |          "type": "keyword"
                  |        },
                  |        "hasPrereq": {
                  |          "type": "keyword"
                  |        },
                  |        "hasSkill": {
                  |          "type": "keyword"
                  |        },
                  |        "hasUpdateAction": {
                  |          "type": "keyword"
                  |        },
                  |        "hasUpdatePerson": {
                  |          "type": "keyword"
                  |        },
                  |        "hiddenLabel": {
                  |          "type": "keyword",
                  |          "ignore_above": 256
                  |        },
                  |        "hiddenLabel_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "historyNote": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "historyNote_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "inScheme": {
                  |          "type": "keyword"
                  |        },
                  |        "isDraft": {
                  |          "type": "keyword",
                  |          "ignore_above": 256
                  |        },
                  |        "isMemberOf": {
                  |          "type": "keyword"
                  |        },
                  |        "isOccupationOf": {
                  |          "type": "keyword"
                  |        },
                  |        "isQualificationOf": {
                  |          "type": "keyword"
                  |        },
                  |        "isSkillOf": {
                  |          "type": "keyword"
                  |        },
                  |        "notation": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "notation_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "note": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "note_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "prefLabel": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "prefLabel_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "query": {
                  |          "type": "percolator"
                  |        },
                  |        "related": {
                  |          "type": "keyword"
                  |        },
                  |        "scopeNote": {
                  |          "type": "text",
                  |          "fields": {
                  |            "keyword": {
                  |              "type": "keyword",
                  |              "ignore_above": 256
                  |            }
                  |          }
                  |        },
                  |        "scopeNote_locales": {
                  |          "type": "keyword"
                  |        },
                  |        "seeAlso": {
                  |          "type": "keyword",
                  |          "ignore_above": 256
                  |        },
                  |        "topConceptOf": {
                  |          "type": "keyword"
                  |        },
                  |        "types": {
                  |          "type": "keyword"
                  |        },
                  |        "updatedAt": {
                  |          "type": "date"
                  |        },
                  |        "vocabulary": {
                  |          "type": "keyword"
                  |        }
                  |      }
                  |    }""".stripMargin

}
