# Axe d'amélioration des Percolation

## Match query

1. Changer le `OR` par defaut et le remplacer par un `AND` 
2. Ajouter `minimum_should_match` 
3. Remplacer le match par un match_phrase

## Terms Query

1. Mettre les concepts en keywords
2. Utiliser les Terms Query en spécifiant le nombre de terme qui doit matcher (Terms set query)

## Document

1. Utiliser sur le Field qui va acceuillir les documents à percoler un Analyzer pipe qui contiendra les étapes suivantes
	1. Character filter : Clean HTML text  
	2. Standard tokenizer
	3. lowercase; remove duplicate ?; stop list of tokens
2. Si étapes 1 difficile ou pas concluante changer en le standard analyzer en français