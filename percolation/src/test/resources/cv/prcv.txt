Ingénieur de recherche chez Mnémotix, j’y amène mes compétences en data mining,
analyse de graphes, en système de recommandation et en analyse de texte.
J’ai participé durant mes années précédents aux travaux de recherche de l’équipe
Connected Intelligence du Laboratoire Hubert Curien autour du Web de données, du
Web sémantique et des systèmes de recommandation.
Mes travaux de recherche portent sur la modélisation de profil d’utilisateur en se
basant sur du contenu textuel pour la recommandation de produits nouveaux, divers
et différents de ce que l’utilisateur avait apprécié dans le passé.
Compétences
Ingénierie documentaire : Data Mining, Système de recommandation, recherche
d'information, indexation, analyse de graphes
Expériences professionnelles
SCIC Mnémotix — ingénieur de recherche - ingénieur backend
JUILLET 2018 À AUJOURD’HUI, MANDELIEU
Développeurs de technologie d’aide à la gestion de données sémantiques.
1D Lab/ Laboratoire Hubert-Curien — Doctorant CIFRE
JANVIER 2015 À JUIN 2017, SAINT-ÉTIENNE
Doctorant CIFRE, création d’algorithmes de recommandations équitables pour des
parcours de découvertes culturelles alternatives en mettant l’accent sur les concepts
de nouveauté, diversité et sérendipité.
Laboratoire Hubert-Curien — Attaché de recherche
OCTOBRE 2014 À DÉCEMBRE 2014, SAINT-ÉTIENNE
Recherche en fouille de texte et développement d’un algorithme qui permet de
recommander à des utilisateurs un liste d’articles de presse nouveaux et présentant
une grande diversité
Laboratoire Hubert-Curien — Stage de master 2
MARS 2014 - JUILLET 2014, SAINT-ÉTIENNE
Recherche en fouille de texte et développement d’un algorithme qui permet de
recommander à des utilisateurs des articles de presse complémentaires, dont les
informations se complètent.
Collège Roger Anglade — Enseignant en Informatique
SEPTEMBRE 2011 - JUIN 2012, PORT-AU-PRINCE
Enseignant en Informatique pour des élèves en 4e et 3e
Turbo System S.A — Webmaster
JUIN 2011 - AOÛT 2012, PORT-AU-PRINCE
Développement et maintenance de site web vitrine pour Turbo System et ses
applications
Formation
Laboratoire Hubert-Curien — Doctorant CIFRE Direction : Pierre MARET
JANVIER 2005 - JANVIER 2008, SAINT-ÉTIENNE
Sujet : Système de recommandation équitable d’œuvres numériques : En quête de
diversité

Université Jean Monnet — Master 2 Web Intelligence
2014, SAINT-ÉTIENNE
Spécialité : Master en informatique dans l’apprentissage automatique, la fouille de
données, la représentation des connaissances.

Contact
Pierre-René LHÉRISSON
29 ans
115 avenue La Bruyère
38100 GRENOBLE
(33) 06 75 44 37 94
pr.lherisson@mnemotix.com
Réseaux sociaux

pierre-rene
HAL​ : PR Lhérisson

Compétences informatiques
Langages compilés :
Java, Scala, C.
Langages interprétés :
Python, R.
Langages de script :
PHP, Bash.
Langages de description :
XML, JSON.
Technologies Web :
DOM, CSS, JQuery.
Frameworks utilisés :
Maven, SBT, Elastic Search, Spark
Langues
Anglais : TOIC 880

École Supérieur d’Infotronique d’Haïti — Licence en informatique
2012, PORT-AU-PRINCE
Spécialité : Licence en sciences de l’informatique
Baccalauréat (section D), Haïti
2008