Responsable du développement des applications Web branchées sur le middleware sémantique Synaptix. Je suis en contact direct avec les utilisateurs côté client et les aide à construire les outils les plus adaptés à leurs besoins.

J’ai commencé ma carrière comme responsable de l’architecture des applications web au sein du studio hypermédia de l’Institut National de l’Audiovisuel où j’ai pu développer des compétences autour de la navigation intelligente au sein de contenus audiovisuels.



Compétences


Développement Web : Maîtrise de la chaîne de rendu ReactJS/Relay Modern/GraphQL et des applications web full-javascript.

Contenu multimédia : Ergonomie et techniques de navigation évoluées dans de gros corpus audiovisuels d’une part et au sein d’un contenu audiovisuel d’autre part.





Expériences professionnelles

SCIC Mnémotix — Co-fondateur - responsable des développements Front-end
AOÛT 2015 À AUJOURD’HUI, MANDELIEU
Responsable des développements informatiques des applications web et mobiles reliées au middleware sémantique SYNAPTIX.

INA — Chef de projet hypermédia
OCTOBRE 2008 - JUILLET 2015, BRY SUR MARNE
Ma mission au sein de l’institut est de mener à bien des projets de valorisation d’archives audiovisuelles. En contact après de nombreux partenaires, j’ai la responsabilité de gérer le développement technique des sites et applications web notamment sur leurs aspects technologiques et architecturaux.

zSlide — Développeur
MARS 2008 - SEPTEMBRE 2008, MONTREUIL
Ce stage ingénieur m’a amené à me familiariser avec la gestion de projet informatique dans le cadre du développement de l’application web Podmailing. Développeur au sein d’une équipe sous la responsabilité d’un chef de projet, j’ai notamment appris à travailler en suivant la méthode itérative et adaptative Agile.




Formation

ENSEEIHT — Diplôme d'ingénieur en Télécoms & Réseaux
2008, TOULOUSE
Baccalauréat Scientifique, mention bien
2003
